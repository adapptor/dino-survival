﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ImpassableTerrain))]
public class ImpassableTerrainEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var impassable = (ImpassableTerrain)target;

        if (GUILayout.Button("Regenerate"))
        {
            impassable.Generate();
        }

        if (GUILayout.Button("Clear"))
        {
            impassable.Clear();
        }
    }
}
