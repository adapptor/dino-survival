using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelManager))]
public class LevelManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var manager = (LevelManager)target;
        if (GUILayout.Button("Collect Next Egg"))
        {
            manager.CollectNextEgg();
        }
    }
}
