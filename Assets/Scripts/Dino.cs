﻿using UnityEngine;

public interface IBaseState
{
    void EnterState();

    void DoState();

    void LeaveState();
}

public interface IPreferredState
{
    bool ShouldEnterState();

    bool ShouldLeaveState();

    // Should only ever be called immediately after ShouldEnterState has returned true
    void EnterState();

    // Should only ever be called immediately after ShouldLeaveState has returned false
    void DoState();

    void LeaveState();
}

public class Dino : MonoBehaviour, IStoryScriptable {
    public DinoType DinoType;

    [Range(0.1f, 1.0f)]
    public float minScale = 0.6f;

    // Can't be more than 1 because that will mess with the navigation offsets
    [Range(0.1f, 1.0f)]
    public float maxScale = 1f;

    [Range(1.0f, 20.0f)]
    public float minAttack = 6.0f;

    [Range(1.0f, 20.0f)]
    public float maxAttack = 10.0f;

    [Range(0.5f, 2.0f)]
    public float minSpeed = 0.6f;

    [Range(0.5f, 2.0f)]
    public float maxSpeed = 1f;

    public IBaseState BaseState { get; set; }
    public IPreferredState PreferredState { get; set; }

    public bool inPreferredState;

    private bool pausedForStory;
    public IdleState idleState;

    public void StartStateMachine()
    {
        if (Application.isPlaying)
        {
            maxScale = Mathf.Max(maxScale, minScale);
            maxAttack = Mathf.Max(maxAttack, minAttack);
            maxSpeed = Mathf.Max(maxSpeed, minSpeed);

            var scale = Random.Range(minScale, maxScale);
            var range = maxScale - minScale;
            var proportion = scale - minScale;
            var scaleFactor = range > 0f ? proportion / range : 1f;

            transform.localScale = Vector3.one * scale;

            var moveBehaviours = GetComponents<MoveAbout>();
            foreach (var moveBehaviour in moveBehaviours)
            {
                moveBehaviour.Speed = minSpeed + ((maxSpeed - minSpeed) * scaleFactor);
            }

            var attackBehaviour = GetComponent<Attack>();
            if (attackBehaviour != null)
            {
                attackBehaviour.AttackStrength = minAttack + ((maxAttack - minAttack) * scaleFactor);
            }

            inPreferredState = false;
            if (BaseState != null)
            {
                BaseState.EnterState();
            }
        }
    }

    void Update()
    {
        if (pausedForStory)
        {
            idleState.DoState();
            return;
        }

        if (!inPreferredState && PreferredState != null && PreferredState.ShouldEnterState())
        {
            if (BaseState != null)
            {
                BaseState.LeaveState();
            }
            PreferredState.EnterState();
            inPreferredState = true;
        }
        else if (inPreferredState && PreferredState.ShouldLeaveState())
        {
            PreferredState.LeaveState();
            if (BaseState != null)
            {
                BaseState.EnterState();
            }
            inPreferredState = false;
        }

        if (inPreferredState)
        {
            PreferredState.DoState();
        }
        else
        {
            if (BaseState != null)
            {
                BaseState.DoState();
            }
        }
    }

    public void StoryPause()
    {
        pausedForStory = true;
        idleState.EnterState();
    }

    public void StoryResume()
    {
        pausedForStory = false;
        idleState.LeaveState();
    }
}
