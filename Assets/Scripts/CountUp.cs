﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountUp : MonoBehaviour {

    public Text myText;
    public int targetValue = 0;
    private int currentValue = 0;

	// Use this for initialization
	void Start () {
        myText.text = currentValue.ToString();
	}
	
	// Update is called once per frame
	void Update () {
        if (currentValue < targetValue){
            currentValue++;
            myText.text = currentValue.ToString();
        }
	}

  
}
