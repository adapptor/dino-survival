﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrowingUp : MonoBehaviour {

  public float size;
  public float duration;
  public float start;

	void Start() {
    StartCoroutine(Grow());
	}

  IEnumerator Grow() {
    float range = size - start;
    float current = start;
    float elapsed = 0f;

    while (current < size) {
      float t = Mathf.Min(elapsed / duration, 1f);
      float scale = start + (t * range);

      transform.localScale = new Vector3(scale, scale, scale);

      elapsed += Time.deltaTime;
      current = scale;

      yield return new WaitForEndOfFrame();
    }

    yield return null;
  }
}
