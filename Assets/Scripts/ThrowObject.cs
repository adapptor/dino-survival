﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowObject: MonoBehaviour  {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void throwObject(){
		StartCoroutine(throwing());
	}

	IEnumerator throwing() {
		var curr = this.transform.position;
		var target = new Vector3(curr.x + 2f, curr.y, curr.z);

		var id = LeanTween
			.move(this.gameObject, target, 0.6f)
			.setEase(LeanTweenType.easeOutQuad)
			.id;

		while (LeanTween.isTweening(id)) {
			yield return new WaitForEndOfFrame();
		}

		LeanTween.cancel(id);
		yield return null;
	}
}
