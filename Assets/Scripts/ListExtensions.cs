using System.Collections.Generic;
using UnityEngine;

public static class ListExtensions
{
    // Shuffle a copy of this list (non-destructive).
    public static List<T> Shuffle<T>(this List<T> list)
    {
        var r = new List<T>(list);
        return r.DestructiveShuffle();
    }

    // Shuffle a list in-place mutating it's contents.
    public static List<T> DestructiveShuffle<T>(this List<T> list)
    {
        int n = list.Count;  
        while (n > 1) {  
            n--;  
            int k = Random.Range(0, n);  
            T value = list[k];  
            list[k] = list[n];  
            list[n] = value;  
        } 
        return list;
    }

    public static T Pop<T>(this List<T> list)
    {
        var item = list[0];
        list.RemoveAt(0);
        return item;
    }
}
