using System;
using System.IO;
using UnityEngine;

public class Screenshots : MonoBehaviour
{
    private int Timestamp
    {
        get
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var timestamp = (int)(DateTime.UtcNow - epoch).TotalSeconds;

            return timestamp;
        }
    }

    public void Update()
    {
        if (Input.GetButton("Screenshot"))
        {
            var filename = "dino-screenshot-" + Timestamp + ".png";
            var filepath = "~/Desktop/" + filename;

            ScreenCapture.CaptureScreenshot(filepath, 1);
            
            Debug.Log("Capturing screenshot to " + filepath);
        }
    }
}
