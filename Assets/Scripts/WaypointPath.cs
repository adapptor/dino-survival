﻿using UnityEngine;

public class WaypointPath : MonoBehaviour
{
    public bool drawInEditor = true;
    public bool cycle = true;
    public Waypoint[] waypoints;

    void OnDrawGizmos()
    {
        if (!drawInEditor)
        {
            return;
        }

        if (waypoints == null || waypoints.Length < 2)
        {
            return;
        }

        for (int index = 0; index < waypoints.Length; ++index)
        {
            int nextWaypoint = index + 1;

            if (index == waypoints.Length - 1)
            {
                if (cycle)
                {
                    nextWaypoint = 0;
                }
                else
                {
                    break;
                }
            }

            if (waypoints[index] != null && waypoints[nextWaypoint] != null)
            {
                Gizmos.color = Color.black;
                Gizmos.DrawLine(waypoints[index].transform.position, waypoints[nextWaypoint].transform.position);
            }
        }
    }

    public int? GetNearestWaypointId(Vector2 position)
    {
        if (waypoints == null || waypoints.Length < 1)
        {
            return null;
        }

        var nearest = 0;
        var shortestDistance = Vector2.Distance(position, (Vector2)waypoints[0].transform.position);

        for (int index = 1; index < waypoints.Length; ++index)
        {
            var distance = Vector2.Distance(position, (Vector2)waypoints[index].transform.position);
            if (distance < shortestDistance)
            {
                nearest = index;
                shortestDistance = distance;
            }
        }

        return nearest;
    }

    public int GetNextWaypointId(int currentWaypointId)
    {
        var end = waypoints.Length - 1;

        if (cycle && currentWaypointId == end)
        {
            return 0;
        }

        if (!cycle && currentWaypointId == end)
        {
            return (end - 1) * -1;
        }

        return currentWaypointId + 1;
    }

    public int GetPreviousWaypointId(int currentWaypointId)
    {
        var end = waypoints.Length - 1;

        if (cycle && currentWaypointId == 0)
        {
            return end;
        }

        if (!cycle && currentWaypointId == -end)
        {
            return end - 1;
        }

        return currentWaypointId - 1;
    }

    public Vector2 GetWaypointPosition(int currentWaypointId)
    {
        if (waypoints == null || waypoints.Length == 0)
        {
            return (Vector2)transform.position;
        }

        int abs = currentWaypointId >= 0 ? currentWaypointId : currentWaypointId * -1;

        return (Vector2)waypoints[abs].transform.position;
    }

    public int GetFirstWaypointId()
    {
        return 0;
    }

    public int GetLastWaypointId()
    {
        if (waypoints == null || waypoints.Length == 0)
        {
            return 0;
        }
        else
        {
            return waypoints.Length - 1;
        }
    }

    public bool IsFirstWaypoint(int id)
    {
        return id == 0;
    }

    public bool IsLastWaypoint(int id)
    {
        if (waypoints == null || waypoints.Length == 0)
        {
            return true;
        }
        else
        {
            return id == waypoints.Length - 1;
        }
    }
}
