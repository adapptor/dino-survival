﻿using UnityEngine;
using UnityEngine.UI;

public class EnergyBar : MonoBehaviour
{
    public const float kMaxStamina = 100f;

    private RectTransform energyBarPanel;
    private RectTransform staminaBarPanel;
    private float energyBarInitialWidth;

    private float currentStamina = kMaxStamina;
    public float CurrentStamina
    {
        get
        {
            return currentStamina;
        }

        set
        {
            currentStamina = value;
            currentEnergy = currentStamina;
        }
    }
    private float currentEnergy = kMaxStamina;

    public float energyDepletedPercent = 15f;
    public Color energyDepletedColor;
    public Color staminaDepletedColor;

    Color energyForegroundColor;
    Color staminaForegroundColor;

    Image energyForegroundImage;
    Image staminaForegroundImage;

    // Use this for initialization
    void Start()
    {
        energyBarPanel = transform.Find("energybar_fg").GetComponent<RectTransform>();
        energyBarInitialWidth = energyBarPanel.rect.width;
        energyForegroundImage = transform.Find("energybar_fg").GetComponent<Image>();
        energyForegroundColor = energyForegroundImage.color;

        staminaBarPanel = transform.Find("energybar_fg_stam").GetComponent<RectTransform>();
        staminaForegroundImage = transform.Find("energybar_fg_stam").GetComponent<Image>();
        staminaForegroundColor = staminaForegroundImage.color;
    }

    // Increase or decrease stamina, an increase will also replenish energy
    public void changeStamina(float changeAmount)
    {
        currentStamina = Mathf.Clamp(currentStamina + changeAmount, 0f, kMaxStamina);
        currentEnergy = changeAmount > 0f ? currentStamina : Mathf.Min(currentEnergy, currentStamina);
    }

    // Reset stamina and energy back to maximum
    public void resetStamina()
    {
        currentStamina = kMaxStamina;
        currentEnergy = currentStamina;
    }

    // Regain energy back up to remaining stamina
    public void regainEnergy(float amount)
    {
        currentEnergy = Mathf.Min(currentStamina, currentEnergy + amount);
    }

    public void depleteEnergyAndStamina(float energyAmount, float staminaAmount)
    {
        currentStamina = Mathf.Max(currentStamina - staminaAmount, 0f);
        currentEnergy = Mathf.Clamp(currentEnergy - energyAmount, 0f, currentStamina);
    }

    void Update()
    {
        var energyPercent = currentEnergy / kMaxStamina;
        var newEnergybarWidth = energyPercent * energyBarInitialWidth;
        energyBarPanel.sizeDelta = new Vector2(newEnergybarWidth, 1.0f);

        if (energyPercent * 100 <= 1f)
        {
            energyForegroundImage.color = Color.clear;
        }
        else if (energyPercent * 100 < energyDepletedPercent)
        {
            energyForegroundImage.color = energyDepletedColor;
        }
        else
        {
            energyForegroundImage.color = energyForegroundColor;
        }

        var staminaPercent = currentStamina / kMaxStamina;
        var newStaminabarWidth = staminaPercent * energyBarInitialWidth;
        staminaBarPanel.sizeDelta = new Vector2(newStaminabarWidth, 1.0f);

        if (staminaPercent * 100 <= 0.01f)
        {
            staminaForegroundImage.color = Color.clear;
        }
        else if (staminaPercent * 100 < energyDepletedPercent)
        {
            staminaForegroundImage.color = staminaDepletedColor;
        }
        else
        {
            staminaForegroundImage.color = staminaForegroundColor;
        }
    }

    public bool hasEnergy()
    {
        return currentEnergy > 1f;
    }
}
