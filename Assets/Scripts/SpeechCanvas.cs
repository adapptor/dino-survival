﻿using UnityEngine;
using UnityEngine.UI;
using System;

public enum Character
{
    Professor,
    General,
    Mutant
}

public class SpeechCanvas : MonoBehaviour
{
    [Serializable]
    public class CharacterFace
    {
        public Character character;
        public Image face;
        public Color textColour;
    }

    public RectTransform faceBox;
    public CharacterFace[] characterFaces;

    public float slideTime = 0.75f;

    public RectTransform objectToSlide;

    public TW_MultiStrings_RandomPointer typeWriter;

    private float originalPosition;

    private int? moveTweenId;
    private bool dismissed = true;
    private bool dismissOnComplete;
    private Action onComplete;

    public bool CanSkip { get; set; }

    private void Awake()
    {
        originalPosition = objectToSlide.rect.height * -1;
        CanSkip = true;
    }

    public void PlayDialogue(string[] dialogue, Action onComplete, bool dismissOnComplete = true)
    {
        typeWriter.SetNewText(dialogue, OnComplete);

        Show();

        typeWriter.StartTypewriter();

        this.dismissOnComplete = dismissOnComplete;
        this.onComplete = onComplete;
    }

    void Show()
    {
        if (!dismissed)
        {
            return;
        }

        if (moveTweenId.HasValue)
        {
            LeanTween.cancel(moveTweenId.Value);
        }
        moveTweenId = LeanTween
            .moveY(objectToSlide, 0, slideTime)
            .setDelay(0.5f)
            .setEase(LeanTweenType.easeOutElastic)
            .setUseEstimatedTime(true)
            .setOnComplete(() => {
                // Annoyingly, using estimated time doesn't always get the end right!
                var rect = objectToSlide.rect;
                objectToSlide.rect.Set(rect.x, 0, rect.width, rect.height);
            })
            .uniqueId;

        // Don't worry about the tweens, they will cancel each other if necessary
        dismissed = false;
    }

    public void SetCharacterFace(Character character, bool faceAtBottom = false)
    {
        foreach (var charFace in characterFaces)
        {
            if (charFace.character == character)
            {
                charFace.face.enabled = true;
                typeWriter.SetTextColour(charFace.textColour);
            }
            else
            {
                charFace.face.enabled = false;
            }
        }

        var rect = faceBox.rect;
        if (!faceAtBottom)
        {
            faceBox.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 5f, rect.height);
        }
        else
        {
            faceBox.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 5f, rect.height);
        }
    }

    public void CompleteTypewriter()
    {
        if (typeWriter.IsDoneTyping())
        {
            typeWriter.NextString();
        }
        else
        {
            typeWriter.SkipTypewriter();
        }
    }

    public void OnComplete()
    {
        if (dismissOnComplete)
        {
            Dismiss();
        }

        if (onComplete != null)
        {
            onComplete();
        }
    }

    void Dismiss()
    {
        if (dismissed)
        {
            return;
        }

        if (moveTweenId.HasValue)
        {
            LeanTween.cancel(moveTweenId.Value);
        }

        moveTweenId = LeanTween
            .moveY(objectToSlide, originalPosition, slideTime)
            .setEase(LeanTweenType.easeOutExpo)
            .setUseEstimatedTime(true)
            .setOnComplete(() => {
                // Annoyingly, using estimated time doesn't always get the end right!
                var rect = objectToSlide.rect;
                objectToSlide.rect.Set(rect.x, originalPosition, rect.width, rect.height);
            })
            .uniqueId;

        // Don't worry about the tweens, they will cancel each other if necessary
        dismissed = true;
    }
}
