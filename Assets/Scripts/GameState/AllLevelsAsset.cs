﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "AllLevels", menuName = "Dino Survival/All Levels Asset", order = 1)]
[Serializable]
public class AllLevelsAsset : ScriptableObject
{
    public LevelDef[] levels;
    public LevelDef getLevel(string levelPath)
    {
        string fileName = System.IO.Path.GetFileNameWithoutExtension(levelPath);
        foreach (var l in levels)
        {
            if (string.Equals(l.fileName, fileName, StringComparison.InvariantCultureIgnoreCase))
            {
                return l;
            }
        }
        return null;
    }
}

[Serializable]
public class LevelDef
{
    public string fileName;
    public string displayName;
    public int timeLimitSeconds = 300;
    public bool showObjectivesAtStart = true;
    public LevelObjective[] objectives;
    public DinoType[] hiddenDinos;
}

[Serializable]
public class LevelObjective
{
    public DinoType dino;
    public int eggCount;
}
