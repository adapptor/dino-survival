﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class SaveGame
{
    public GameScore Score = new GameScore();
    public string currentLevel = null;
    public bool hasSeenStory = false;
    public List<SaveLevel> levelStates = new List<SaveLevel>();

    // For saving a level-state
    //public int Seed = UnityEngine.Random.Range(int.MinValue, int.MaxValue);
    //public SavePlayer Player = new SavePlayer();
    //public SaveLevel CurrentLevel = new SaveLevel();
    //public SavedTreesDictionary Trees = new SavedTreesDictionary();

    private static SaveGame s_saveGame;
    public static SaveGame Instance
    {
        get
        {
            if (s_saveGame == null)
            {
                Load();
            }
            return s_saveGame;
        }
    }

    public static void Reset()
    {
        PlayerPrefs.DeleteAll();
        s_saveGame = null;
    }

    private static readonly string SAVE_GAME_KEY = "game_state";
    internal static void Load()
    {
        // Get or create saved game state
        string gameStateJson = PlayerPrefs.GetString(SAVE_GAME_KEY);
        if (string.IsNullOrEmpty(gameStateJson))
        {
            s_saveGame = new SaveGame();
        }
        else
        {
            s_saveGame = JsonUtility.FromJson<SaveGame>(gameStateJson);
        }
    }

    public void Save()
    {
        // Save current game
        PlayerPrefs.SetString(SAVE_GAME_KEY, JsonUtility.ToJson(this));
        PlayerPrefs.Save();
    }

    public SaveLevel GetLevel(string fileName)
    {
        var levelState = levelStates.FirstOrDefault(l => string.Compare(l.fileName, fileName, StringComparison.InvariantCultureIgnoreCase) == 0);
        if (levelState == null)
        {
            levelState = new SaveLevel() { fileName = fileName };
            levelStates.Add(levelState);
        }
        return levelState;
    }

    public void CompleteLevelAndSave(AllLevelsAsset allLevels, string fileName)
    {
        var thisLevel = GetLevel(fileName);
        thisLevel.isComplete = true;

        var levelId = Array.FindIndex(allLevels.levels, l => string.Compare(l.fileName, fileName, StringComparison.InvariantCultureIgnoreCase) == 0);
        if (levelId >= 0 && levelId + 1 < allLevels.levels.Length)
        {
            var nextLevel = GetLevel(allLevels.levels[levelId + 1].fileName);
            nextLevel.isLocked = false;
        }
        Save();
    }

    internal static void Clear()
    {
        s_saveGame = new SaveGame();
    }
}

//public class SavedTreesDictionary : SerializableDictionaryBase<string, Tree.SaveState>
//{
//}