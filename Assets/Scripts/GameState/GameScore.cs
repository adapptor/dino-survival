﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameScore
{
    private List<DinoScore> scores = new List<DinoScore>();

    public int GetDinoScore(DinoType dinoType)
    {
        int score = GetDinoScoreObject(dinoType).score;
        return score;
    }

    public void AddDinoScore(DinoType dinoType)
    {
        var ds = GetDinoScoreObject(dinoType);
        ds.score += 1;
    }

    private DinoScore GetDinoScoreObject(DinoType dinoType)
    {
        var dinoScore = scores.FirstOrDefault(ds => ds.dinoType == dinoType);
        if (dinoScore == null)
        {
            dinoScore = new DinoScore();
            dinoScore.dinoType = dinoType;
            scores.Add(dinoScore);
        }
        return dinoScore;
    }
}

[System.Serializable]
class DinoScore
{
    public DinoType dinoType = DinoType.None;
    public int score = 0;
}