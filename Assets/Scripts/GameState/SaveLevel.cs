﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveLevel
{
    public string fileName;
    public bool isLocked = true;
    public bool isComplete = false;
}
