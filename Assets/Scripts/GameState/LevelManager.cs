﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public AllLevelsAsset allLevels;

    [Header("Level customisation")]

    [Range(Healthbar.kMaxHealth / 10, Healthbar.kMaxHealth)]
    public float startingHealth = Healthbar.kMaxHealth;
    [Range(EnergyBar.kMaxStamina / 10, EnergyBar.kMaxStamina)]
    public float startingStamina = EnergyBar.kMaxStamina;

    public bool hideHealth;
    public bool hideStamina;
    public bool hideSprint;
    public bool hideBinocs;
    public StoryDialogue startDialogue;
    public StoryDialogue finishDialogue;
    public StoryDialogue failDialogue;

    public StoryDialogue healthIncreaseDialogue;
    public StoryDialogue healthMaxedDialogue;

    public LevelDef levelDef { get; private set; }
    internal LevelState levelState { get; private set; }

    public delegate void ScoreChangedDelegate(LevelManager levelMgr, DinoType dinoType, bool didMeetObjective, bool allObjectivesMet);
    internal ScoreChangedDelegate OnScoreChanged;

    internal Action OnLevelComplete;
    internal Action OnLevelFailed;

    // Score for the current level
    internal GameScore levelScore = new GameScore();

    internal Hud hud;

    void Awake()
    {
        levelState = LevelState.PrePlay;
        Time.timeScale = 1.0f;
        hud = FindObjectOfType<Hud>();

        levelDef = allLevels.getLevel(SceneManager.GetActiveScene().name);
        if (levelDef == null)
        {
            Debug.LogWarning("No LevelDef found for current level. Add it to AllLevels asset.");
        }

        // Init random seed. Other objects using Random generation should do so in Awake.
        //UnityEngine.Random.InitState(SaveGame.Seed);
    }

    void Start()
    {
        hud.speechCanvas.gameObject.SetActive(true);
        hud.touchControls.SetActive(true);
        hud.hudElements.SetActive(true);
        hud.objectives.SetActive(true);

        SetHealthVisible(!hideHealth);
        SetStaminaVisible(!hideStamina);
        SetSprintVisible(!hideSprint);
        SetBinocsVisible(!hideBinocs);

        hud.healthBar.currentHealth = startingHealth;
        hud.energyBar.CurrentStamina = startingStamina;

        if (startDialogue != null)
        {
            startDialogue.PlayDialogue();
        }

        if (healthIncreaseDialogue != null)
        {
            hud.healthBar.OnHealthIncreaseButNotAtMax += OnHealthIncrease;
        }

        if (healthMaxedDialogue != null)
        {
            hud.healthBar.OnHealthReachedMax += OnHealthMaxed;
        }

        StartLevel();
    }

    void OnHealthIncrease()
    {
        if (healthIncreaseDialogue.gameObject.activeInHierarchy)
        {
            healthIncreaseDialogue.PlayDialogue();
        }
    }

    void OnHealthMaxed()
    {
        if (healthMaxedDialogue.gameObject.activeInHierarchy)
        {
            healthMaxedDialogue.PlayDialogue();
        }
    }

    public void StartLevel()
    {
        if (levelState != LevelState.PrePlay)
        {
            return;
        }

        if (levelDef != null && levelDef.timeLimitSeconds > 0)
        {
            StartTimer(levelDef.timeLimitSeconds);
        }
        else
        {
            hud.timer.NoTimeLimit = true;
        }

        levelState = LevelState.Playing;
    }

    public void SetHealthVisible(bool visible)
    {
        hud.healthBar.gameObject.SetActive(visible);
    }

    public void SetSprintVisible(bool visible)
    {
        hud.energyManager.gameObject.SetActive(visible);
    }

    public void SetStaminaVisible(bool visible)
    {
        hud.energyBar.gameObject.SetActive(visible);
    }

    public void SetBinocsVisible(bool visible)
    {
        hud.zoom.gameObject.SetActive(visible);
    }

    public void StartTimer(int seconds)
    {
        hud.timer.SetTimer(seconds);
    }

    public void CollectNextEgg()
    {
        foreach (var objective in this.levelDef.objectives)
        {
            var score = this.levelScore.GetDinoScore(objective.dino);
            if (score < objective.eggCount)
            {
                OnDinoTeleported(objective.dino);
                break; // done
            }
        }
    }

    public void OnDinoTeleported(DinoType dinoType)
    {
        Debug.Log("Teleported " + dinoType.ToString() + " egg");

        levelScore.AddDinoScore(dinoType);
        if (levelDef == null)
        {
            return;
        }

        bool allMet = true;
        bool justMet = false;
        foreach (var objective in levelDef.objectives)
        {
            int eggs = levelScore.GetDinoScore(objective.dino);
            if (eggs < objective.eggCount)
            {
                allMet = false;
            }
            if (dinoType == objective.dino && eggs == objective.eggCount)
            {
                justMet = true;
            }
        }
        if (OnScoreChanged != null)
        {
            OnScoreChanged(this, dinoType, justMet, allMet);
        }
        if (allMet)
        {
            if (levelState == LevelState.Playing)
            {
                if (finishDialogue == null)
                {
                    CompleteLevel();
                }
                else
                {
                    finishDialogue.PlayDialogue();
                }
            }
        }
    }

    public void CompleteLevel()
    {
        levelState = LevelState.LevelComplete;
        hud.DisablePause();

        if (OnLevelComplete != null)
        {
            OnLevelComplete();
        }
        hud.gameOver.gameObject.SetActive(true);
        Invoke("StopTime", DinoScoresUI.AnimTime);

        if (levelDef != null)
        {
            SaveGame.Instance.CompleteLevelAndSave(allLevels, levelDef.fileName);
        }
    }

    void StopTime()
    {
        Time.timeScale = 0.0f;
    }

    void Update()
    {
        if (levelState == LevelState.Playing)
        {
            if (hud.timer.countDownFinished())
            {
                levelState = LevelState.GameOver;
                hud.DisablePause();

                Action fail = () =>
                {
                    if (OnLevelFailed != null)
                    {
                        OnLevelFailed();
                    }

                    hud.gameOver.gameObject.SetActive(true);
                };

                if (failDialogue != null)
                {
                    failDialogue.freezeGameplay = true;
                    failDialogue.sustainFrozenGameplay = true;
                    failDialogue.PlayDialogue(fail);
                }
                else
                {
                    fail();
                }
            }
        }
    }

    // private void Start()
    // {
    //     //ApplySaveGameToCurrentLevel();
    // }

    //void ApplySaveGameToCurrentLevel()
    //{
    //    // Apply SaveGame to level
    //    SaveGame.Player.ApplyToPlayer(player);

    //    // Apply saveable objects
    //    var saveableObjects = FindObjectsOfType<SaveableObject>();
    //    Debug.Log("saveableObjects count: " + saveableObjects.Length.ToString());

    //    // Trees
    //    foreach (var saveId in SaveGame.Trees.Keys)
    //    {
    //        var treeSO = saveableObjects.FirstOrDefault(so => so.saveId == saveId);
    //        if (treeSO == null)
    //        {
    //            Debug.LogError("Failed to find Tree SaveableObject: " + saveId);
    //            continue;
    //        }
    //        var tree = treeSO.gameObject.GetComponent<Tree>();
    //        if (treeSO == null)
    //        {
    //            Debug.LogError("Failed to find get Tree component from SaveableObject: " + saveId);
    //            continue;
    //        }
    //        tree.currentState = SaveGame.Trees[saveId];
    //    }
    //}

    // internal void Save()
    // {
    //     //ApplyCurrentLevelToSaveGame();

    //     SaveGame.Save();
    // }

    //private void ApplyCurrentLevelToSaveGame()
    //{
    //    // Apply level to SaveGame
    //    SaveGame.Player.FromPlayer(player);

    //    // Extract saveable objects
    //    var saveableObjects = new List<SaveableObject>(FindObjectsOfType<SaveableObject>());

    //    // Trees
    //    SaveGame.Trees.Clear();
    //    foreach (var so in saveableObjects)
    //    {
    //        var tree = so.gameObject.GetComponent<Tree>();
    //        if (tree != null)
    //        {
    //            SaveGame.Trees.Add(so.saveId, tree.currentState);
    //        }
    //    }
    //}

    //internal void Load()
    //{
    //    // Just reload current level
    //    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    //}

    //internal void New()
    //{
    //    SaveGame.Clear();
    //    SaveGame.Load();
    //    Load();
    //}
}

public enum LevelState
{
    PrePlay,
    Playing,
    LevelComplete,
    GameOver
}
