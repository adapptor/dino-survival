﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SavePlayer
{
    public Vector3 Position;
    public float Health = Healthbar.kMaxHealth;
    public float Energy = EnergyBar.kMaxStamina;
    public DinoType CarryingEgg;

    public void FromPlayer(PlayerController player)
    {
        Position = player.transform.position;
        Health = player.currentHealth;
        Energy = Object.FindObjectOfType<EnergyManager>().energyBar.CurrentStamina;
        CarryingEgg = player.typeOfEggBeingHeld;
    }

    public void ApplyToPlayer(PlayerController player)
    {
        if (Position != Vector3.zero)
        {
            player.transform.position = Position;
        }
        player.currentHealth = Health;
        Object.FindObjectOfType<EnergyManager>().energyBar.CurrentStamina = Energy;
        player.typeOfEggBeingHeld = CarryingEgg;
    }
}
