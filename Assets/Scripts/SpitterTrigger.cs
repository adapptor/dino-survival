﻿using UnityEngine;

public class SpitterTrigger : MonoBehaviour
{
    public bool PlayerInTrigger { get; private set; }

    void Awake()
    {
        if (Application.isPlaying)
        {
            var meshRenderer = GetComponent<MeshRenderer>();
            if (meshRenderer != null)
            {
                meshRenderer.enabled = false;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D trigger)
    {
        if (trigger.tag == "Player")
        {
            PlayerInTrigger = true;
        }
    }

    void OnTriggerExit2D(Collider2D trigger)
    {
        if (trigger.tag == "Player")
        {
            PlayerInTrigger = false;
        }
    }
}
