﻿using UnityEngine;

public class RandomAudioSamplePlayer : MonoBehaviour
{
    [System.Serializable]
    public class AudioSourceWithDelay
    {
        public AudioSource audioSource;
        public float delaySeconds;
    }

    public float minSecsBetweenPlays = 1.0f;

    [Header("Optional: explicitly list audio sources with delays")]
    public AudioSourceWithDelay[] optionalExplicitSourcesWithDelays = new AudioSourceWithDelay[0];

    private AudioSource[] audioSamples;
    private float secsBetweenPlays = 0f;

    void Awake()
    {
        secsBetweenPlays = minSecsBetweenPlays;
        audioSamples = GetComponents<AudioSource>();

        if (optionalExplicitSourcesWithDelays.Length == 0)
        {
            optionalExplicitSourcesWithDelays = new AudioSourceWithDelay[audioSamples.Length];

            for (int i = 0; i < audioSamples.Length; ++i)
            {
                optionalExplicitSourcesWithDelays[i] = new AudioSourceWithDelay();
                optionalExplicitSourcesWithDelays[i].audioSource = audioSamples[i];
                optionalExplicitSourcesWithDelays[i].delaySeconds = 0f;
            }
        }
    }

    void Update()
    {
        secsBetweenPlays += Time.deltaTime;
    }

    public void Play()
    {
        if (secsBetweenPlays < minSecsBetweenPlays)
        {
            return;
        }

        secsBetweenPlays = 0;

        if (optionalExplicitSourcesWithDelays == null || optionalExplicitSourcesWithDelays.Length == 0)
        {
            return;
        }

        int sampleToPlay = Random.Range(0, optionalExplicitSourcesWithDelays.Length);

        var delay = optionalExplicitSourcesWithDelays[sampleToPlay].delaySeconds;
        var sample = optionalExplicitSourcesWithDelays[sampleToPlay].audioSource;

        if (delay > 0)
        {
            sample.PlayDelayed(delay);
        }
        else
        {
            // Negative delay meaning skip the first part of the clip
            sample.time = delay * -1;
            sample.Play();
        }
    }
}
