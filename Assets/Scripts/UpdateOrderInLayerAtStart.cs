﻿using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class UpdateOrderInLayerAtStart : MonoBehaviour
{
    private const float orderYMultiplier = -10f;
    public float orderHeightOffset = 0f;
    public float lineOrderOffset = 0f;

    public bool debugLog = false;

    [Header("Optional: include only these sprite renderers")]
    public SpriteRenderer[] optionalSpritesToUpdate = new SpriteRenderer[0];

    bool nothingToUpdateWarningShown;
    bool ambiguousSpriteWarningShown;

    void Start()
    {
        nothingToUpdateWarningShown = false;
        ambiguousSpriteWarningShown = false;
        UpdateOrder();
    }

    void Update()
    {
        if (!Application.isPlaying)
        {
            UpdateOrder();
        }
    }

    void UpdateOrder()
    {
        bool somethingUpdated = false;

        somethingUpdated = somethingUpdated || UpdateOrderInLayerSpriteRenderer();
        somethingUpdated = somethingUpdated || UpdateOrderInLayerMeshRenderer();
        somethingUpdated = somethingUpdated || UpdateOrderInLayerLineRenderer();

        if (!somethingUpdated && !nothingToUpdateWarningShown)
        {
            Debug.LogWarning("Could not find an appropriate sprite, mesh, or line renderer for UpdateOrderInLayerAtStart", this);
            nothingToUpdateWarningShown = true;
        }
    }

    bool UpdateOrderInLayerSpriteRenderer()
    {
        bool spriteUpdated = false;

        if (optionalSpritesToUpdate.Length > 0)
        {
            foreach (var sprite in optionalSpritesToUpdate)
            {
                spriteUpdated = true;
                sprite.sortingOrder = (int)((transform.position.y + orderHeightOffset) * orderYMultiplier);
            }
        }
        else
        {
            var sprites = GetComponents<SpriteRenderer>();
            if (sprites.Length == 0)
            {
                var list = new List<SpriteRenderer>();
                foreach (Transform child in transform)
                {
                    var sprite = child.GetComponent<SpriteRenderer>();
                    if (sprite != null)
                    {
                        list.Add(sprite);
                    }
                }

                sprites = list.ToArray();
            }

            bool spriteSelected = false;
            foreach (var sprite in sprites)
            {
                if (sprite.sortingLayerName != "Shadows")
                {
                    if (debugLog) Debug.Log(transform.name + " picked a sprite in layer: " + sprite.sortingLayerName + ", called: " + sprite.transform.name, sprite.transform);
                    if (spriteSelected && !ambiguousSpriteWarningShown)
                    {
                        Debug.LogWarning("Ambiguous sprite selection for UpdateOrderInLayerAtStart", this);
                        ambiguousSpriteWarningShown = true;
                        continue;
                    }
                    sprite.sortingOrder = (int)((transform.position.y + orderHeightOffset) * orderYMultiplier);
                    spriteUpdated = true;
                }
            }
        }

        return spriteUpdated;
    }

    bool UpdateOrderInLayerMeshRenderer()
    {
        var meshRenderer = GetComponent<MeshRenderer>();
        if (meshRenderer == null)
        {
            meshRenderer = GetComponentInChildren<MeshRenderer>();
        }

        if (meshRenderer == null)
        {
            return false;
        }

        meshRenderer.sortingOrder = (int)((transform.position.y + orderHeightOffset) * orderYMultiplier);
        return true;
    }

    bool UpdateOrderInLayerLineRenderer()
    {
        var lineRenderer = GetComponent<LineRenderer>();
        if (lineRenderer == null)
        {
            lineRenderer = GetComponentInChildren<LineRenderer>();
        }

        if (lineRenderer == null)
        {
            return false;
        }

        lineRenderer.sortingOrder = (int)((transform.position.y + orderHeightOffset + lineOrderOffset) * orderYMultiplier);
        return true;
    }
}