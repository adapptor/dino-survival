﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingCreditItem : MonoBehaviour
{
    const float SCREEN_HEIGHT = 640.0f;
    public float screenLengthsPerSecond = 1.0f;

    void Start()
    {
        SnapToBottomOfScreen();
    }

    void Update()
    {
        var transform = this.GetComponent<RectTransform>();
        var height = Mathf.Min(Screen.height, SCREEN_HEIGHT); 

        transform.Translate(Vector3.up * Time.deltaTime * this.screenLengthsPerSecond * height);

        if (HasScrolledOffscreen)
        {
            SnapToBottomOfScreen();
        }
    }

    void SnapToBottomOfScreen()
    {
        var transform = this.GetComponent<RectTransform>();
        transform.anchoredPosition = Vector2.zero + (Vector2.down * SCREEN_HEIGHT * 1.2f);
    }

    private bool HasScrolledOffscreen
    {
        get
        {
            var transform = this.GetComponent<RectTransform>();
            return transform.anchoredPosition.y > transform.rect.height;
        }
    }
}
