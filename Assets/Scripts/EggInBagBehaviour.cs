﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggInBagBehaviour : MonoBehaviour {

	public PlayerController player;
    public SpriteRenderer playerRenderer;
    public SpriteRenderer dilopoEggRendererBehind;
    public SpriteRenderer dilopoEggRendererFront;
    public SpriteRenderer dimetroEggRendererBehind;
    public SpriteRenderer dimetroEggRendererFront;
    public SpriteRenderer pachyEggRendererBehind;
    public SpriteRenderer pachyEggRendererFront;
    public SpriteRenderer raptorEggRendererBehind;
    public SpriteRenderer raptorEggRendererFront;

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        dilopoEggRendererBehind.enabled = player.typeOfEggBeingHeld == DinoType.Dilopo && !player.hasBackTurnedToCamera();
        dilopoEggRendererFront.enabled = player.typeOfEggBeingHeld == DinoType.Dilopo && player.hasBackTurnedToCamera();
        dimetroEggRendererBehind.enabled = player.typeOfEggBeingHeld == DinoType.Dimetro && !player.hasBackTurnedToCamera();
        dimetroEggRendererFront.enabled = player.typeOfEggBeingHeld == DinoType.Dimetro && player.hasBackTurnedToCamera();
        pachyEggRendererBehind.enabled = player.typeOfEggBeingHeld == DinoType.Pachy && !player.hasBackTurnedToCamera();
        pachyEggRendererFront.enabled = player.typeOfEggBeingHeld == DinoType.Pachy && player.hasBackTurnedToCamera();
        raptorEggRendererBehind.enabled = player.typeOfEggBeingHeld == DinoType.Raptor && !player.hasBackTurnedToCamera();
        raptorEggRendererFront.enabled = player.typeOfEggBeingHeld == DinoType.Raptor && player.hasBackTurnedToCamera();

        int renderOrderBehind = playerRenderer.sortingOrder - 1;
        int renderOrderFront = playerRenderer.sortingOrder + 1;

        dilopoEggRendererBehind.sortingOrder = renderOrderBehind;
        dimetroEggRendererBehind.sortingOrder = renderOrderBehind;
        pachyEggRendererBehind.sortingOrder = renderOrderBehind;
        raptorEggRendererBehind.sortingOrder = renderOrderBehind;

        dilopoEggRendererFront.sortingOrder = renderOrderFront;
        dimetroEggRendererFront.sortingOrder = renderOrderFront;
        pachyEggRendererFront.sortingOrder = renderOrderFront;
        raptorEggRendererFront.sortingOrder = renderOrderFront;
	}
}
