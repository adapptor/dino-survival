﻿using UnityEngine;
using UnityEngine.UI;

public class CountdownTimer : MonoBehaviour
{
    public float time;
    private Text timer;

    public bool NoTimeLimit { get; set; }

    // Use this for initialization
    void Start()
    {
        timer = GetComponent<Text>();
    }

    public void SetTimer(float seconds){
        time = seconds;
        NoTimeLimit = false;
    }

    public bool countDownFinished(){
        return time.Equals(0);
    }

    // Update is called once per frame
    void Update()
    {
        if (NoTimeLimit)
        {
            timer.text = "00:00";
            timer.color = Color.black;
            return;
        }

        if (time > 0)
        {
            time -= Time.deltaTime;

            int minutes = Mathf.FloorToInt(time / 60F);
            int seconds = Mathf.FloorToInt(time - minutes * 60);
            string niceTime = string.Format("{0:0}:{1:00}", minutes, seconds);

            if (time < 60)
            {
                timer.color = Color.red;
            } else {
                timer.color = Color.black;
            }

            timer.text = niceTime;
        } else {
            time = 0.0f;
            timer.text = "0:00";
        }
    }
}
