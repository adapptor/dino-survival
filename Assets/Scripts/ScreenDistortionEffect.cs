﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenDistortionEffect : MonoBehaviour
{
    public Shader distortShader;
    public float maxWarpAmount = 0.04f;
    public float maxInvertColoursAmount = 5.0f;
    public float warpSpeed = 2.0f;
    float easeInOutTime = 3.0f;
    Material distortEffect;
    bool distortionEnabled;
    float amount = 0;
    int tweenId;

    void Start()
    {
        distortEffect = new Material(distortShader);
    }

    public void SetDistortionEnabled(bool enable)
    {
        if (enable == distortionEnabled)
        {
            return;
        }

        distortionEnabled = enable;
        if (distortionEnabled)
        {
            // enable component and tween in
            this.enabled = true;
            LeanTween.cancel(tweenId);
            tweenId = LeanTween.value(amount, 1.0f, easeInOutTime)
                .setOnUpdate(UpdateAmount)
                .uniqueId;
        }
        else
        {
            // tween out then disable component
            LeanTween.cancel(tweenId);
            tweenId = LeanTween.value(amount, 0.0f, easeInOutTime)
                .setOnUpdate(UpdateAmount)
                .setOnComplete(() => this.enabled = false)
                .uniqueId;
        }
    }

    void UpdateAmount(float amount)
    {
        this.amount = amount;
        distortEffect.SetFloat("_Amount", amount * maxWarpAmount);
    }

    private void Update()
    {
        distortEffect.SetFloat("_InvertedAmount", amount * maxInvertColoursAmount); // * Mathf.Sin(Time.timeSinceLevelLoad * 3.0f));
        distortEffect.SetFloat("_Speed", warpSpeed);
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, distortEffect);
    }
}
