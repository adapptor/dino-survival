﻿using UnityEngine;

interface ITapable
{
    // Is this game object allowed to tap on this item?
    bool CanTap(GameObject sender);

    // Trigger tap behaviour.
    void OnTap();
}
