﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverMenu : MonoBehaviour {

    public AllLevelsAsset allLevels;
    public Button nextButton;
    public Button restartButton;
    public Button exitButton;

    internal Hud hud;

    void Start()
    {
        hud = FindObjectOfType<Hud>();
        var nextLevel = GetNextLevel();
        if (nextLevel != null)
        {
            nextButton.onClick.AddListener(() => SceneManager.LoadScene(nextLevel.fileName));
        }
        else
        {
            nextButton.onClick.AddListener(() => hud.tbc.gameObject.SetActive(true));
        }

        restartButton.onClick.AddListener(() =>
        {
            restartButton.GetComponentInChildren<Text>().text = "Loading...";
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        });

        exitButton.onClick.AddListener(() => SceneManager.LoadScene("main_menu"));
    }

    // Try and get the next level (if there is one).
    LevelDef GetNextLevel()
    {
        var currentLevelName = SceneManager.GetActiveScene().name;
        var index = 0;

        LevelDef nextLevel = null;

        // Find the next level (if there is one)
        foreach (var level in this.allLevels.levels)
        {
            if (level.fileName.Equals(currentLevelName) &&
                this.allLevels.levels.Length >= index + 2)
            {
                nextLevel = this.allLevels.levels[index + 1];
                break;
            }
            index++;
        }

        return nextLevel;
    }
}
