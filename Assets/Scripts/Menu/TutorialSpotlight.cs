﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TutorialSpotlight : MonoBehaviour
{
    RectTransform rectXfm;
    RectTransform canvasRect;
    const float SCALE_OUT = 30.0f;
    const float SCALE_DEFAULT = 5.0f;
    const float ANIM_TIME = 0.5f;
    int zoomTweenId;

    void Awake()
    {
        rectXfm = (RectTransform)this.transform;
        rectXfm.localScale = Vector3.one * SCALE_OUT;
        var canvas = GetComponentInParent<Canvas>();
        canvasRect = canvas.GetComponent<RectTransform>();
    }

    public void ZoomIn()
    {
        ZoomTo(SCALE_DEFAULT);
    }

    private LTDescr ZoomTo(float zoom)
    {
        LeanTween.cancel(zoomTweenId);
        var desc = LeanTween.scale(rectXfm, Vector3.one * zoom, ANIM_TIME).setUseEstimatedTime(true);
        zoomTweenId = desc.uniqueId;
        return desc;
    }

    public void Focus(Vector3 target)
    {
        gameObject.SetActive(true);
        LeanTween.move(rectXfm, target, ANIM_TIME).setUseEstimatedTime(true);
        ZoomIn();
    }

    public void FocusWorld(Vector3 target)
    {
        if (canvasRect == null)
        {
            var canvas = GetComponentInParent<Canvas>();
            canvasRect = canvas.GetComponent<RectTransform>();
        }
        Focus(WorldToCanvasPosition(target));
    }

    public void FocusCanvas(RectTransform targetRectXfm)
    {
        Vector2 outPos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(rectXfm, targetRectXfm.position, null, out outPos);
        Focus(outPos);
    }

    internal void Deactivate()
    {
        ZoomTo(SCALE_OUT).setOnComplete(DeactivateGameObj);
        LeanTween.move(rectXfm, Vector3.zero, ANIM_TIME).setUseEstimatedTime(true);
    }

    private void DeactivateGameObj()
    {
        this.gameObject.SetActive(false);
    }

    private Vector2 WorldToCanvasPosition(Vector3 position)
    {
        //Vector position (percentage from 0 to 1) considering camera size.
        //For example (0,0) is lower left, middle is (0.5,0.5)
        Vector2 temp = Camera.main.WorldToViewportPoint(position);

        //Calculate position considering our percentage, using our canvas size
        //So if canvas size is (1100,500), and percentage is (0.5,0.5), current value will be (550,250)
        temp.x *= canvasRect.sizeDelta.x;
        temp.y *= canvasRect.sizeDelta.y;

        //The result is ready, but, this result is correct if canvas recttransform pivot is 0,0 - left lower corner.
        //But in reality its middle (0.5,0.5) by default, so we remove the amount considering cavnas rectransform pivot.
        //We could multiply with constant 0.5, but we will actually read the value, so if custom rect transform is passed(with custom pivot) , 
        //returned value will still be correct.

        temp.x -= canvasRect.sizeDelta.x * canvasRect.pivot.x;
        temp.y -= canvasRect.sizeDelta.y * canvasRect.pivot.y;

        return temp;
    }
}
