﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hud : MonoBehaviour
{
    public Button pauseButton;
    public PauseMenu pauseMenu;
    public GameOverMenu gameOver;
    public GameObject touchControls;
    public SpeechCanvas speechCanvas;
    public GameObject objectives;
    // Group for in-game UI to be enabled when the game starts
    public GameObject hudElements;
    public SingleJoystick joystick;
    public Healthbar healthBar;
    public EnergyBar energyBar;
    public EnergyManager energyManager;
    public CountdownTimer timer;
    public ZoomButton zoom;
    public ToBeContinuedScreen tbc;

    private bool gameplayAlreadyFrozen = false;

    public void DisablePause()
    {
        pauseButton.interactable = false;
        pauseMenu.gameObject.SetActive(false);
    }

    public void TogglePause()
    {
        var isPaused = !pauseMenu.gameObject.activeSelf;
        pauseMenu.gameObject.SetActive(isPaused);
        pauseButton.interactable = !isPaused;

        if (isPaused)
        {
            if (Time.timeScale < 0.5f)
            {
                gameplayAlreadyFrozen = true;
            }
            else
            {
                gameplayAlreadyFrozen = false;
                Time.timeScale = 0.0f;
            }
        }
        else
        {
            if (!gameplayAlreadyFrozen)
            {
                Time.timeScale = 1.0f;
            }
        }
    }

	// Use this for initialization
	void Start () {
        pauseMenu.gameObject.SetActive(false);
    }
}