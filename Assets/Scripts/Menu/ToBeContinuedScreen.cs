﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ToBeContinuedScreen : MonoBehaviour {

    void Start()
    {
        Time.timeScale = 0.0f;
    }

    public void OnClick()
    {
        SceneManager.LoadScene("credits");
    }
}
