﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public AllLevelsAsset allLevels;
    public UILevel levelPrefab;
    public RectTransform levelsList;

    private List<UILevel> uiLevels = new List<UILevel>();

    // Use this for initialization
    void Start()
    {
        Time.timeScale = 1f;

        bool didInit = false;
        for (int i = 0; i < allLevels.levels.Length; i++)
        {
            var levelDef = allLevels.levels[i];
            var levelState = SaveGame.Instance.GetLevel(levelDef.fileName);
            if (i == 0 && levelState.isLocked)
            {
                didInit = true;
                levelState.isLocked = false;
            }
            var uiLevel = Instantiate(levelPrefab, levelsList);
            uiLevels.Add(uiLevel);
            uiLevel.Init(levelDef, levelState);
            uiLevel.button.onClick.AddListener(() =>
            {
                uiLevel.levelName.text = "Loading...";
                SaveGame.Instance.currentLevel = levelDef.fileName;
                SaveGame.Instance.Save();

                if (SaveGame.Instance.hasSeenStory)
                {
                    SceneManager.LoadScene(SaveGame.Instance.currentLevel);
                }
                else
                {
                    SceneManager.LoadScene("Storyboard");
                }
            });

            if (!levelState.isLocked)
            {
                SaveGame.Instance.currentLevel = levelDef.fileName;
                EventSystem.current.SetSelectedGameObject(uiLevel.button.gameObject, null);
            }
        }

        if (didInit)
        {
            SaveGame.Instance.Save();
        }
    }

    public void ShowStory()
    {
        SaveGame.Instance.hasSeenStory = true;
        SaveGame.Instance.Save();

        SceneManager.LoadScene("Storyboard");
    }

    public void ShowCredits()
    {
        SceneManager.LoadScene("credits");
    }

    public void ShowPrivacy()
    {
         Application.OpenURL("http://blayzes-game.adapptor.com.au");
    }

    void OnGUI()
    {
        //Delete all of the PlayerPrefs settings by pressing this Button
        // if (GUI.Button(new Rect(10, 10, 150, 20), "Delete Player Prefs"))
        // {
        //     SaveGame.Reset();
        // }

        // Unlock all levels
        // if (GUI.Button(new Rect(10, 40, 150, 20), "Unlock all levels"))
        // {
        //     foreach(var level in uiLevels)
        //     {
        //         level.Unlock();
        //     }
        // }
    }
}
