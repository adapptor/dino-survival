using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuMainButton : MonoBehaviour
{

    public Text buttonText;

    private bool hasBeenClicked = false;

    public void TaskOnClick()
    {
        if (!hasBeenClicked)
        {
            buttonText.text = "LOADING...";
            UnityEngine.SceneManagement.SceneManager.LoadScene("main_menu");
            hasBeenClicked = true;
        }

    }
}
