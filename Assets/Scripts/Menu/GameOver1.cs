﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameOver1 : MonoBehaviour {
    

	// Use this for initialization
	void Start () {

        var score = SaveGame.Instance.Score;

        GameObject DimetroScore = GameObject.Find("DimetroScore");
        DimetroScore.GetComponent<CountUp>().targetValue = score.GetDinoScore(DinoType.Dimetro);
        GameObject RaptorScore = GameObject.Find("RaptorScore");
        RaptorScore.GetComponent<CountUp>().targetValue = score.GetDinoScore(DinoType.Raptor);
        GameObject PatchyScore = GameObject.Find("PatchyScore");
        PatchyScore.GetComponent<CountUp>().targetValue = score.GetDinoScore(DinoType.Pachy);
	}
}
