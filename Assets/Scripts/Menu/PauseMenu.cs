﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour {

    public Hud hud;
    public Button continueButton;
    public Button restartButton;
    public Button exitButton;

    // Testing save level state.
    public Button saveButton;
    public Button loadButton;
    public Button newButton;

    // Use this for initialization
    void Start ()
    {
        continueButton.onClick.AddListener(hud.TogglePause);
        restartButton.onClick.AddListener(() => {
			restartButton.GetComponentInChildren<Text>().text = "Loading...";
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		});
        exitButton.onClick.AddListener(() => SceneManager.LoadScene("main_menu"));

        // // Testing save level state.
        // saveButton.onClick.AddListener(levelMgr.Save);
        // loadButton.onClick.AddListener(levelMgr.Load);
        // newButton.onClick.AddListener(levelMgr.New);
	}
}
