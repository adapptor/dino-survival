﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILevel : MonoBehaviour
{
    public Button button;
    public Text levelName;
    public Image locked;
    public Image complete;

    public void Init(LevelDef levelDef, SaveLevel levelState)
    {
        levelName.text = levelDef.displayName;
        locked.gameObject.SetActive(levelState.isLocked);
        complete.gameObject.SetActive(levelState.isComplete);
        button.interactable = !levelState.isLocked;
    }

    public void Unlock()
    {
        locked.gameObject.SetActive(false);
        complete.gameObject.SetActive(true);
        button.interactable = true;
    }

    public void SetSelected(bool isSelected)
    {
    }
}
