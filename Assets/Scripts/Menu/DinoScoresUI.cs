﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DinoScoresUI : MonoBehaviour
{
    public Text heading;
    public DinoScoreUIEntry[] entries;
    const float OFFSCREEN_Y = 400.0f;
    const float SLIDE_TIME = 0.6f;
    const float BOUNCE_TIME = 0.5f;
    const int BOUNCE_COUNT = 2;
    public static float AnimTime
    {
        get
        {
            return SLIDE_TIME + BOUNCE_TIME * BOUNCE_COUNT;
        }
    }

    private LevelManager levelMgr;

    void Start()
    {
        levelMgr = FindObjectOfType<LevelManager>();
        if (levelMgr == null) {
            Debug.LogWarning("Can't find the LevelManager, make sure you have the LevelManager prefab in your scene");
            this.gameObject.SetActive(false);
            return;
        }
        if (levelMgr.levelDef == null) {
            this.gameObject.SetActive(false);
            return;
        }
        levelMgr.OnScoreChanged += OnScoreChanged;
        levelMgr.OnLevelComplete += OnLevelComplete;
        levelMgr.OnLevelFailed += OnLevelFailed;

        foreach (var entry in entries)
        {
            bool hidden = levelMgr.levelDef.hiddenDinos.Contains(entry.dinoType);
            entry.image.color = hidden ? Color.black : Color.white;
            entry.score.gameObject.SetActive(!hidden);
            if (hidden)
            {
                entry.name.text = "????";
            }
        }
        UpdateScores(levelMgr);

        SetY(OFFSCREEN_Y);
        if (levelMgr.levelDef.showObjectivesAtStart)
        {
            SlideIn(DinoType.None);
            Invoke("SlideOut", 5.0f);
        }
    }

    void Update()
    {
        if (levelMgr.levelState == LevelState.Playing && Input.anyKeyDown)
        {
            SlideOut();
        }
    }

    void SetY(float y)
    {
        var rt = this.transform as RectTransform;
        var pos = rt.anchoredPosition;
        pos.y = y;
        rt.anchoredPosition = pos;
    }

    int slideInTween;
    int slideOutTween;
    
    void SlideIn(DinoType bounceDinoScore)
    {
        LeanTween.cancel(slideInTween);
        slideInTween = SlideTo(0.0f)
            .setOnComplete(() => BounceScore(bounceDinoScore))
            .setUseEstimatedTime(true)
            .uniqueId;
    }

    void SlideOut()
    {
        LeanTween.cancel(slideOutTween);
        slideOutTween = SlideTo(OFFSCREEN_Y).uniqueId;
    }

    LTDescr SlideTo(float y)
    {
        return LeanTween.moveY(this.transform as RectTransform, y, SLIDE_TIME)
            .setUseEstimatedTime(true)
            .setEaseInOutSine();
    }

    void OnScoreChanged(LevelManager levelMgr, DinoType dinoType, bool didMeetObjective, bool allObjectivesMet)
    {
        UpdateScores(levelMgr);
        SlideIn(dinoType);
    }

    void OnLevelComplete()
    {
        heading.text = "Level Complete!";
        // No need to slide in as this happens after OnScoreChanged anyway.
    }

    public void OnLevelFailed()
    {
        heading.text = "Game Over!";
        SlideIn(DinoType.None);
    }

    void UpdateScores(LevelManager levelMgr)
    {
        foreach (var entry in entries)
        {
            // Get objective for this dino type
            var objective = levelMgr.levelDef.objectives.FirstOrDefault(o => o.dino == entry.dinoType);
            if (objective == null)
            {
                entry.score.text = "";
            }
            else
            {
                int eggs = levelMgr.levelScore.GetDinoScore(entry.dinoType);
                entry.score.text = string.Format("{0}/{1}", eggs, objective.eggCount);
            }
        }
    }

    void BounceScore(DinoType dinoType)
    {
        var entry = entries.FirstOrDefault(e => e.dinoType == dinoType);
        if (entry != null)
        {
            LeanTween.scale(entry.score.rectTransform, Vector3.one * 1.25f, BOUNCE_TIME)
                .setEasePunch()
                .setLoopCount(BOUNCE_COUNT)
                .setUseEstimatedTime(true);
        }
    }
}

[System.Serializable]
public class DinoScoreUIEntry
{
    public DinoType dinoType;
    public Image image;
    public Text name;
    public Text score;
}

