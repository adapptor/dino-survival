﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialUI : MonoBehaviour
{
    public TutorialSpotlight spotlight;
    public RectTransform joystick;
    public RectTransform health;
    public RectTransform energy;
    public RectTransform enerbyButton;
    public RectTransform zoomButton;
    public RectTransform timer;
}
