﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialObject : MonoBehaviour
{
    public string[] texts;

    Tutorial tutorial;

    private void Start()
    {
        tutorial = FindObjectOfType<Tutorial>();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            tutorial.OnObjectDiscovered(this);
            // deactivate so we only trigger once.
            gameObject.SetActive(false);
        }
    }
}
