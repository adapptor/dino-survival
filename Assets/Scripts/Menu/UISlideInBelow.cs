﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISlideInBelow : MonoBehaviour {
	public float endPos = 0;
	public float slideTime = 1f;

	public RectTransform objectToSlide;

	// Use this for initialization
	void Start () {
		LeanTween.moveY (objectToSlide, endPos, slideTime);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
