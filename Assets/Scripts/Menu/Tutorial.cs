﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    public GameObject tutorialUIPrefab;
    public Transform egg;
    public Transform teleporter;

    enum TutorialState
    {
        Intro,
        JoystickFocus,
        JoystickUse,
        EnergyButtonFocus,
        EnergyButtonUse,
        EnergyBar,
        HealthBar,
        Teleporter,
        Energy,
        Ptera,
        Dino,
        Egg,
        Done
    }

    TutorialState currentState = TutorialState.Intro;

    PlayerController player;
    Hud hud;
    TutorialUI tutorialUI;
    CameraController camController;
    float timePerformingAction = 0;

    // Use this for initialization
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
        hud = FindObjectOfType<Hud>();
        camController = FindObjectOfType<CameraController>();

        // Instantiate tutorial UI
        tutorialUI = Instantiate(tutorialUIPrefab, hud.transform).GetComponent<TutorialUI>();

        // Set tutorial UI to be underneath the speech canvas
        var speechCanvasIndex = hud.speechCanvas.transform.GetSiblingIndex();
        tutorialUI.transform.SetSiblingIndex(speechCanvasIndex - 1);


        EnterState(TutorialState.Intro);
    }

    private void Update()
    {
        UpdateState();
    }

    private void UpdateState()
    {
        switch (currentState)
        {
            case TutorialState.JoystickFocus:
                if (hud.joystick.GetInputDirection().magnitude > 0.7f)
                {
                    EnterState(TutorialState.JoystickUse);
                }
                break;
            case TutorialState.JoystickUse:
                if (player.isMoving)
                {
                    timePerformingAction += Time.deltaTime;
                    if (timePerformingAction > 2.0f)
                    {
                        EnterState(TutorialState.EnergyButtonFocus);
                    }
                }
                break;
            case TutorialState.EnergyButtonFocus:
                if (hud.energyManager.usingEnergy)
                {
                    EnterState(TutorialState.EnergyButtonUse);
                }
                break;
            case TutorialState.EnergyButtonUse:
                if (hud.energyManager.usingEnergy && player.isMoving)
                {
                    timePerformingAction += Time.deltaTime;
                    if (timePerformingAction > 1.0f)
                    {
                        EnterState(TutorialState.Energy);
                    }
                }
                break;
        }
    }

    IEnumerator EnterStateDelayed(TutorialState state, float delay)
    {
        yield return new WaitForSecondsRealtime(delay);
        EnterState(state);
    }

    void EnterState(TutorialState state)
    {
        timePerformingAction = 0.0f;
        currentState = state;
        switch (currentState)
        {
            case TutorialState.Intro:
                Time.timeScale = 0.0f;
                SetSpeech(
                    new string[] {
                        "Blayze, welcome to the triassic era. I hope the time ride was not too bumpy!",
                        "First let's get our bearings around here."
                    },
                    true,
                    () => EnterState(TutorialState.JoystickFocus));
                break;
            case TutorialState.JoystickFocus:
                Time.timeScale = 0.0f;
                tutorialUI.spotlight.Focus(tutorialUI.joystick.anchoredPosition);
                SetSpeech("This is your joystick. Touch it and drag to move around.", false);
                break;
            case TutorialState.JoystickUse:
                Time.timeScale = 1.0f;
                tutorialUI.spotlight.Deactivate();
                SetSpeech("Cool, now walk around a bit to get the hang of it!", false);
                break;
            case TutorialState.EnergyButtonFocus:
                Time.timeScale = 0.0f;
                tutorialUI.spotlight.Focus(tutorialUI.enerbyButton.anchoredPosition);
                SetSpeech("Good, but you want to go faster right? Press this Energy button.", false);
                break;
            case TutorialState.EnergyButtonUse:
                Time.timeScale = 1.0f;
                tutorialUI.spotlight.Deactivate();
                SetSpeech("Practice your running. You'll need to use the joystick and the Energy button at the same time.", false);
                break;
            case TutorialState.Energy:
                Time.timeScale = 0.0f;
                tutorialUI.spotlight.Focus(tutorialUI.energy.anchoredPosition);
                SetSpeech("Very good, you probably noticed that running uses up your energy. Don't stress, it builds back up over time.",
                    true,
                    () => EnterState(TutorialState.Egg));
                break;
            // case TutorialState.HealthBar:
            //     Time.timeScale = 0.0f;
            //     tutorialUI.spotlight.Focus(tutorialUI.health.anchoredPosition);
            //     SetSpeech("This other ",
            //         () => EnterState(TutorialState.HealthBar));
            //     break;
            case TutorialState.Egg:
                Time.timeScale = 0.0f;
                tutorialUI.spotlight.Deactivate();
                camController.SetTarget(egg, () =>
                {
                    tutorialUI.spotlight.FocusWorld(egg.position);
                    SetSpeech("See that dinosaur nest over there? Your mission is to steal the egg.",
                        true,
                        () => EnterState(TutorialState.Teleporter));
                });
                break;
            case TutorialState.Teleporter:
                Time.timeScale = 0.0f;
                tutorialUI.spotlight.Deactivate();
                camController.SetTarget(teleporter, () =>
                {
                    tutorialUI.spotlight.FocusWorld(teleporter.position);
                    SetSpeech("Once you have an egg, bring it back to the time machine so we can send it back to the future.",
                        true,
                        () => EnterState(TutorialState.Done));
                });
                break;
            case TutorialState.Done:
                Time.timeScale = 1.0f;
                camController.SetTarget(null, null);
                tutorialUI.spotlight.Deactivate();
                SetSpeech("Oh, and by the way, watch out for angry parent dinosaurs. They can be very dangerous!", true);
                break;
        }
    }

    private void SetSpeech(string text, bool showButtons, Action onDismiss = null)
    {
        SetSpeech( new string[] { text }, showButtons, onDismiss);
    }

    private void SetSpeech(string[] texts, bool showButtons, Action onDismiss = null)
    {
        hud.speechCanvas.CanSkip = showButtons;
        hud.speechCanvas.PlayDialogue(texts, onDismiss);
    }

    internal void OnObjectDiscovered(TutorialObject tutorialObject)
    {
        tutorialUI.spotlight.FocusWorld(tutorialObject.transform.position);
    }
}
