﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderParticlesOnLayer : MonoBehaviour {

	public string layer;

	// Use this for initialization
	void Start () {
		GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingLayerName = layer;
	}
}
