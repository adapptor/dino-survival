﻿using UnityEngine;
using System.Collections;

public class SlowArea : MonoBehaviour
{
    public float speedModifier = 0.5f;
    private PlayerController playerController;
    private Collider2D slowAreaCollider;
    private Collider2D playerCollider;
    private bool isCollidingThisFrame = false;
    private bool wasCollidingLastFrame = false;

    void Awake()
    {
        playerController = FindObjectOfType<PlayerController>();
        playerCollider = playerController.GetComponent<CircleCollider2D>();

        slowAreaCollider = GetComponent<PolygonCollider2D>();
        var placeholder = transform.GetComponentInAncestors<PlaceholderCommon>();

        if (placeholder != null)
        {
            if (slowAreaCollider != null)
            {
                slowAreaCollider.enabled = false;
            }

            slowAreaCollider = placeholder.GetComponent<PolygonCollider2D>();
        }
    }

    void Update() {
        if (slowAreaCollider == null || playerCollider == null) {
            return;
        }
        if (slowAreaCollider.IsTouching(playerCollider)) {
            isCollidingThisFrame = true;
        } else {
            isCollidingThisFrame = false;
        }

        if (isCollidingThisFrame != wasCollidingLastFrame) {
            if (isCollidingThisFrame) {
                onTrigger();
            } else {
                onTriggerExit();
            }
        }

        wasCollidingLastFrame = isCollidingThisFrame;
    }

    private void onTrigger()
    {
        if (!playerController.slowAreas.Contains(this)) {
            playerController.slowAreas.Add(this);
        }
    }

    private void onTriggerExit()
    {
        playerController.slowAreas.Remove(this);
    }
}
