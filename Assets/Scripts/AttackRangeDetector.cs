﻿using UnityEngine;

public class AttackRangeDetector : MonoBehaviour
{
    public bool TargetInDamageRange { get; private set; }

    void OnTriggerEnter2D(Collider2D trigger)
    {
        if (trigger.tag == "AttackTarget")
        {
            TargetInDamageRange = true;
        }
    }

    void OnTriggerExit2D(Collider2D trigger)
    {
        if (trigger.tag == "AttackTarget")
        {
            TargetInDamageRange = false;
        }
    }
}
