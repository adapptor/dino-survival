﻿using System.Collections;
using UnityEngine;

public class Egg : MonoBehaviour {

    private int tweenId;
    private bool spawning;
    public GameObject dinosaurPrefab;
    public float timer;
    public DinoType type;
    private float initialScale;
    private AudioSource hatchingAudio;

    private void Awake()
    {
        initialScale = this.transform.localScale.x;
        hatchingAudio = GetComponent<AudioSource>();
    }

    public void spawnDino()
    {
        if (!spawning)
            StartCoroutine(spawn());
    }

    private IEnumerator spawn() {
        
        spawning = true;

        float bulgingScale = initialScale + 0.2f;
        tweenId = LeanTween
            .scale(gameObject, Vector3.one * bulgingScale, 1.0f)
            .setEase(LeanTweenType.easeOutQuad)
            .setDelay(0.1f)
            .setLoopPingPong()
            .id;

        yield return new WaitForSeconds(timer * 60.0f);

        LeanTween.cancel(tweenId);

        tweenId = LeanTween
            .scale(gameObject, Vector3.one * bulgingScale, 0.5f)
            .setEase(LeanTweenType.easeOutQuad)
            .id;

        while (LeanTween.isTweening(tweenId)) {
            yield return new WaitForEndOfFrame();
        }

        hatchingAudio.Play();

        while (hatchingAudio.isPlaying) {
            yield return new WaitForEndOfFrame();
        }

        var dinoInstance = Instantiate(dinosaurPrefab, transform.position, Quaternion.identity);
        var dinoStateMachine = dinoInstance.GetComponent<Dino>();
        dinoStateMachine.maxScale = 0.2f;
        dinoStateMachine.minScale = 0.15f;
        dinoStateMachine.BaseState = dinoInstance.GetComponent<IdleState>();
        dinoStateMachine.StartStateMachine();

        Destroy(gameObject);

        yield return null;
    }

    public bool isHatching() {
        return spawning;
    }
}
