﻿using UnityEngine;

public abstract class Attack : MoveAbout, IPreferredState
{
    public GameObject targetMarkerPrefab;
    public float attackSpeedMultiplier = 2;

    public DefendArea DefendArea { get; set; }
    public float AttackStrength { get; set; }

    protected AttackRangeDetector attackRangeDetector;
    protected TargetDetector targetDetector;
    protected Collider2D dinoCollider;

    TargetMarker targetMarker;

    protected float pickDestinationDelay = 0f;

    public override void Awake()
    {
        base.Awake();
        attackRangeDetector = GetComponentInChildren<AttackRangeDetector>();
        targetDetector = GetComponentInChildren<TargetDetector>();
        dinoCollider = GetComponent<Collider2D>();
    }

    public virtual bool ShouldEnterState()
    {
        var target = targetDetector.Target;

        return
            target != null &&
            target.isAlive &&
            DefendArea != null &&
            DefendArea.PlayerInDefendArea() &&
            DefendArea.ColliderInDefendArea(dinoCollider);
    }

    public virtual bool ShouldLeaveState()
    {
        var target = targetDetector.Target;

        return !DefendArea.ColliderInDefendArea(dinoCollider) ||
            (!GetTargetMarkerPosition().HasValue &&
            (target == null ||
            !target.isAlive ||
            !DefendArea.PlayerInDefendArea()));
    }

    // Should only ever be called immediately after ShouldEnterState has returned true
    public abstract void EnterState();

    // Should only ever be called immediately after ShouldLeaveState has returned false
    public abstract void DoState();

    public abstract void LeaveState();

    public void DealDamage(PlayerController target)
    {
        target.DealDamage(AttackStrength);
    }

    protected Vector2 GetAdjustedDestination(Vector2 targetPosition)
    {
        var proposed = targetPosition + (Vector2)(transform.position - attackRangeDetector.transform.position);

        var obstacle = ObstacleInPath(proposed, targetPosition);

        if (obstacle.HasValue)
        {
            if (drawDecisions) Debug.DrawLine(targetPosition, proposed, Color.red, 10f);
            proposed = PointJustBefore(1f, obstacle.Value, targetPosition);
            if (drawDecisions) Debug.DrawLine(targetPosition, proposed, Color.yellow, 10f);
        }
        else
        {
            if (drawDecisions) Debug.DrawLine(targetPosition, proposed, Color.white, 10f);
        }

        // The destination which would line the attack range detector up with the target
        return proposed;
    }

    // Pass null to hide the marker
    protected void SetTargetMarkerPosition(Vector2? position)
    {
        if (targetMarker == null)
        {
            targetMarker = Instantiate(targetMarkerPrefab).GetComponent<TargetMarker>();
        }

        if (position.HasValue)
        {
            targetMarker.gameObject.SetActive(true);
            targetMarker.transform.position = position.Value;
        }
        else
        {
            targetMarker.gameObject.SetActive(false);
        }
    }

    protected Vector2? GetTargetMarkerPosition()
    {
        if (targetMarker == null || !targetMarker.gameObject.activeSelf)
        {
            return null;
        }
        else
        {
            return (Vector2)targetMarker.transform.position;
        }
    }

    protected void PickValidDestination(Vector2 invalidDestination, Vector2 obstacle)
    {
        var validDestination = FindNearestValidDestination(invalidDestination, 10, 4);
        if (validDestination != null)
        {
            // Use the nearest valid destination
            destination = validDestination.Value;
        }
        else
        {
            // No valid destination can be found, use the position a little way before the obstacle
            destination = PointJustBefore(0.2f, obstacle);

            if (drawDecisions) Debug.DrawLine(transform.position, destination, Color.magenta, 10f);
        }
    }
}
