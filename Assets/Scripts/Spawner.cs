﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Linq;
using System.Linq.Expressions;

public class Spawner : MonoBehaviour
{
    public GameObject[] pointsOfInterestToSpawn;
    public GameObject[] prefabsToSpawn;
    public int pointsOfInterestCount;
    public int count;
    public float rangeX = 120.0f;
    public float rangeY = 120.0f;
    public float minScale = 0.8f;
    public float maxScale = 1.5f;

    private const float spacing = 0.8f;
    private const float orderYMultiplier = -10f;

    Vector3 GetRandomPos()
    {
        return new Vector3(Random.Range(-0.5f, 0.5f) * rangeX, Random.Range(-0.5f, 0.5f) * rangeY, 0.0f);
    }

    Vector3 GetRandomScaleVec()
    {
        return Vector3.one * Random.Range(minScale, maxScale);
    }

    // Use this for initialization
    void Awake()
    {
        LeanTween.init(1000);

        for (int i = 0; i < pointsOfInterestCount; ++i)
        {
            spawn(pointsOfInterestToSpawn[Random.Range(0, pointsOfInterestToSpawn.Length)], false);
        }

        for (int i = 0; i < count; ++i)
        {
            spawn(prefabsToSpawn[Random.Range(0, prefabsToSpawn.Length)]);
        }
    }

    void spawn(GameObject prefab, bool randomiseScale = true)
    {
        var placed = false;

        var pos = Vector3.zero;
        var scale = Vector3.one;

        var allObjects = FindObjectsOfType<GameObject>();
        var allPOIs = GameObject.FindGameObjectsWithTag("POI");

        while (!placed)
        {
            pos = GetRandomPos();
            scale = GetRandomScaleVec();

            Vector2 pos2D = pos;
            placed = !allPOIs.Any(x => x.GetComponent<Collider2D>().bounds.Contains(pos2D)) &&
                !allObjects.Any(x =>
            {
                return ((x.transform.position - pos).magnitude <= spacing);
            });
        }

        var newObj = Instantiate(prefab, this.transform, false);
        newObj.transform.localPosition = pos;

        if (randomiseScale)
        {
            newObj.transform.localScale = scale;
        }

        var spriteRenderer = newObj.GetComponent<SpriteRenderer>();
        if (spriteRenderer == null)
        {
            spriteRenderer = newObj.GetComponentInChildren<SpriteRenderer>();
        }

        if (spriteRenderer)
        {
            spriteRenderer.sortingOrder = (int)(pos.y * orderYMultiplier);
        }
        
    }
}
