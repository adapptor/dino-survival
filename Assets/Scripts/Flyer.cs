﻿using UnityEngine;

public class Flyer : MonoBehaviour
{
    public bool facingLeft = false;
    public float speed = 2f;
    public GameObject exclamation;
    public AudioSource flyAwaySound;
    public RandomAudioSamplePlayer squawkSounds;

    Vector2 destination;
    WaypointPath circlingPath = null;
    int? circlingWaypoint = null;
    WaypointPath showingWayPath = null;
    int? showingWayWaypoint = null;
    bool showingWayBack = false;
    Vector2 showingWayFinalDestination;
    bool flyingAway = false;

    public void CircleCheckpoint(WaypointPath path)
    {
        circlingPath = path;
        circlingWaypoint = 0;
    }

    public void ShowTheWay(Checkpoint checkpoint, WaypointPath showingWayPath = null, bool showingWayBack = false)
    {
        squawkSounds.Play();
        this.showingWayPath = showingWayPath;
        this.showingWayBack = showingWayBack;

        if (this.showingWayPath != null)
        {
            showingWayWaypoint = this.showingWayBack ?
                this.showingWayPath.GetLastWaypointId() :
                this.showingWayPath.GetFirstWaypointId();

            destination = this.showingWayPath.GetWaypointPosition(showingWayWaypoint.Value);
            showingWayFinalDestination = checkpoint.transform.position;
        }
        else
        {
            destination = checkpoint.transform.position;
        }

        UpdateFacing();
        circlingWaypoint = null;
        exclamation.SetActive(true);

        LeanTween
            .rotateZ(exclamation, 5f, 0.1f)
            .setEase(LeanTweenType.easeShake)
            .setRepeat(4)
            .setOnComplete(() => exclamation.SetActive(false));
    }

    // Update is called once per frame
    void Update()
    {
        float step = speed * Time.deltaTime;

        Vector2 moveVector = (destination - (Vector2)transform.position).normalized;
        transform.position = (Vector2)transform.position + moveVector * step;

        if (Vector2.Distance(transform.position, destination) <= Nav.navigationOffset)
        {
            if (circlingWaypoint.HasValue)
            {
                circlingWaypoint = circlingPath.GetNextWaypointId(circlingWaypoint.Value);
                destination = circlingPath.GetWaypointPosition(circlingWaypoint.Value);
                UpdateFacing();
            }
            else if (showingWayPath != null)
            {
                if ((!showingWayBack && showingWayPath.IsLastWaypoint(showingWayWaypoint.Value)) ||
                    (showingWayBack && showingWayPath.IsFirstWaypoint(showingWayWaypoint.Value)))
                {
                    destination = showingWayFinalDestination;
                    showingWayPath = null;
                }
                else
                {
                    showingWayWaypoint = showingWayBack ?
                        showingWayPath.GetPreviousWaypointId(showingWayWaypoint.Value) :
                        showingWayPath.GetNextWaypointId(showingWayWaypoint.Value);

                    destination = showingWayPath.GetWaypointPosition(showingWayWaypoint.Value);
                }

                UpdateFacing();
            }
            else
            {
                if (flyingAway)
                {
                    Destroy(gameObject);
                }
                else
                {
                    var newDirection = 
                        moveVector.x > 0f ? 
                              new Vector2(moveVector.y * -1, moveVector.x) : // Clockwise 90
                              new Vector2(moveVector.y, moveVector.x * -1); // Counter-Clockwise 90

                    destination = (Vector2)transform.position + newDirection * 50f;
                    UpdateFacing();
                    speed = speed * 2f;
                    flyAwaySound.time = 0.8f;
                    flyAwaySound.Play();
                    flyingAway = true;
                }
            }
        }
    }

    public void Awake()
    {
        destination = transform.position;
        exclamation.SetActive(false);
    }

    protected void UpdateFacing(float threshold = 0f)
    {
        if (facingLeft && destination.x - transform.position.x > threshold)
        {
            facingLeft = false;
        }
        else if (!facingLeft && transform.position.x - destination.x > threshold)
        {
            facingLeft = true;
        }

        var scale = transform.localScale;
        if (facingLeft && scale.x > 0)
        {
            scale.x *= -1;
        }
        else if (!facingLeft && scale.x < 0)
        {
            scale.x *= -1;
        }
        transform.localScale = scale;
    }
}
