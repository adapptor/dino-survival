﻿using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[ExecuteInEditMode]
public class ColliderRenderer : MonoBehaviour
{
    public enum Outline { None, Open, Closed }
    public Outline outline = Outline.Closed;

    PolygonCollider2D pc2;
    LineRenderer lineRenderer;

    void Start()
    {
        RenderCollider();
    }

#if UNITY_EDITOR
    void Update()
    {
        if (!Application.isPlaying)
        {
            RenderCollider();
        }
    }
#endif

    void RenderCollider()
    {
        if (pc2 == null)
        {
            pc2 = GetComponent<PolygonCollider2D>();
            var placeholder = transform.GetComponentInAncestors<PlaceholderCommon>();

            if (placeholder != null)
            {
                var placeholderCollider = placeholder.GetComponent<PolygonCollider2D>();

                if (placeholderCollider != null)
                {
                    if (pc2 != null)
                    {
                        pc2.enabled = false;
                    }

                    pc2 = placeholderCollider;
                }
            }
        }

        if (pc2 != null)
        {
            RenderMesh();
            RenderLine();
        }
    }

    void RenderMesh()
    {
        //Render thing
        int pointCount = 0;
        pointCount = pc2.GetTotalPointCount();
        MeshFilter mf = GetComponent<MeshFilter>();
        Mesh mesh = new Mesh();
        Vector2[] points = pc2.points;
        Vector3[] vertices = new Vector3[pointCount];
        Vector2[] uv = new Vector2[pointCount];
        for (int j = 0; j < pointCount; j++)
        {
            Vector2 actual = points[j];
            vertices[j] = new Vector3(actual.x, actual.y, 0);
            uv[j] = actual;
        }
        Triangulator tr = new Triangulator(points);
        int[] triangles = tr.Triangulate();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uv;
        mf.mesh = mesh;
        //Render thing
    }

    void RenderLine()
    {
        if (lineRenderer == null) lineRenderer = GetComponent<LineRenderer>();

        if (lineRenderer == null)
        {
            return;
        }

        if (outline == Outline.None)
        {
            lineRenderer.enabled = false;
            return;
        }
        else
        {
            lineRenderer.enabled = true;
        }

        //Render thing
        int pointCount = 0;
        pointCount = pc2.GetTotalPointCount();
        if (pointCount < 1) return;

        lineRenderer.positionCount = pointCount;
        lineRenderer.loop = outline == Outline.Closed;

        Vector2[] points = pc2.points;
        for (int j = 0; j < pointCount; j++)
        {
            Vector2 actual = points[j];
            lineRenderer.SetPosition(j, new Vector3(actual.x, actual.y, 0));
        }
    }
}
