﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    private GameObject[] allDinos;
    public AudioSource mainBackingTrack;
    public AudioSource attackingTrack;
    public float fadeTimeToAttack = 2f;

    private float backingTrackVolume;
    private float attackTrackVolume;
    private bool playingBackingTrack = true;
    private Coroutine fadeInCoroutine;
    private Coroutine fadeOutCoroutine;

    // Use this for initialization
    void Start()
    {
        allDinos = GameObject.FindGameObjectsWithTag("Dinosaur");
        backingTrackVolume = mainBackingTrack.volume;
        attackTrackVolume = attackingTrack.volume;
        fadeBackToNormalTrack();
    }

    void Update()
    {
        bool isAnyDinoAttacking = false;

        if (allDinos != null && !(allDinos.Length == 0))
        {
            foreach (GameObject dinoObj in allDinos)
            {
                if (dinoObj != null)
                {
                    Dino dino = dinoObj.GetComponent<Dino>();
                    if (dino != null && dino.inPreferredState)
                    {
                        isAnyDinoAttacking = true;
                        break;
                    }
                }
                else
                {
                    // workaround for issue where on start, the dino objects can be null in the array
                    allDinos = GameObject.FindGameObjectsWithTag("Dinosaur");
                }
            }
        }

        if (isAnyDinoAttacking && playingBackingTrack)
        {
            fadeIntoAttackTrack();
        }
        else if (!isAnyDinoAttacking && !playingBackingTrack)
        {
            fadeBackToNormalTrack();
        }
    }

    void fadeIntoAttackTrack()
    {
        playingBackingTrack = false;
        if (!attackingTrack.isPlaying)
        {
            attackingTrack.Play();
        }
        stopCurrentFadeCoroutines();
        fadeOutCoroutine = StartCoroutine(fadeTrackOut(mainBackingTrack, fadeTimeToAttack));
        fadeInCoroutine = StartCoroutine(fadeTrackIn(attackingTrack, fadeTimeToAttack, attackTrackVolume));
    }

    void fadeBackToNormalTrack()
    {
        playingBackingTrack = true;
        if (!mainBackingTrack.isPlaying)
        {
            mainBackingTrack.Play();
        }
        stopCurrentFadeCoroutines();
        fadeOutCoroutine = StartCoroutine(fadeTrackOut(attackingTrack, fadeTimeToAttack));
        fadeInCoroutine = StartCoroutine(fadeTrackIn(mainBackingTrack, fadeTimeToAttack, backingTrackVolume));
    }

    private IEnumerator fadeTrackOut(AudioSource audioSource, float fadeTime)
    {
        float startVolume = audioSource.volume;

        while (audioSource.volume > 0)
        {
            audioSource.volume -= startVolume * Time.deltaTime / fadeTime;
            yield return null;
        }

        audioSource.Stop();
    }


    private IEnumerator fadeTrackIn(AudioSource audioSource, float fadeTime, float targetVolume)
    {
        while (audioSource.volume < targetVolume)
        {
            audioSource.volume += targetVolume * Time.deltaTime / fadeTime;
            yield return null;
        }
    }

    private void stopCurrentFadeCoroutines()
    {
        if (fadeOutCoroutine != null)
        {
            StopCoroutine(fadeOutCoroutine);
        }
        if (fadeInCoroutine != null)
        {
            StopCoroutine(fadeInCoroutine);
        }
    }

}
