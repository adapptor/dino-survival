﻿using UnityEngine;

public class DamageArea : MonoBehaviour
{
    public float damagePerSecond = 5f;
    private PlayerController playerController;
    private Collider2D damageAreaCollider;
    private Collider2D playerCollider;
    private bool isCollidingThisFrame = false;
    private bool wasCollidingLastFrame = false;

    void Awake()
    {
        playerController = FindObjectOfType<PlayerController>();
        playerCollider = playerController.GetComponent<CircleCollider2D>();

        damageAreaCollider = GetComponent<PolygonCollider2D>();
        var placeholder = transform.GetComponentInAncestors<PlaceholderCommon>();

        if (placeholder != null)
        {
            if (damageAreaCollider != null)
            {
                damageAreaCollider.enabled = false;
            }

            damageAreaCollider = placeholder.GetComponent<PolygonCollider2D>();
        }
    }

    void Update() {
        if (damageAreaCollider == null || playerCollider == null) {
            return;
        }
        if (damageAreaCollider.IsTouching(playerCollider)) {
            isCollidingThisFrame = true;
        } else {
            isCollidingThisFrame = false;
        }

        if (isCollidingThisFrame != wasCollidingLastFrame) {
            if (isCollidingThisFrame) {
                onTrigger();
            } else {
                onTriggerExit();
            }
        }

        wasCollidingLastFrame = isCollidingThisFrame;
    }

    private void onTrigger()
    {
        if (!playerController.damageAreas.Contains(this)) {
            playerController.damageAreas.Add(this);
        }
    }

    private void onTriggerExit()
    {
        playerController.damageAreas.Remove(this);
    }
}
