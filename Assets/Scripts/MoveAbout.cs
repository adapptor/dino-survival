﻿using UnityEngine;

public class MoveAbout : MonoBehaviour
{
    public bool drawDecisions = false;
    public bool defaultOrientationIsLeft = false;
    public float moonwalkToleranceSeconds = 0.5f;

    public float Speed { get; set; }

    protected Vector2 destination;
    protected Animator animator;
    protected Rigidbody2D body;

    int navigationMask;
    int impassibleLayerMask;
    const string impassableLayer = "Impassable";
    const string navObstacleLayer = "NavigationObstacles";

    bool facingLeft = false;
    bool moonwalking = false;

    float moonwalkingElapsedTime = 0f;

    public virtual void Awake()
    {
        animator = GetComponentInChildren<Animator>();
        body = GetComponent<Rigidbody2D>();
        impassibleLayerMask = LayerMask.NameToLayer(impassableLayer);
        navigationMask = LayerMask.GetMask(new string[] { impassableLayer, navObstacleLayer });
        facingLeft = defaultOrientationIsLeft;
    }

    // Returns the remaining distance to destination
    protected void MoveTowardDestination(float speedMultiplier = 1f)
    {
        if (moonwalking)
        {
            moonwalkingElapsedTime += Time.deltaTime;
        }
        else
        {
            moonwalkingElapsedTime = 0f;
        }

        if (moonwalkingElapsedTime > moonwalkToleranceSeconds)
        {
            //Force the moonwalking to stop by updating facing with a 0 tolerance
            UpdateFacing();
            moonwalkingElapsedTime = 0f;
        }

        var direction = destination - (Vector2)transform.position;

        if (drawDecisions)
        {
            Debug.DrawLine(destination + new Vector2(-0.1f, -0.1f), destination + new Vector2(0.1f, 0.1f), Color.cyan);
            Debug.DrawLine(destination + new Vector2(-0.1f, 0.1f), destination + new Vector2(0.1f, -0.1f), Color.cyan);
        }

        if (direction.magnitude < 0.1f)
        {
            body.velocity = Vector2.zero;
        }
        else
        {
            var speed = Speed > 0 ? Speed : 2f;
            body.velocity = direction.normalized * speed * speedMultiplier;
        }
    }

    protected float RemainingDistanceToDestination()
    {
        return Vector2.Distance(transform.position, destination);
    }

    protected void UpdateFacing(float threshold = 0f)
    {
        var diff = destination.x - transform.position.x;
        if (diff > 0f)
        {
            if (diff > threshold)
            {
                facingLeft = false;
                moonwalking = false;
            }
            else
            {
                moonwalking = true;
            }
        }
        else
        {
            if (diff < threshold * -1)
            {
                facingLeft = true;
                moonwalking = false;
            }
            else
            {
                moonwalking = true;
            }
        }

        bool flip = (defaultOrientationIsLeft && !facingLeft) || (!defaultOrientationIsLeft && facingLeft);

        var scale = transform.localScale;
        if (flip && scale.x > 0)
        {
            scale.x *= -1;
        }
        else if (!flip && scale.x < 0)
        {
            scale.x *= -1;
        }
        transform.localScale = scale;
    }

    protected Vector2? ObstacleInPath(Vector2 targetDest, Vector2? startPos = null)
    {
        var currentPos = startPos.HasValue ? startPos.Value : (Vector2)transform.position;
        var offset = targetDest - currentPos;
        var hit = Physics2D.Raycast(currentPos, offset, offset.magnitude, navigationMask);

        Vector2? hitPoint = null;

        if (hit.collider != null)
        {
            if (hit.collider.gameObject.layer == impassibleLayerMask)
            {
                Vector2 diff = currentPos - hit.point;
                if (diff.magnitude < Nav.navigationOffset)
                {
                    hitPoint = currentPos;
                }
                else
                {
                    // This still won't help if the ray is at a very acute angle to the collider
                    hitPoint = hit.point + diff.normalized * Nav.navigationOffset;
                }
            }
            else
            {
                hitPoint = hit.point;
            }
        }

        if (drawDecisions && hitPoint != null)
        {
            Debug.DrawLine(hitPoint.Value + new Vector2(-0.1f, -0.1f), hitPoint.Value + new Vector2(0.1f, 0.1f), Color.red, 10f);
            Debug.DrawLine(hitPoint.Value + new Vector2(-0.1f, 0.1f), hitPoint.Value + new Vector2(0.1f, -0.1f), Color.red, 10f);
        }

        return hitPoint;
    }

    protected Vector2 PointJustBefore(float magnitude, Vector2 destination, Vector2? startPos = null)
    {
        var currentPos = startPos.HasValue ? startPos.Value : (Vector2)transform.position;

        return destination + (currentPos - destination).normalized * magnitude;
    }

    protected Vector2? FindNearestValidDestination(Vector2 invalidDestination, int incrementDegrees, int attemptsInEitherDirection)
    {
        var attempts = new Vector2?[attemptsInEitherDirection * 2];

        var currentPos = (Vector2)transform.position;
        var destVector = invalidDestination - currentPos;

        for (int i = 0; i < attemptsInEitherDirection * 2; ++i)
        {
            var angleMultiplier = i < attemptsInEitherDirection ? i + 1 : (i - attemptsInEitherDirection + 1) * -1;

            var attemptVector = (Vector2)(Quaternion.AngleAxis(angleMultiplier * incrementDegrees, Vector3.forward) * destVector);
            var attemptDest = currentPos + attemptVector;

            var obstacle = ObstacleInPath(attemptDest);

            var validDest = obstacle.HasValue ? obstacle.Value : attemptDest;
            if (drawDecisions)
            {
                Debug.DrawLine(currentPos, validDest, Color.yellow, 10f);
                Debug.DrawLine(validDest, attemptDest, Color.red, 10f);
            }

            if (ObstacleInPath(invalidDestination, validDest) == null)
            {
                if (drawDecisions) Debug.DrawLine(validDest, invalidDestination, Color.yellow, 10f);
                attempts[i] = Nav.NearestPointOnLine(currentPos, validDest, invalidDestination);
            }
            else
            {
                if (drawDecisions) Debug.DrawLine(validDest, invalidDestination, Color.red, 10f);
                attempts[i] = null;
            }
        }

        var shortestDistance = destVector.magnitude;
        Vector2? bestDestination = null;

        foreach (var attempt in attempts)
        {
            if (attempt.HasValue)
            {
                var attemptDist = (invalidDestination - attempt.Value).magnitude;
                if (attemptDist < shortestDistance)
                {
                    bestDestination = attempt.Value;
                    shortestDistance = attemptDist;
                }
            }
        }

        if (drawDecisions && bestDestination.HasValue)
        {
            Debug.DrawLine(currentPos, bestDestination.Value, Color.green, 10f);
            Debug.DrawLine(bestDestination.Value, invalidDestination, Color.green, 10f);
        }

        return bestDestination;
    }
}
