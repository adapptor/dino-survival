﻿using UnityEngine;

[ExecuteInEditMode]
public class Spitter : MonoBehaviour
{
    public Transform venomSpawnPoint;
    public Transform venomTarget;
    public GameObject venomPrefab;
    public Animator animator;
    public SpitterTrigger spitterTrigger;
    public RandomAudioSamplePlayer spitSounds;

    bool spitting = false;
    bool facingLeft = false;

    VenomBall venomBallInstance = null;

    void Awake()
    {
        animator.SetBool("still", true);
    }

    public void SetTrigger(SpitterTrigger trigger)
    {
        spitterTrigger.gameObject.SetActive(false);
        spitterTrigger = trigger;
    }

    public void SetTarget(Transform target)
    {
        venomTarget.gameObject.SetActive(false);
        venomTarget = target;
    }

    void Start()
    {
        UpdateFacing();

        var angle = Mathf.Abs(Vector3.Angle(
            facingLeft ? Vector3.left : Vector3.right,
            (venomTarget.position - venomSpawnPoint.position).normalized));
        if (angle > 30f)
        {
            Debug.LogWarning("The angle between your spitter and its target (" + angle + ") is too steep, it will look odd", this);
        }
    }

	void Update()
	{
        if (!Application.isPlaying)
        {
            UpdateFacing();
            return;
        }

        if (spitting)
        {
            if (venomBallInstance != null && venomBallInstance.State == VenomBall.VenomState.Done)
            {
                Destroy(venomBallInstance.gameObject);
                venomBallInstance = null;
                spitting = false;
            }
        }
        else if (spitterTrigger.PlayerInTrigger)
        {
            spitting = true;
            animator.SetBool("still", false);
            spitSounds.Play();
        }
	}

    void UpdateFacing()
    {
        facingLeft = transform.position.x > venomTarget.position.x;

        transform.localScale = facingLeft ? new Vector3(-1, 1, 1) : Vector3.one;
    }

	public void Spit()
    {
        var instance = Instantiate(venomPrefab, venomSpawnPoint.position, Quaternion.identity);
        instance.transform.localScale = facingLeft ? new Vector3(-1, 1, 1) : Vector3.one;

        venomBallInstance = instance.GetComponent<VenomBall>();
        var venomVector = venomTarget.position - venomSpawnPoint.position;
        venomBallInstance.NormalisedDirection = venomVector.normalized;
        venomBallInstance.MaxDistance = venomVector.magnitude;
    }

    public void SpitComplete()
    {
        animator.SetBool("still", true);
    }
}
