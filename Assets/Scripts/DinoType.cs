﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DinoType
{
	None,
	Dimetro,
	Pachy,
	Raptor,
	Dilopo,
}
