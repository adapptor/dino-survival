﻿using System.Collections.Generic;
using UnityEngine;

// Fills all attached colliders with spawned objects (e.g. trees, foliage etc).
[ExecuteInEditMode]
public class ImpassableTerrain : MonoBehaviour
{
    // Array of game objects to draw from when filling the area.
    public GameObject[] prefabs;

    // Initial spacing between spawned objects.
    public float spacing = 1.0f;

    // Percentage chance of spawning an object at grid point (e.g. 0.8 =
    // 80%). This can be used to control how 'dense' the foliage is.
    public float density = 0.8f;

    // Upper limit of a random distance to apply to each spawned object.
    public float noise = 0.5f;

    // Distance from the edge of the collider in which objects will not spawn
    // (zero indicates objects spawn upto the edge).
    public float inset = 0.0f;

    public void Awake()
    {
        if (Application.isPlaying)
        {
            Generate();

            // Turn OFF mesh renderer as this is an editor-only feature
            var meshRenderer = GetComponent<MeshRenderer>();
            if (meshRenderer != null)
            {
                meshRenderer.enabled = false;
            }
        }
    }

    public void Generate()
    {
        Clear();

        var colliders = GetComponents<Collider2D>();
        var placeholder = transform.GetComponentInAncestors<PlaceholderCommon>();

        if (placeholder != null)
        {
            foreach (var coll in colliders)
            {
                coll.enabled = false;
            }

            colliders = placeholder.GetComponents<Collider2D>();
        }

        foreach (var coll in colliders)
        {
            FillArea(coll);
        }
    }

    public void Clear()
    {
        var children = new List<GameObject>();
        foreach (Transform child in transform)
        {
            children.Add(child.gameObject);
        }

        foreach (var go in children)
        {
            DestroyImmediate(go);
        }
    }

    // Find the closest point on the edge of the collider to the provided point.
    private static Vector2 ClosestPoint(
        Collider2D collider,
        GameObject proxy,
        Vector2 point)
    {
        // Move the proxy object to the target position.
        proxy.transform.position = point;

        // Test collision and return the nearest point.
        var c = proxy.GetComponent<Collider2D>();
        var d = collider.Distance(c);
        return d.pointA;
    }

    // Fill an area represented by a collider with objects. The process is as follows:
    //
    //   1. Scan across the rectangular bounds of the area using a step size of
    //   `spacing`.
    // 
    //   2. Check if the point is within the area bounds and not too close to
    //   the edge of the collider (see `inset`).
    //
    //   3. Spawn the object if all tests pass and then nudge it in a random
    //   direction (see `noise`).
    // 
    private void FillArea(Collider2D area)
    {
        var bounds = area.bounds;
        var origin = bounds.center;
        var width = bounds.size.x;
        var height = bounds.size.y;

        // Create a temporary object that we will use as a proxy for a world
        // position so that we can find the nearest point using `ClosestPoint`.
        var proxy = new GameObject("fill-area-proxy");
        {
            var c = proxy.AddComponent<CircleCollider2D>();
            c.radius = 0.1f;
        }

        for (float x = 0; x < width; x += spacing)
        {
            for (float y = 0; y < height; y += spacing)
            {
                var px = origin.x + x - width / 2 + Random.Range(0.0f, noise) * spacing;
                var py = origin.y + y - height / 2 + Random.Range(0.0f, noise) * spacing; 

                var pos = new Vector3(px, py, 0);

                if (Random.Range(0.0f, 1.0f) < density && area.OverlapPoint(pos))
                {
                    if (inset <= 0.0f || Vector3.Distance(pos, ClosestPoint(area, proxy, pos)) > inset)
                    {
                        var go = SpawnRandomPrefab();
                    
                        go.transform.position = pos;

                        var spriteRenderer =
                            go.GetComponent<SpriteRenderer>() ??
                            go.GetComponentInChildren<SpriteRenderer>();

                        if (spriteRenderer)
                        {
                            spriteRenderer.sortingOrder = (int)pos.y;
                        }                        
                    }
                }
            }
        }

        // Destroy the proxy object now that we are done.
        DestroyImmediate(proxy);
    }

    private GameObject SpawnRandomPrefab()
    {
        int n = Random.Range(0, prefabs.Length);
        return Instantiate(prefabs[n], transform, true);
    }
}
