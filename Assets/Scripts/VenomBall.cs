﻿using UnityEngine;

public class VenomBall : MonoBehaviour
{
    public enum VenomState
    {
        Flying,
        Splashing,
        Done
    }
    
    public ParticleSystem splash;
    public SpriteRenderer sprite;
    public CircleCollider2D circleCollider;
    public float speed = 10f;
    public RandomAudioSamplePlayer splatSounds;

    public Vector3 NormalisedDirection { get; set; }
    public float MaxDistance { get; set; }
    public VenomState State { get; private set; }

    Vector3 origin;
    float distanceTravelled = 0f;

    void Awake()
    {
        splash.Stop();
        origin = transform.position;
    }

    void Update()
    {
        if (State == VenomState.Flying)
        {
            distanceTravelled += speed * Time.deltaTime;
            transform.position = origin + NormalisedDirection * distanceTravelled;

            if (distanceTravelled >= MaxDistance)
            {
                Splash();
            }
        }
        else if (State == VenomState.Splashing)
        {
            if (!splash.isEmitting)
            {
                State = VenomState.Done;
            }
        }
    }

    void Splash()
    {
        splatSounds.Play();
        splash.Play();
        sprite.enabled = false;
        State = VenomState.Splashing;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "AttackTarget")
        {
            var player = collision.transform.parent.GetComponent<PlayerController>();
            if (player != null && player.isAlive)
            {
                circleCollider.enabled = false;
                player.InflictPoison(3f);
                Splash();
            }
        }
    }
}
