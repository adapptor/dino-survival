﻿using System;
using UnityEngine;

public class HudPickup : MonoBehaviour
{
    public enum PickupType
    {
        Health,
        Energy
    }

    public PickupType pickupType;
    public Action OnComplete { get; set; }

    int? scaleTweenId;

	void Start()
    {
        var destinationObjectName = ObjectNameForPickupType(pickupType);
        
        var destinationPanel = GameObject.Find(destinationObjectName).GetComponent<RectTransform>();

        Vector2 outDest;
        RectTransformUtility.ScreenPointToLocalPointInRectangle((RectTransform)transform, destinationPanel.position, null, out outDest);

        LeanTween
            .move((RectTransform)transform, outDest, 0.4f)
            .setEase(LeanTweenType.easeOutQuad)
            .setOnComplete(Replenish);

        scaleTweenId = LeanTween
            .scale((RectTransform)transform, Vector3.one * 3f, 0.2f)
            .setEase(LeanTweenType.easeOutQuad)
            .setOnComplete(ScaleBack)
            .id;
	}

    string ObjectNameForPickupType(PickupType type)
    {
        switch (type)
        {
            case PickupType.Health: return "healthbar_bg";
            default: return "energybar_bg";
        }
    }

    void ScaleBack()
    {
        scaleTweenId = LeanTween
            .scale((RectTransform)transform, Vector3.one, 0.2f)
            .setEase(LeanTweenType.easeInQuad)
            .setOnComplete(() => scaleTweenId = null)
            .id;
    }

    void Replenish()
    {
        if (scaleTweenId.HasValue)
        {
            LeanTween.cancel(scaleTweenId.Value);
        }

        if (OnComplete != null)
        {
            OnComplete();
        }

        Destroy(gameObject);
    }
}
