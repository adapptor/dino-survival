﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinoTeleporter : MonoBehaviour {
	public float dormantMinAlhpa = 0.3f;
	public float dormantMaxAlhpa = 0.7f;
	public float dormantTransitionTime = 1.5f;

	public float teleportingMinAlpha = 0.3f;
	public float teleportingMaxAlpha = 1.0f;
	public float teleportTime = 2.0f;

	public Renderer dormantMainRenderer;
	public Renderer dormantOverlayRenderer;
	public Renderer teleportingMainRenderer;
	public Renderer teleportingOverlayRenderer;

	private AudioSource teleportAudio;

	enum TeleporterState {
		DORMANT,
		TELEPORTING,
	}

	private Color startColor;
	private Color endColor;

	// Use this for initialization
	void Start () {
		teleportAudio = GetComponent<AudioSource>();
		TransitionToDormant ();
	}

	private void TransitionToDormant() {
		dormantMainRenderer.enabled = true;
		dormantOverlayRenderer.enabled = true;
		teleportingMainRenderer.enabled = false;
		teleportingOverlayRenderer.enabled = false;

		//begin dormat fading in/out animation
		startColor = dormantOverlayRenderer.material.color;
		startColor.a = dormantMinAlhpa;

		endColor = dormantOverlayRenderer.material.color;
		endColor.a = dormantMaxAlhpa;

		LeanTween.value (
			dormantOverlayRenderer.gameObject, 
			startColor, 
			endColor, 
			dormantTransitionTime).setOnUpdate ((Color val) => { 
				dormantOverlayRenderer.material.color = val;
			}).setLoopPingPong (-1);

		GetComponent<ShakeObject>().StopShake();
	}

	public void TransitionToTeleporting() {

		teleportAudio.Play();

		dormantMainRenderer.enabled = false;
		dormantOverlayRenderer.enabled = false;
		teleportingMainRenderer.enabled = true;
		teleportingOverlayRenderer.enabled = true;

		//begin dormat fading in/out animation
		startColor = teleportingOverlayRenderer.material.color;
		startColor.a = teleportingMinAlpha;

		endColor = teleportingOverlayRenderer.material.color;
		endColor.a = teleportingMaxAlpha;

		LeanTween.value (
			teleportingOverlayRenderer.gameObject, 
			startColor, 
			endColor, 
			teleportTime).setOnUpdate ((Color val) => { 
				teleportingOverlayRenderer.material.color = val;
			});

		GetComponent<ShakeObject>().StartShake();
		CancelInvoke("TransitionToDormant");
		Invoke("TransitionToDormant", teleportTime);
	}
}
