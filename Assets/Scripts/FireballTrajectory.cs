﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballTrajectory : MonoBehaviour
{
    public GameObject explosionPrefab;
    public GameObject targetMarkerPrefab;

    float maxLife = 1.6f;
    Vector2 gravity = new Vector2(0f, -9.8f);
    Vector2 velocity = new Vector2(-1f, 10f);
    Vector2 initPos;
    Vector2 lastPos;
    float initTime;
    private GameObject targetMarker;
    float targetMarkerDuration = 1.5f;

    // Set by object instantiating the fireball
    internal Vector2 target;

    private UpdateOrderInLayer orderScript;

    // Use this for initialization
    void Start()
    {
        orderScript = GetComponent<UpdateOrderInLayer>();
        initPos = this.transform.position;
        lastPos = initPos;
        initTime = Time.timeSinceLevelLoad;
        
        // y = p.y + v.y * t + a * t * t
        // target.y = initPos.y + this.velocity.y * maxLife + this.gravity.y * maxLife * maxLife;
        this.velocity.y = (target.y - initPos.y - this.gravity.y * maxLife * maxLife) / maxLife;

        // x = p.x + v.x * t;
        // target.x = initPos.x + this.velocity.x * maxLife;
        this.velocity.x = (target.x - initPos.x) / maxLife;

        // Give player 1 second of seeing target to get out of there!
        Invoke("AddTarget", Mathf.Max(0.0f, maxLife - targetMarkerDuration));
        Invoke("Explode", maxLife);
    }

    void AddTarget()
    {
        if (targetMarkerPrefab != null)
        {
            targetMarker = Instantiate(targetMarkerPrefab, target, Quaternion.identity);
        }
    }

    void Explode()
    {
        if (explosionPrefab != null)
        {
            Instantiate(explosionPrefab, this.transform.position, Quaternion.identity);
        }
        Destroy(gameObject);
        Destroy(targetMarker);
    }

    // Update is called once per frame
    void Update()
    {
        float t = Time.timeSinceLevelLoad - initTime;
        //Vector2 vel = velocity + gravity * t;
        Vector2 pos = initPos + velocity * t + gravity * t * t;
        Vector2 vel = pos - lastPos;
        lastPos = pos;

        transform.position = pos;
        if (vel.magnitude > 0.01f)
        {
            var dirn = vel.normalized;
            transform.eulerAngles = new Vector3(0.0f, 0.0f, Mathf.Atan2(dirn.y, dirn.x) * Mathf.Rad2Deg);
        }

        var height = pos.y - target.y;
        orderScript.orderHeightOffset = -height;
    }
}
