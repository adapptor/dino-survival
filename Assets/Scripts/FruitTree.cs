﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FruitTree : MonoBehaviour
{
    public Fruit[] fruitSlots = new Fruit[0];
    public Transform centre;
    public RandomAudioSamplePlayer knockSounds;
    public int numberOfFruitToMaintain = 0;
    public float outOfRangeDistance = 5f;
    public float spawnAppleDelay = 3f;

    [Range(1, 9)]
    public int minResistance = 2;
    [Range(2, 10)]
    public int maxResistance = 5;

    int hits = 0;
    int hitResistance = 0;
    int? shakeTweenId;
    int treeHitMask;

    float elapsedSecondsSinceLastHit = 0f;
    float elapsedSecondsSinceLastSpawn = 0f;
    PlayerController player;
    float screenDiagonalWithBuffer = 0f;
    bool playerLeftTheScene = true;
    int maintainedFruit = 0;

    void Awake()
    {
        numberOfFruitToMaintain = Mathf.Min(numberOfFruitToMaintain, fruitSlots.Length);
        minResistance = Mathf.Min(minResistance, maxResistance);
        hitResistance = Random.Range(minResistance, maxResistance + 1);
        treeHitMask = LayerMask.GetMask(new string[] { "Clickable" });
    }

    void Start()
    {
        Vector3 lowerLeft = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        Vector3 upperRight = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));

        // Distance from the player (centre) to the screen corner plus a bit of buffer
        screenDiagonalWithBuffer = Vector3.Distance(lowerLeft, upperRight) / 2f + 3f;

        player = GameObject.FindWithTag("Player").GetComponent<PlayerController>();

        foreach (var fruit in fruitSlots)
        {
            if (fruit.transform.position.y > transform.position.y)
            {
                Debug.LogWarning("Fruit position must be lower than tree in Y direction", fruit.transform);
            }
        }

        var goneFruit = new List<Fruit>(fruitSlots);

        for (var i = 0; i < numberOfFruitToMaintain; ++i)
        {
            var index = Random.Range(0, goneFruit.Count);
            var fruit = goneFruit[index];
            goneFruit.RemoveAt(index);
            ResetFruitOnTree(fruit);
            maintainedFruit++;
        }
    }

    public void Update()
    {
        // Avoid tap passthrough of game HuD elements
        if (EventSystem.current != null && !EventSystem.current.IsPointerOverGameObject())
        {
            if (Input.GetMouseButtonDown(0))
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                var queriesHitTriggersOldValue = Physics2D.queriesHitTriggers;
                Physics2D.queriesHitTriggers = true;

                var hit = Physics2D.GetRayIntersection(ray, Mathf.Infinity, treeHitMask);

                Physics2D.queriesHitTriggers = queriesHitTriggersOldValue;

                if (hit.transform == transform)
                {
                    OnTap();
                }
            }
        }

        // Don't allow hits to carry over into newly spawned apples
        elapsedSecondsSinceLastHit += Time.deltaTime;
        if (elapsedSecondsSinceLastHit > spawnAppleDelay)
        {
            hits = 0;
        }

        // While the player is in range of the tree, check to see if any apples have been picked up and if they've only
        // just come in range of the tree drop an apple if there aren't any on the ground already
        if (Vector2.Distance(player.transform.position, centre.position) < outOfRangeDistance)
        {
            bool anyDroppedFruit = false;
            maintainedFruit = 0;

            foreach (var fruit in fruitSlots)
            {
                if (fruit.State != Fruit.FruitState.Gone)
                {
                    maintainedFruit++;

                    if (fruit.State == Fruit.FruitState.Dropped || fruit.State == Fruit.FruitState.Dropping)
                    {
                        anyDroppedFruit = true;
                    }
                }
            }

            if (playerLeftTheScene && !anyDroppedFruit)
            {
                DropFruit();
            }

            playerLeftTheScene = false;
        }

        if (Vector2.Distance(player.transform.position, centre.position) > screenDiagonalWithBuffer &&
            maintainedFruit < numberOfFruitToMaintain)
        {
            playerLeftTheScene = true;
            elapsedSecondsSinceLastSpawn += Time.deltaTime;

            if (elapsedSecondsSinceLastSpawn > spawnAppleDelay)
            {
                elapsedSecondsSinceLastSpawn = 0f;
                var goneFruit = new List<Fruit>();

                foreach (var fruit in fruitSlots)
                {
                    if (fruit.State == Fruit.FruitState.Gone)
                    {
                        goneFruit.Add(fruit);
                    }
                }

                ResetFruitOnTree(goneFruit[Random.Range(0, goneFruit.Count)]);
                maintainedFruit++;
            }
        }
    }

    public void OnTap()
    {
        knockSounds.Play();

        if (shakeTweenId.HasValue)
        {
            LeanTween.cancel(shakeTweenId.Value);
            transform.eulerAngles = Vector3.zero;
        }

        shakeTweenId = LeanTween
            .rotateZ(gameObject, 1f, 0.1f)
            .setEase(LeanTweenType.easeShake)
            .setRepeat(2)
            .id;

        foreach (var fruit in fruitSlots)
        {
            if (fruit.State == Fruit.FruitState.OnTree)
            {
                fruit.Shake();
            }
        }

        elapsedSecondsSinceLastHit = 0f;
        hits = (hits + 1) % hitResistance;
        if (hits == 0)
        {
            hitResistance = Random.Range(minResistance, maxResistance + 1);
            DropFruit();
        }
    }

    void DropFruit()
    {
        var fruitOnTree = new List<Fruit>();

        foreach (var fruit in fruitSlots)
        {
            if (fruit.State == Fruit.FruitState.OnTree)
            {
                fruitOnTree.Add(fruit);
            }
        }

        if (fruitOnTree.Count > 0)
        {
            var fruit = fruitOnTree[Random.Range(0, fruitOnTree.Count)];
            fruit.GetComponent<UpdateOrderInLayer>().orderHeightOffset = 0f;
            fruit.Drop();
        }
    }

    void ResetFruitOnTree(Fruit fruit)
    {
        var yDiff = Mathf.Max(0f, transform.position.y - fruit.transform.position.y);
        fruit.GetComponent<UpdateOrderInLayer>().orderHeightOffset = yDiff - 0.11f; // The 0.11 will keep the apple in front of the tree by 1 sorting layer
        fruit.ResetOnTree();
    }
}
