﻿using UnityEngine;

public class IdleState : MoveAbout, IBaseState
{
    public void EnterState()
    {
        animator.SetBool("still", true);
    }

    public void DoState()
    {
        body.velocity = Vector2.zero;
    }

    public void LeaveState()
    {
        animator.SetBool("still", false);
    }
}
