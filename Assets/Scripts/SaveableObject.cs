﻿using UnityEngine;

public class SaveableObject : MonoBehaviour {
    [Tooltip("Leave blank for randomly spawned prefabs. Pre-set for saveable objects in the scene.")]
    public string saveId;

    private void Awake()
    {
        if (System.String.IsNullOrEmpty(saveId))
        {
            saveId = Random.Range(0, int.MaxValue).ToString() + "-" + Random.Range(0, int.MaxValue) + "-" + Random.Range(0, int.MaxValue);
        }
    }
}
