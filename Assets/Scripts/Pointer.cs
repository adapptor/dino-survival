using UnityEngine;
using System.Collections;

public class Pointer : MonoBehaviour
{
    public GameObject pointer;
    public GameObject player;
    public GameObject teleport;

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
    void Update() {
        Vector3 vectorToTarget = teleport.transform.position - player.transform.position;
        float angle = (Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg) - 90;
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        pointer.transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * 1);
	}
}
