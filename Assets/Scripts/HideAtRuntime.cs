﻿using UnityEngine;

public class HideAtRuntime : MonoBehaviour {

    void Awake()
    {
        if (Application.isPlaying)
        {
            var spriteRenderer = GetComponentInChildren<SpriteRenderer>();

            if (spriteRenderer != null)
            {
                spriteRenderer.enabled = false;
            }
        }
    }
}
