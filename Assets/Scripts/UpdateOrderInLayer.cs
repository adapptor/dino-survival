﻿using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class UpdateOrderInLayer : MonoBehaviour
{
    private const float orderYMultiplier = -10f;
    public float orderHeightOffset = 0f;

    public bool debugLog = false;

    [Header("Optional: include only these sprite renderers")]
    public SpriteRenderer[] optionalSpritesToUpdate = new SpriteRenderer[0];

    SpriteRenderer spriteRenderer;
    MeshRenderer meshRenderer;
    LineRenderer lineRenderer;
    
    bool ambiguousSpriteWarningShown;

    void Start()
    {
        ambiguousSpriteWarningShown = false;

        if (optionalSpritesToUpdate.Length == 0)
        {
            var sprites = GetComponents<SpriteRenderer>();
            if (sprites.Length == 0)
            {
                var list = new List<SpriteRenderer>();
                foreach (Transform child in transform)
                {
                    var sprite = child.GetComponent<SpriteRenderer>();
                    if (sprite != null)
                    {
                        list.Add(sprite);
                    }
                }

                sprites = list.ToArray();
            }

            spriteRenderer = null;
            foreach (var sprite in sprites)
            {
                if (sprite.sortingLayerName != "Shadows")
                {
                    if (debugLog) Debug.Log(transform.name + " picked a sprite in layer: " + sprite.sortingLayerName + ", called: " + sprite.transform.name, sprite.transform);
                    if (spriteRenderer != null && !ambiguousSpriteWarningShown)
                    {
                        Debug.LogWarning("Ambiguous sprite selection for UpdateOrderInLayer", this);
                        ambiguousSpriteWarningShown = true;
                        continue;
                    }
                    spriteRenderer = sprite;
                }
            }
        }

        meshRenderer = GetComponent<MeshRenderer>();
        if (meshRenderer == null) {
            meshRenderer = GetComponentInChildren<MeshRenderer>();
        }

        lineRenderer = GetComponent<LineRenderer>();
        if (lineRenderer == null) {
            lineRenderer = GetComponentInChildren<LineRenderer>();
        }

        if (optionalSpritesToUpdate.Length == 0 &&
            spriteRenderer == null &&
            lineRenderer == null &&
            meshRenderer == null)
        {
            Debug.LogWarning("Could not find an appropriate sprite, mesh, or line renderer for UpdateOrderInLayer", this);
        }
    }

    void Update()
    {
        if (optionalSpritesToUpdate.Length > 0)
        {
            foreach (var sprite in optionalSpritesToUpdate)
            {
                sprite.sortingOrder = (int)((transform.position.y + orderHeightOffset) * orderYMultiplier);
            }
        }
        else
        {
            if (spriteRenderer != null)
            {
                spriteRenderer.sortingOrder = (int)((this.transform.position.y + orderHeightOffset) * orderYMultiplier);
            }
        }

        if (meshRenderer != null) {
            meshRenderer.sortingOrder = (int)((this.transform.position.y + orderHeightOffset) * orderYMultiplier);
        }
        
        if (lineRenderer != null) {
            lineRenderer.sortingOrder = (int)((this.transform.position.y + orderHeightOffset) * orderYMultiplier);
        }
    }
}