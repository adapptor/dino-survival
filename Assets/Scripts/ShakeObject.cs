﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeObject : MonoBehaviour {
	public float moveAmount = 0.15f;

	public float speed = 40.0f;

	public bool shakeEnabled;

	private Vector3 localPos;

	// Use this for initialization
	void Awake () {
		localPos = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
		if (shakeEnabled)
		{
			float t = Time.timeSinceLevelLoad * speed;
			transform.localPosition = localPos + new Vector3(
				moveAmount * Mathf.Sin(t) * Mathf.Cos(1.3f * t),
				moveAmount * Mathf.Cos(0.8f * t) * Mathf.Sin(1.1f * t),
				0.0f);
		}
	}

	public void StartShake(float time) {
		shakeEnabled = true;
		CancelInvoke("StopShake");
		Invoke("StopShake", time);
	}

	public void StartShake() {
		shakeEnabled = true;
		CancelInvoke("StopShake");
	}

	public void StopShake()
	{
		if (shakeEnabled) {
			transform.localPosition = localPos;
			shakeEnabled = false;
		}
	}
}
