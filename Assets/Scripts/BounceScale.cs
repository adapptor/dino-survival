﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceScale : MonoBehaviour {

	public float scaleUpAmount = 0.2f;

	private float initialScale = 1f;

	private void Awake()
    {
        initialScale = this.transform.localScale.x;
    }

	void Start () {
		float bulgingScale = initialScale + scaleUpAmount;
        LeanTween
            .scale(gameObject, Vector3.one * bulgingScale, 1.0f)
            .setEase(LeanTweenType.easeOutQuad)
            .setDelay(0.1f)
            .setLoopPingPong();
	}

}
