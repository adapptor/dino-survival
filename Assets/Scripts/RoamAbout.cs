﻿using UnityEngine;

public class RoamAbout : MoveAbout, IBaseState
{
    public float minPauseSeconds = 0.5f;
    public float maxPauseSeconds = 2f;

    public float MaximumDistanceFromOrigin { get; set; }
    public Vector3? PointOfOrigin { get; set; }

    float remainingPauseTimeSeconds = 0f;

    public void EnterState()
    {
        if (!PointOfOrigin.HasValue)
        {
            PointOfOrigin = transform.position;
        }

        Pause();
    }

    public void DoState()
    {
        if (remainingPauseTimeSeconds > 0f)
        {
            remainingPauseTimeSeconds -= Time.deltaTime;

            if (remainingPauseTimeSeconds <= 0f)
            {
                animator.SetBool("still", false);
                PickNewDestination();
                UpdateFacing();
            }
        }
        else
        {
            MoveTowardDestination();
            if (RemainingDistanceToDestination() < Nav.navigationOffset)
            {
                Pause();
            }
        }
    }

    public void LeaveState()
    {
        animator.SetBool("still", false);
    }

    void Pause()
    {
        remainingPauseTimeSeconds = Random.Range(minPauseSeconds, maxPauseSeconds);

        animator.SetBool("still", true);
        destination = transform.position;
        body.velocity = Vector2.zero;
    }

    void PickNewDestination()
    {
        var origin = (Vector2)PointOfOrigin.Value;
        var newRandomPoint = Random.insideUnitCircle;
        newRandomPoint = newRandomPoint * MaximumDistanceFromOrigin;
        newRandomPoint = origin + newRandomPoint;

        if (drawDecisions) Debug.DrawLine(transform.position, newRandomPoint, Color.white, 10f);

        var validDestination = ObstacleInPath(newRandomPoint) == null ? newRandomPoint : FindNearestValidDestination(newRandomPoint, 10, 4);
        if (validDestination.HasValue)
        {
            destination = validDestination.Value;
        }
        else
        {
            Pause();
        }
    }
}
