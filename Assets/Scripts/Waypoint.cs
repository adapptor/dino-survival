﻿using UnityEngine;

[ExecuteInEditMode]
public class Waypoint : MonoBehaviour
{
    public bool drawInEditor = true;

    void Awake()
    {
        if (Application.isPlaying || !drawInEditor)
        {
            var prefabIcon = GetComponentInChildren<SpriteRenderer>();
            if (prefabIcon != null)
            {
                prefabIcon.enabled = false;
            }
        }
    }

    void OnDrawGizmosSelected()
    {
        if (drawInEditor)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, 0.3f);
        }
    }
}
