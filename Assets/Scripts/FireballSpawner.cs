﻿using System.Collections;
using UnityEngine;


public static class AudioFadeScript
{
    public static IEnumerator FadeOut(AudioSource audioSource, float FadeTime)
    {
        while (audioSource.volume > 0f)
        {
            audioSource.volume -= Time.deltaTime / FadeTime;

            yield return null;
        }

        audioSource.volume = 0f;
    }

    public static IEnumerator FadeIn(AudioSource audioSource, float FadeTime)
    {
        while (audioSource.volume < 1f)
        {
            audioSource.volume += Time.deltaTime / FadeTime;

            yield return null;
        }

        audioSource.volume = 1f;
    }
}

public class FireballSpawner : MonoBehaviour
{
    public GameObject fireballPrefab;
    public Transform fireballSpawnPoint;
    public Collider2D avoidArea;
    public float checkPlayerDelay = 5.0f;
    public float targetPlayerRange = 8.0f;
    public float fireballDelay = 0.2f;
    public int fireballCount = 5;

    public AudioSource rumbleSound;
    public AudioSource eruptSound;

    private PlayerController player;
    private Collider2D dangerArea;

    Coroutine inCoroutine = null;
    Coroutine outCoroutine = null;

    void Awake()
    {
        dangerArea = GetComponent<PolygonCollider2D>();
        var placeholder = transform.GetComponentInAncestors<PlaceholderCommon>();

        if (placeholder != null)
        {
            if (dangerArea != null)
            {
                dangerArea.enabled = false;
            }

            dangerArea = placeholder.GetComponent<PolygonCollider2D>();
        }

        if (Application.isPlaying)
        {
            var meshRenderer = GetComponent<MeshRenderer>();
            if (meshRenderer != null)
            {
                meshRenderer.enabled = false;
            }
        }

        rumbleSound.volume = 0;
        rumbleSound.Play();
    }

    // Use this for initialization
    IEnumerator Start()
    {
        player = FindObjectOfType<PlayerController>();
        var camShake = Camera.main.GetComponent<ShakeObject>();
        while (true)
        {
            var playerPos = player.transform.position;
            if (dangerArea.OverlapPoint(playerPos))
            {
                eruptSound.Play();
                // How dare the player enter our area... launch a volley of fireballs!
                if (camShake != null)
                {
                    camShake.StartShake(fireballCount * fireballDelay);
                }
                int count = 0;
                while (count < fireballCount)
                {
                    var offset = Random.insideUnitCircle * targetPlayerRange;
                    var fireballPos = playerPos + new Vector3(offset.x, offset.y);
                    if (!avoidArea.OverlapPoint(fireballPos))
                    {
                        SpawnFireball(fireballPos);
                        count++;
                        yield return new WaitForSeconds(fireballDelay);
                    }
                }
            }
            yield return new WaitForSeconds(checkPlayerDelay);
        }
    }

	void SpawnFireball(Vector3 targetPos)
    {
        var fireballObj = Instantiate(fireballPrefab, fireballSpawnPoint.position, Quaternion.identity);
        var fireball = fireballObj.GetComponent<FireballTrajectory>();
        fireball.target = targetPos;
    }

    void Update()
    {
        if (rumbleSound.time > 24.5f)
        {
            rumbleSound.time = 12.5f;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            if (inCoroutine != null)
            {
                StopCoroutine(inCoroutine);
                inCoroutine = null;
            }

            if (outCoroutine != null)
            {
                StopCoroutine(outCoroutine);
                outCoroutine = null;
            }

            inCoroutine = StartCoroutine(AudioFadeScript.FadeIn(rumbleSound, 1f));
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            if (inCoroutine != null)
            {
                StopCoroutine(inCoroutine);
                inCoroutine = null;
            }

            if (outCoroutine != null)
            {
                StopCoroutine(outCoroutine);
                outCoroutine = null;
            }

            outCoroutine = StartCoroutine(AudioFadeScript.FadeOut(rumbleSound, 1f));
        }
    }
}
