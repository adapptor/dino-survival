﻿using UnityEngine;

public class Nav
{
    public const float navigationOffset = 0.5f;

    public static Vector2 NearestPointOnLine(Vector2 start, Vector2 end, Vector2 pnt)
    {
        var line = (end - start);
        var len = line.magnitude;
        line.Normalize();

        var v = pnt - start;
        var d = Vector3.Dot(v, line);
        d = Mathf.Clamp(d, 0f, len);
        return start + line * d;
    }
}

[ExecuteInEditMode]
public class OffsetNavigationCollider : MonoBehaviour
{
    public CircleCollider2D navigationCollider;
    public CircleCollider2D physicsCollider;

    // Assumes that these objects will not change scale over time
    void Awake()
    {
        OffsetCollider();
    }

    void Update()
    {
        if (!Application.isPlaying)
        {
            OffsetCollider();
        }
    }

    void OffsetCollider()
    {
        if (navigationCollider == null || physicsCollider == null)
        {
            return;
        }

        // add the extra navigation offset and a little extra tolerance
        float effectiveRadius = physicsCollider.radius * transform.lossyScale.x + Nav.navigationOffset + 0.1f;
        navigationCollider.radius = effectiveRadius / transform.lossyScale.x;
    }
}
