﻿using UnityEngine;

public class Hunt : Attack
{
    public float poisonInflictionSeconds = 0.0f;
    public ParticleSystem poisonEffect;
    public RandomAudioSamplePlayer attackSounds;

    public override void EnterState()
    {
        pickDestinationDelay = 0f;
    }

    public override void DoState()
    {
        var targetMarker = GetTargetMarkerPosition();
        pickDestinationDelay -= Time.deltaTime;

        if (!targetMarker.HasValue)
        {
            var target = targetDetector.Target;
            if (pickDestinationDelay <= 0f)
            {
                PickDestination(target.transform.position);
            }
            MoveTowardDestination(attackSpeedMultiplier);

            //trigger an attack if in range
            if (attackRangeDetector.TargetInDamageRange)
            {
                SetTargetMarkerPosition(target.transform.position);
                destination = transform.position;
                MoveTowardDestination(attackSpeedMultiplier);
                attackSounds.Play();
                animator.SetBool("attack", true);
            }
            else
            {
                // if the dino has reached it's destination but can't attack, then idle
                float remaining = RemainingDistanceToDestination();
                if (remaining <= 0.1f)
                {
                    animator.SetBool("still", true);
                }
                else
                {
                    animator.SetBool("still", false);
                }
            }
        }
    }

    public override void LeaveState()
    {
        // No steps necessary
    }

    public void TryBite()
    {
        var target = targetDetector.Target;

        if (target != null && attackRangeDetector.TargetInDamageRange)
        {
            DealDamage(target);
            if (poisonInflictionSeconds > 0)
            {
                target.InflictPoison(poisonInflictionSeconds);
                if (poisonEffect != null)
                {
                    poisonEffect.Play();
                }
            }
        }
    }

    public void BiteCompleted()
    {
        animator.SetBool("attack", false);
        SetTargetMarkerPosition(null);
    }

    void PickDestination(Vector3 targetPosition)
    {
        // Aim directly at the target for the purposes of facing the correct direction
        destination = (Vector2)targetPosition;
        UpdateFacing(0.2f);

        if (drawDecisions) Debug.DrawLine(transform.position, destination, Color.white, 10f);

        var adjustedTarget = GetAdjustedDestination(destination);

        var obstacle = ObstacleInPath(adjustedTarget);
        if (obstacle == null)
        {
            // If there's nothing in the way, go straight toward the adjustedTarget
            destination = adjustedTarget;
            if (drawDecisions) Debug.DrawLine(transform.position, destination, Color.gray, 10f);
        }
        else
        {
            // Find the best option with an obstacle in the way
            PickValidDestination(adjustedTarget, new Vector3(obstacle.Value.x, obstacle.Value.y, 0));

            // Move a bit before trying to pick a new destination
            pickDestinationDelay = 0.5f;
        }
    }
}
