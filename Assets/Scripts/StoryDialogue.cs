﻿using System;
using UnityEngine;

[ExecuteInEditMode]
public class StoryDialogue : MonoBehaviour
{
    [Header("OnStart Actions")]
    public Transform cameraFocusTarget;
    public bool enableHealth = false;
    public bool enableStamina = false;
    public bool enableSprint = false;
    public bool enableBinoculars = false;
    public bool startTimer = false;
    public int timerSeconds = 0;
    public bool freezeGameplay = false;
    public bool toggleReturningHome = false;

    // Stops the gameplay from resuming on completion
    public bool sustainFrozenGameplay = false;

    [Header("OnComplete Actions (On start unless Freeze Gameplay or Camera Focus Target used)")]
    public bool finishLevel = false;
    public StoryDialogue triggerNextDialogue;
    public GameObject[] activateObjects = new GameObject[0];
    public GameObject[] deactivateObjects = new GameObject[0];

    // Can be used to resume story-scriptable "paused" objects for triggers using sustainPausedObjects 
    public Trigger[] triggersToComplete = new Trigger[0];

    // Stops the dialogue from resetting the camera focus to the player on completion
    public bool holdCameraFocus = false;

    [Header("Dialogue Properties")]
    public Character speakingCharacter;
    public bool characterAtBottom;
    [TextArea]
    public string[] dialogue;

    private Hud hud;
    private CameraController camController;
    private PlayerController playerController;

    void OnDrawGizmos()
    {
        Gizmos.DrawIcon(transform.position, "dialogue.png");
    }

    void Awake()
    {
        // Sprite renderer is just there to give the prefab an icon in the project view
        // disable it in the editor and player
        var prefabIcon = GetComponent<SpriteRenderer>();
        if (prefabIcon != null)
        {
            prefabIcon.enabled = false;
        }

        hud = FindObjectOfType<Hud>();
        camController = FindObjectOfType<CameraController>();
        playerController = FindObjectOfType<PlayerController>();
    }

    void Start()
    {
        if (Application.isPlaying)
        {
            foreach (var obj in activateObjects)
            {
                obj.SetActive(false);
            }
        }
    }

    public void PlayDialogue(Action extraOnComplete = null)
    {
        hud.speechCanvas.SetCharacterFace(speakingCharacter, characterAtBottom);

        OnStart();

        Action onComplete = () =>
        {
            OnComplete();

            if (extraOnComplete != null)
            {
                extraOnComplete.Invoke();
            }
        };

        if (!freezeGameplay && cameraFocusTarget == null && !holdCameraFocus)
        {
            onComplete.Invoke();
            onComplete = null;
        }

        Action onDismissed = () =>
        {
            if (triggerNextDialogue != null)
            {
                triggerNextDialogue.PlayDialogue();
            }

            if (onComplete != null)
            {
                onComplete.Invoke();
            }

            if (this.finishLevel)
            {
                FinishLevel();
            }
        };

        hud.speechCanvas.PlayDialogue(dialogue, onDismissed, triggerNextDialogue == null);
    }

    void OnStart()
    {
        if (cameraFocusTarget != null)
        {
            hud.speechCanvas.CanSkip = false;
            camController.SetTarget(cameraFocusTarget, () =>
            {
                hud.speechCanvas.CanSkip = true;
            });
        }

        if (freezeGameplay)
        {
            Time.timeScale = 0.0f;
        }

        var levelManager = FindObjectOfType<LevelManager>();
        if (enableHealth)
        {
            levelManager.SetHealthVisible(true);
        }

        if (enableStamina)
        {
            levelManager.SetStaminaVisible(true);
        }

        if (enableSprint)
        {
            levelManager.SetSprintVisible(true);
        }

        if (enableBinoculars)
        {
            levelManager.SetBinocsVisible(true);
        }

        if (startTimer)
        {
            levelManager.StartTimer(timerSeconds);
        }

        if (toggleReturningHome)
        {
            playerController.ToggleReturningHome();
        }
    }

    void FinishLevel()
    {
        var levelManager = FindObjectOfType<LevelManager>();
        if (levelManager != null)
        {
            levelManager.CompleteLevel();
        }
        else
        {
            Debug.LogWarning("Could not find a LevelManager, unable to finish level");
        }        
    }

    void OnComplete()
    {
        if (freezeGameplay && !sustainFrozenGameplay)
        {
            Time.timeScale = 1.0f;
        }

        foreach (var gameObj in activateObjects)
        {
            gameObj.SetActive(true);
        }

        foreach (var gameObj in deactivateObjects)
        {
            gameObj.SetActive(false);
        }

        foreach (var trigger in triggersToComplete)
        {
            trigger.ResumePausedObjects();
        }

        if (!holdCameraFocus)
        {
            camController.SetTarget(null, null);
        }

        gameObject.SetActive(false);
    }
}
