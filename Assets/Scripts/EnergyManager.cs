﻿using UnityEngine;
using UnityEngine.EventSystems;

public class EnergyManager : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public float runSpeedModifier = 3;
    public float energyLossPerSecond = 5;
    public float staminaLossPerSecond = 1;
    public float energyRegenerationRate = 5;

    public PlayerController playerController;
    public Transform playerTransform;
    public EnergyBar energyBar;

    bool usingEnergyViaUiButton = false;
    bool usingEnergyViaFireButton = false;

    AudioSource energyTapSound;
    public bool usingEnergy { get; private set; }

    void Start()
    {
        var player = GameObject.FindWithTag("Player");
        energyTapSound = GetComponent<AudioSource>();

        if (player != null)
        {
            playerController = player.GetComponent<PlayerController>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (playerController == null)
        {
            return;
        }

        // UI Button supersedes the fire button
        if (!usingEnergyViaUiButton)
        {
            if (usingEnergyViaFireButton && Input.GetAxis("Fire1") <= 0.1f)
            {
                usingEnergyViaFireButton = false;
            }
            else if (!usingEnergyViaFireButton && Input.GetAxis("Fire1") > 0.1f)
            {
                usingEnergyViaFireButton = true;
            }
        }
        else
        {
            usingEnergyViaFireButton = false;
        }

        var usingEnergyNow = (usingEnergyViaUiButton || usingEnergyViaFireButton) && energyBar.hasEnergy();

        if (!usingEnergy && usingEnergyNow)
        {
            energyTapSound.Play();
        }

        usingEnergy = usingEnergyNow;

        if (usingEnergy && playerController.isMoving)
        {
            playerController.speedModifier = runSpeedModifier;
            var energyLoss = energyLossPerSecond * Time.deltaTime;
            var staminaLoss = staminaLossPerSecond * Time.deltaTime;
            energyBar.depleteEnergyAndStamina(energyLoss, staminaLoss);
        }
        else
        {
            playerController.speedModifier = 1;
        }

        if (!usingEnergyViaUiButton && !usingEnergyViaFireButton)
        {
            var energyGain = energyRegenerationRate * Time.deltaTime;
            energyBar.regainEnergy(energyGain);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        usingEnergyViaUiButton = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        usingEnergyViaUiButton = false;
    }

}
