﻿using UnityEngine;

public class BiteEvents : MonoBehaviour
{
    Hunt hunter;

    public void Awake()
    {
        hunter = transform.parent.GetComponent<Hunt>();
    }

    public void Bite()
    {
        hunter.TryBite();
    }

    public void BiteCompleted()
    {
        hunter.BiteCompleted();
    }
}
