﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinoTeleporterArea : MonoBehaviour {

	public DinoTeleporter teleporter;

	List<GameObject> teleportingObjects = new List<GameObject>();

    void OnTriggerEnter2D(Collider2D other)
    {
		if (other.gameObject.tag == "Dinosaur")
		{
			if (teleportingObjects.Contains(other.gameObject))
			{
				return;
			}
			else
			{
				teleportingObjects.Add(other.gameObject);
			}

			// Teleport dino
			teleporter.TransitionToTeleporting();

			// Shake dino
			var dinoObj = other.gameObject;
			var shakeDino = dinoObj.AddComponent<ShakeObject>();
			shakeDino.StartShake();

			// animate dino material for teleport effect
			var fadeToTeleportColorTime = teleporter.teleportTime * 0.9f;
			var dinoRenderer = dinoObj.GetComponentInChildren<Renderer>();
			var startTeleportColor = dinoRenderer.material.color;
			//fade to cyan and transparent (because that's obviously what teleporting looks like)
			var endTeleportColor = Color.cyan;
			endTeleportColor.a = 0;

			LeanTween.value (
				dinoObj, 
				startTeleportColor, 
				endTeleportColor, 
				fadeToTeleportColorTime).setOnUpdate ((Color val) => { 
					dinoRenderer.material.color = val;
				});

			StartCoroutine(DestroyDinoDelayed(dinoObj));
		}
	}

	private IEnumerator DestroyDinoDelayed(GameObject dinoObj) {
		yield return new WaitForSeconds(teleporter.teleportTime);

		// add score for the dino
		Dino dino = dinoObj.GetComponent<Dino>();
		if (dino != null) {
            var levelManager = FindObjectOfType<LevelManager>();
            if (levelManager != null)
            {
                levelManager.OnDinoTeleported(dino.DinoType);
            }
		}
		
		teleportingObjects.Remove(dinoObj);
		Destroy(dinoObj);
	}
}
