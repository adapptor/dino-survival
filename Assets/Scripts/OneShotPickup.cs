﻿using System.Collections;
using UnityEngine;

public class OneShotPickup : MonoBehaviour
{
    public float healthModifier;
    public float energyModifier;
    [Range(0f, 60f)]
    public float psychadelicSeconds;
    public AudioSource pickupAudio;
    public GameObject hudPickupPrefab;

    public bool PickedUp { get; set; }
    public Theme? OptionalTheme { get; set; }

    RectTransform uiCanvasTransform;

    void Awake()
    {
        PickedUp = false;
        uiCanvasTransform = GameObject.Find("UI").GetComponent<RectTransform>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (PickedUp)
        {
            return;
        }

        if (other.tag == "Player")
        {
            PickedUp = true;
            if (hudPickupPrefab != null)
            {
                var hudPickup = Instantiate(hudPickupPrefab, uiCanvasTransform, false);
                var theme = hudPickup.GetComponent<Themed>();
                hudPickup.GetComponent<HudPickup>().OnComplete = ApplyPickup;
                if (theme != null && OptionalTheme.HasValue)
                {
                    theme.theme = OptionalTheme.Value;
                    theme.UpdateTheme();
                }
            }
            else
            {
                ApplyPickup();
            }

            if (pickupAudio != null) {
                StartCoroutine(destroyAfterAudioFinished());
            } else {
                Destroy(gameObject);
            }
        }
    }

    void ApplyPickup()
    {
        if (!Mathf.Approximately(healthModifier, 0f))
        {
            FindObjectOfType<Healthbar>().changeHealth(healthModifier);
        }

        if (!Mathf.Approximately(energyModifier, 0f))
        {
            FindObjectOfType<EnergyBar>().changeStamina(energyModifier);
        }

        if (!Mathf.Approximately(psychadelicSeconds, 0f))
        {
            FindObjectOfType<PlayerController>().addPsychadelicSeconds(psychadelicSeconds);
        }
    }

    private IEnumerator destroyAfterAudioFinished() {
        pickupAudio.Play();
        // disable colliders and renderers so the object effectively is removed from the game world
        disableColliders();
        disableRenderers();
        yield return new WaitForSeconds(pickupAudio.clip.length);
        // once audio is played, destroy the object for real
        Destroy(gameObject);
    }

    private void disableColliders() {
        foreach (Collider2D parentCollider in GetComponents<Collider2D>()) {
            parentCollider.enabled = false;
        }
        foreach (Collider2D childCollider in GetComponentsInChildren<Collider2D>()) {
            childCollider.enabled = false;
        }
    }

    private void disableRenderers() {
        foreach (Renderer parentRenderer in GetComponents<Renderer>()) {
            parentRenderer.enabled = false;
        }
        foreach (Renderer childRenderer in GetComponentsInChildren<Renderer>()) {
            childRenderer.enabled = false;
        }
    }
}
