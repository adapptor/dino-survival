﻿using UnityEngine;

public class Charge : Attack
{
    bool damageDealtForCharge = false;
    public float chargeSpeedMultiplier = 2.5f;
    public float additionalChargeDistance = 7f;
    public RandomAudioSamplePlayer chargeSounds;

    float remainingCoolOffTimeSeconds = 0f;

    void StartCharging(Vector3 targetPosition)
    {
        chargeSounds.Play();
        damageDealtForCharge = false;
        SetTargetMarkerPosition(targetPosition);
        animator.SetBool("charge", true);
    }

    void StopCharging()
    {
        damageDealtForCharge = false;
        SetTargetMarkerPosition(null);
        animator.SetBool("charge", false);
    }

    private void CoolOff()
    {
        remainingCoolOffTimeSeconds = 2f;

        animator.SetBool("still", true);
    }

    public override void EnterState()
    {
        var target = targetDetector.Target;

        pickDestinationDelay = 0f;
    }

    public override void DoState()
    {
        var target = targetDetector.Target;
        pickDestinationDelay -= Time.deltaTime;

        if (remainingCoolOffTimeSeconds > 0f)
        {
            remainingCoolOffTimeSeconds -= Time.deltaTime;

            // Keep moving while the animation switches over
            if (RemainingDistanceToDestination() > Nav.navigationOffset)
            {
                MoveTowardDestination(chargeSpeedMultiplier);
            }
            else
            {
                body.velocity = Vector2.zero;
            }
        }
        else
        {
            var targetMarker = GetTargetMarkerPosition();

            if (targetMarker.HasValue)
            {
                MoveTowardDestination(chargeSpeedMultiplier);

                if (!damageDealtForCharge && target != null && attackRangeDetector.TargetInDamageRange)
                {
                    DealDamage(target);
                    damageDealtForCharge = true;
                }

                if (RemainingDistanceToDestination() <= 1f)
                {
                    StopCharging();
                    CoolOff();
                }
            }
            else
            {
                animator.SetBool("still", false);
                if (pickDestinationDelay <= 0f)
                {
                    PickDestination(target.transform.position);
                }
                MoveTowardDestination(attackSpeedMultiplier);
            }
        }
    }

    public override bool ShouldLeaveState()
    {
        if (remainingCoolOffTimeSeconds > 0f)
        {
            return false;
        }
        else
        {
            var targetMarker = GetTargetMarkerPosition();
            if (targetMarker.HasValue && !DefendArea.ColliderInDefendArea(dinoCollider))
            {
                StopCharging();
                CoolOff();
                destination = transform.position;
                return false;
            }
            else
            {
                return base.ShouldLeaveState();
            }
        }
    }

    public override void LeaveState()
    {
        // No steps necessary
    }

    void PickDestination(Vector3 targetPosition)
    {
        // Aim directly at the target for the purposes of facing the correct direction
        destination = (Vector2)targetPosition;
        UpdateFacing(0.2f);

        if (drawDecisions) Debug.DrawLine(transform.position, destination, Color.white, 10f);

        var adjustedTarget = GetAdjustedDestination(destination);

        var obstacle = ObstacleInPath(adjustedTarget);
        if (obstacle == null)
        {
            // If there's nothing in the way, start charging
            StartCharging(targetPosition);

            // Keep charging past your target by a significant amount
            var difference = adjustedTarget - (Vector2)transform.position;
            adjustedTarget = (Vector2)transform.position + difference.normalized * (difference.magnitude + additionalChargeDistance);

            obstacle = ObstacleInPath(adjustedTarget);
            if (obstacle == null)
            {
                // Still nothing in the way, go straight toward the adjustedTarget
                destination = adjustedTarget;
                if (drawDecisions) Debug.DrawLine(transform.position, destination, Color.gray, 10f);
            }
            else
            {
                // Go as far as you can then, use the position a little way before the obstacle
                destination = PointJustBefore(0.2f, obstacle.Value);

                if (drawDecisions) Debug.DrawLine(transform.position, destination, Color.magenta, 10f);
            }
        }
        else
        {
            // Find the best option with an obstacle in the way
            PickValidDestination(adjustedTarget, obstacle.Value);

            // Move a bit before trying to pick a new destination
            pickDestinationDelay = 0.5f;
        }
    }
}
