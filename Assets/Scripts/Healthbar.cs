﻿using System;
using UnityEngine;

public class Healthbar : MonoBehaviour {

	public const float kMaxHealth = 100;

	private RectTransform healthBarPanel;
	private float healthBarInitialWidth;
    [System.NonSerialized]
	public float currentHealth = kMaxHealth;

    internal Action OnHealthIncreaseButNotAtMax;
    internal Action OnHealthReachedMax;

    // Use this for initialization
    void Start () {
		healthBarPanel = (RectTransform)transform.Find ("healthbar_fg").GetComponent<RectTransform>();
		healthBarInitialWidth = healthBarPanel.rect.width;
    }

    public void changeHealth(float changeAmount)
    {
        if (OnHealthReachedMax != null)
        {
            if (changeAmount > 0f && currentHealth + changeAmount >= kMaxHealth)
            {
                OnHealthReachedMax();
            }
        }

        if (OnHealthIncreaseButNotAtMax != null)
        {
            if (changeAmount > 0f && currentHealth + changeAmount < kMaxHealth)
            {
                OnHealthIncreaseButNotAtMax();
            }
        }

        currentHealth = Mathf.Clamp(currentHealth + changeAmount, 0f, kMaxHealth);
    }

	public void takeHealth(float amount) {
		currentHealth = Mathf.Max(0.0f, currentHealth - amount);
	}

	void Update() {
		var healthPercent = currentHealth / kMaxHealth;
		var newHealthbarWidth = healthPercent * healthBarInitialWidth;
		healthBarPanel.sizeDelta = new Vector2 (newHealthbarWidth, 1.0f);
	}

	public bool isAlive() {
		return currentHealth > 0;
	}

}
