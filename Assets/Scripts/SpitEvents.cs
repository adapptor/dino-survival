﻿using UnityEngine;

public class SpitEvents : MonoBehaviour
{
    public Spitter spitter;

    public void Awake()
    {
        spitter = transform.parent.GetComponent<Spitter>();
    }

    public void Spit()
    {
        spitter.Spit();
    }

    public void SpitComplete()
    {
        spitter.SpitComplete();
    }
}
