﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinoAnimationEventListener : MonoBehaviour {
    public RandomAudioSamplePlayer footstepSounds;
    public void playFootStepAudio() {
        footstepSounds.Play();
    }
}
