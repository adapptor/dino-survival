﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ZoomButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    private CameraController mainCamera;
    private PlayerController playerController;
    private bool zoomingOut = false;
    private float currentZoomFactor = 0f;
    public float zoomSpeed = 6;

    // Use this for initialization
    void Start()
    {
        mainCamera = GameObject.FindObjectOfType<CameraController>();
        playerController = FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (zoomingOut && !playerController.isMoving)
        {
            handleZoomOut();
        }
        else
        {
            handleZoomIn();
        }
    }

    private void handleZoomOut()
    {
        currentZoomFactor = Mathf.Min(currentZoomFactor + Time.deltaTime * zoomSpeed, 1f);
        mainCamera.ZoomFactor = currentZoomFactor;
    }

    private void handleZoomIn()
    {
        currentZoomFactor = Mathf.Max(currentZoomFactor - Time.deltaTime * zoomSpeed, 0f);
        mainCamera.ZoomFactor = currentZoomFactor;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        zoomingOut = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        zoomingOut = false;
    }
}
