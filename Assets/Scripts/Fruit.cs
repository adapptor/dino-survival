using UnityEngine;

[ExecuteInEditMode]
public class Fruit : MonoBehaviour
{
    public enum FruitState
    {
        OnTree,
        Dropping,
        Dropped,
        Gone
    }

    public GameObject fallingFruit;
    public SpriteRenderer shadow;
    public AudioSource thudSound;
    public GameObject pickupPrefab;

    public FruitState State { get; private set; }

    float height = 0f;
    float shadowScale;
    float fuzzyShadowScale;
    const float kGravityConstant = 9.8f / 3f;

    int? shakeTweenId;
    int? dropTweenId;
    int? shadowScaleTweenId;
    int? shadowAlphaTweenId;

    OneShotPickup pickupInstance = null;
    Themed theme;

    void Awake()
    {
        SetInitialValues();
        theme = GetComponentInChildren<Themed>(true);
    }

    void Update()
    {
        if (Application.isPlaying)
        {
            if (State == FruitState.Dropped && pickupInstance.PickedUp)
            {
                SetAsGone();
            }
        }
        else
        {
            SetInitialValues();
        }
    }

    void SetInitialValues()
    {
        height = fallingFruit.transform.position.y - transform.position.y;
        shadow.transform.position = transform.position;
        shadowScale = shadow.transform.localScale.x;
        fuzzyShadowScale = shadowScale * 2f;
        if (Application.isPlaying)
        {
            SetAsGone();
        }
        else
        {
            ResetOnTree();
        }
    }

    public void ResetOnTree()
    {
        if (State == FruitState.Dropping)
        {
            CancelDrop();
        }

        State = FruitState.OnTree;
        fallingFruit.transform.position = transform.position + new Vector3(0f, height, 0f);
        if (Application.isPlaying)
        {
            shadow.color = new Color(shadow.color.r, shadow.color.g, shadow.color.b, 0f);
        }
        fallingFruit.SetActive(true);
    }

    public void Shake()
    {
        if (!shakeTweenId.HasValue)
        {
            shakeTweenId = LeanTween
                .rotateZ(fallingFruit, 5f, 0.1f)
                .setEase(LeanTweenType.easeShake)
                .setRepeat(2)
                .setOnComplete(ShakeComplete)
                .id;
        }
    }

    public void Drop()
    {
        if (State != FruitState.OnTree)
        {
            ResetOnTree();
        }

        State = FruitState.Dropping;

        if (!shakeTweenId.HasValue)
        {
            ShakeComplete();
        }
    }

    void ShakeComplete()
    {
        shakeTweenId = null;

        if (State != FruitState.Dropping)
        {
            return;
        }

        dropTweenId = null;
        shadowScaleTweenId = null;
        shadowAlphaTweenId = null;

        var fallTime = Mathf.Sqrt(2f * height / kGravityConstant);
        var timeOffset = fallTime / 4f;

        dropTweenId = LeanTween
            .move(fallingFruit, transform, fallTime + timeOffset)
            .setEase(LeanTweenType.easeInQuad)
            .setOnComplete(Dropped)
            .setTime(timeOffset)
            .id;

        shadow.transform.localScale = Vector3.one * fuzzyShadowScale;
        shadowScaleTweenId = LeanTween
            .scale(shadow.gameObject, Vector3.one * shadowScale, fallTime + timeOffset)
            .setEase(LeanTweenType.easeInQuad)
            .setTime(timeOffset)
            .id;

        shadowAlphaTweenId = LeanTween
            .alpha(shadow.gameObject, 1f, fallTime + timeOffset)
            .setEase(LeanTweenType.easeInQuad)
            .setTime(timeOffset)
            .id;
    }

    public void Dropped()
    {
        CancelDrop();

        State = FruitState.Dropped;

        thudSound.Play();
        pickupInstance = Instantiate(pickupPrefab, transform.position, Quaternion.identity).GetComponent<OneShotPickup>();
        pickupInstance.OptionalTheme = theme.theme;
    }

    void CancelDrop()
    {
        if (shakeTweenId.HasValue)
        {
            LeanTween.cancel(shakeTweenId.Value);
        }

        if (dropTweenId.HasValue)
        {
            LeanTween.cancel(dropTweenId.Value);
        }

        if (shadowScaleTweenId.HasValue)
        {
            LeanTween.cancel(shadowScaleTweenId.Value);
        }

        if (shadowAlphaTweenId.HasValue)
        {
            LeanTween.cancel(shadowAlphaTweenId.Value);
        }
    }

    void SetAsGone()
    {
        State = FruitState.Gone;
        fallingFruit.SetActive(false);
        shadow.color = new Color(shadow.color.r, shadow.color.g, shadow.color.b, 0f);
    }
}
