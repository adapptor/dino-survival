﻿using UnityEngine;

public class FollowPath : MoveAbout, IBaseState
{
    public float pauseSeconds = 1f;
    public WaypointPath Path { get; set; }

    int? currentWaypointId;

    float remainingPauseTimeSeconds = 0f;
    bool gettingIntoPosition = false;

    public void EnterState()
    {
        var waypoint = Path.GetNearestWaypointId((Vector2)transform.position);

        if (waypoint.HasValue)
        {
            // Keep going in the direction you were heading before
            if (currentWaypointId.HasValue && currentWaypointId < 0)
            {
                waypoint = waypoint * -1;
            }
            currentWaypointId = waypoint;
            destination = GetStartingPosition();
        }
        else
        {
            destination = (Vector2)transform.position;
        }

        UpdateFacing();

        Pause();
    }

    public void DoState()
    {
        if (!currentWaypointId.HasValue)
        {
            return;
        }

        if (remainingPauseTimeSeconds > 0f)
        {
            remainingPauseTimeSeconds -= Time.deltaTime;

            if (remainingPauseTimeSeconds <= 0f)
            {
                animator.SetBool("still", false);
            }
        }
        else
        {
            MoveTowardDestination();
            if (RemainingDistanceToDestination() < Nav.navigationOffset)
            {
                if (!gettingIntoPosition)
                {
                    currentWaypointId = Path.GetNextWaypointId(currentWaypointId.Value);
                }

                gettingIntoPosition = false;

                destination = Path.GetWaypointPosition(currentWaypointId.Value);
                UpdateFacing();
            }
        }
    }

    public void LeaveState()
    {
        animator.SetBool("still", false);
    }

    public Vector2 GetStartingPosition()
    {
        var currentPosition = (Vector2)transform.position;
        var currentWaypointPosition = Path.GetWaypointPosition(currentWaypointId.Value);

        var secondClosestWaypointId = Path.GetPreviousWaypointId(currentWaypointId.Value);
        var secondClosestWaypointPosition = Path.GetWaypointPosition(secondClosestWaypointId);
        var distanceToSecondClosestWaypoint = Vector2.Distance(currentPosition, secondClosestWaypointPosition);
        var previousIsClosest = true;

        var nextWaypointId = Path.GetNextWaypointId(currentWaypointId.Value);

        // if previous and next are not the same waypoint
        if (secondClosestWaypointId != nextWaypointId && secondClosestWaypointId != nextWaypointId * -1)
        {
            var nextWaypointPosition = Path.GetWaypointPosition(nextWaypointId);
            var distanceToNextWaypoint = Vector2.Distance(currentPosition, nextWaypointPosition);

            if (distanceToSecondClosestWaypoint > distanceToNextWaypoint)
            {
                secondClosestWaypointId = nextWaypointId;
                secondClosestWaypointPosition = nextWaypointPosition;
                distanceToSecondClosestWaypoint = distanceToNextWaypoint;
                previousIsClosest = false;
            }
        }

        Vector2 nearestPoint = Nav.NearestPointOnLine(currentWaypointPosition, secondClosestWaypointPosition, currentPosition);
        if (Vector2.Distance(currentWaypointPosition, nearestPoint) < Nav.navigationOffset * 2)
        {
            if (drawDecisions) Debug.DrawLine(transform.position, currentWaypointPosition, Color.white, 10f);
            gettingIntoPosition = false;
            return currentWaypointPosition;
        }
        else
        {
            if (drawDecisions) Debug.DrawLine(transform.position, nearestPoint, Color.yellow, 10f);
            gettingIntoPosition = previousIsClosest;
            return nearestPoint;
        }
    }

    void Pause()
    {
        remainingPauseTimeSeconds = pauseSeconds;

        animator.SetBool("still", true);
        body.velocity = Vector2.zero;
    }
}
