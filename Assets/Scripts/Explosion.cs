﻿using UnityEngine;

public class Explosion : MonoBehaviour
{
    public GameObject scorchMark;
    public GameObject scorchMarkSprite;
    public Collider2D blastZone;
    public Animator explosionAnim;
    public RandomAudioSamplePlayer boomSounds;

    void Start()
    {
        boomSounds.Play();
        scorchMarkSprite.transform.eulerAngles = new Vector3(0f, 0f, Random.Range(0f, 359f));
        scorchMarkSprite.transform.localScale = Vector3.zero;
        scorchMark.transform.localScale = new Vector3(1f, 0.5f, 1f);

        int id = LeanTween.scale(scorchMarkSprite, Vector3.one * Random.Range(0.4f, 0.5f), 0.1f).id;

        Invoke("DisableDamage", 1f);
        Invoke("FadeOut", explosionAnim.GetCurrentAnimatorClipInfo(0)[0].clip.length);
    }

    void DisableDamage()
    {
        blastZone.enabled = false;
    }

    void FadeOut()
    {
        explosionAnim.gameObject.SetActive(false);

        int id = LeanTween.alpha(scorchMarkSprite, 0f, 10f).id;
        LTDescr tween = LeanTween.descr(id);

        tween.setEase(LeanTweenType.easeInExpo);
        tween.setOnComplete(() => Destroy(gameObject));
    }
}
