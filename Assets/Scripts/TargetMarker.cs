﻿using UnityEngine;

//
// controls the 2D target on the ground where damage will be applied at some point in the future
public class TargetMarker : MonoBehaviour
{
    SpriteRenderer spriteRenderer;
    float flashAlphaMin = 0.5f;
    float flashAlphaMax = 1.0f;

    LTDescr flashTween;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void Start()
    {
        setAlpha(flashAlphaMin);
        flashTween = LeanTween.alpha(gameObject, flashAlphaMax, 0.2f).setLoopPingPong();
    }

    void setAlpha(float a)
    {
        var c = spriteRenderer.color;
        c.a = a;
        spriteRenderer.color = c;
    }

    public void OnDestroy()
    {
        LeanTween.cancel(flashTween.uniqueId);
    }
}
