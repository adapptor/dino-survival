﻿using UnityEngine;

public class TargetDetector : MonoBehaviour
{
    public PlayerController Target { get; private set; }
    Rigidbody2D body;

    void Awake()
    {
        body = transform.parent.GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (Target != null)
        {
            transform.right = (Target.transform.position - transform.position).normalized * transform.parent.localScale.x;
            return;
        }

        if (body.velocity.magnitude > 0.1f)
        {
            transform.right = body.velocity.normalized * transform.parent.localScale.x;
        }
        else
        {
            transform.rotation = Quaternion.identity;
        }
    }

    void OnTriggerEnter2D(Collider2D trigger)
    {
        if (trigger.tag == "AttackTarget")
        {
            var player = trigger.transform.parent.GetComponent<PlayerController>();
            if (player != null && player.isAlive)
            {
                Target = player;
            }
        }
    }

    void OnTriggerExit2D(Collider2D trigger)
    {
        if (trigger.tag == "AttackTarget")
        {
            Target = null;
        }
    }
}
