﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationEvents : MonoBehaviour {

	public void handleFootstepEvent() {
		PlayerController player = GetComponent<PlayerController>();
		if (player != null) {
			player.playFootstepSound();
		}
	}
}
