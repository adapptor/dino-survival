﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    [HideInInspector]
    public bool IsPanning;
    [HideInInspector]
    public bool IsZooming;
    public bool CanPan = false;
    public bool CanZoom = true;

    // Factor (percentage) of zoom range.
    [Range(0.0f, 1.0f)]
    public float ZoomFactor = 0.5f;

    // Rate at which zoom can change.
    public float ZoomRate = 0.03f;

    public float MinZoom = 3.0f;
    public float MaxZoom = 6.0f;

    const float MOVE_SPEED = 20.0f;

    PlayerController player;
    Transform targetObject;
    int moveTweenId;

    float ZoomValue
    {
        get { return MinZoom + ZoomFactor * (MaxZoom - MinZoom); }
    }

    public float CameraPanSensitvity = 1.0f;

    bool lastIsMouseButtonDown;
    Vector3 lastMouseButtonDownPos;
    Camera camComponent;

    float depth = 0;
    float lastRotX;

    public void Awake()
    {
        depth = transform.position.z;
        camComponent = GetComponentInChildren<Camera>();
        var playerObject = GameObject.FindWithTag("Player");
        if (playerObject != null)
        {
            player = playerObject.GetComponent<PlayerController>();
        }
        // It's important that the camera position and zoom is calculated in the Awake function as other scripts rely
        // on the screen dimensions in their Start functions
        RepositionCamera();
    }

    void SetCameraPosition(Vector2 pos)
    {
        transform.position = new Vector3(pos.x, pos.y, depth);
    }

    /// <summary>
    /// Tells the camera to tween to the given position. Note that following a moving target is not supported.
    /// Set target back to null to move the camera back to the player.
    /// </summary>
    public void SetTarget(Transform target, System.Action onMoveComplete)
    {
        if (target != null)
        {
            // Stop camera following player
            targetObject = target;
        }
        Vector2 tweenTarget = target == null ? player.transform.position : target.transform.position;
        float distance = Vector2.Distance(tweenTarget, this.transform.position);

        LeanTween.cancel(moveTweenId);
        if (distance < 0.1f)
        {
            return;
        }

        moveTweenId = LeanTween.move(this.gameObject, new Vector3(tweenTarget.x, tweenTarget.y, depth), distance / MOVE_SPEED)
            .setEaseInOutQuad()
            .setUseEstimatedTime(true)
            .setOnComplete(() =>
            {
                if (target == null)
                {
                    // Allow camera to follow player again
                    targetObject = null;
                }
                if (onMoveComplete != null)
                {
                    onMoveComplete();
                }
            })
            .uniqueId;
    }

    void Update ()
    {
        RepositionCamera();
    }

    void RepositionCamera()
    {
        camComponent.orthographicSize = ZoomValue;

        if (CanPan)
        {
            DoPan();
        }

        if (Application.isEditor && CanZoom)
        {
            DoZoom();
        }

        lastMouseButtonDownPos = Input.mousePosition;

        if (player != null && !IsPanning && targetObject == null)
        {
            player.HasCameraFocus = true;
            SetCameraPosition(player.transform.position);
        }
        else
        {
            player.HasCameraFocus = false;
        }
    }

    void DoZoom()
    {
        // IsZooming = Input.GetMouseButton(1);

        // if (IsZooming && lastMouseButtonDownPos != Input.mousePosition)
        // {
        //     OnZoomed(lastMouseButtonDownPos, Input.mousePosition);
        // }
    }

    void DoPan()
    {
        if (Input.GetMouseButton(0))
        {
            // Start panning if dragged a small distance
            if (!IsPanning && Vector3.Distance(lastMouseButtonDownPos, Input.mousePosition) > 4.0f)
            {
                IsPanning = true;
            }
        }
        else
        {
            IsPanning = false;
        }

        if (IsPanning && lastMouseButtonDownPos != Input.mousePosition)
        {
            OnDragged(lastMouseButtonDownPos, Input.mousePosition);
        }
    }

    void OnDragged(Vector2 from, Vector2 to)
    {
        var delta = (to - from) * CameraPanSensitvity * -1.0f;
        var pos = this.transform.position;
        SetCameraPosition((Vector2)pos + delta);
    }

    void OnZoomed(Vector2 from, Vector2 to)
    {
        var delta = Mathf.Sign((to - from).y) * ZoomRate;
        ZoomFactor = Mathf.Clamp(ZoomFactor + delta, 0.0f, 1.0f);
    }
}
