using UnityEngine;
using System.Collections.Generic;

public enum PlayerState
{
    Idle,
    Moving,
}

public class PlayerController : MonoBehaviour
{
    public PlayerState state = PlayerState.Idle;

    public float walkSpeed = 1.0f;
    public float speedModifier = 1.0f;
    public bool facingRight = true;
    public List<DamageArea> damageAreas;
    public List<SlowArea> slowAreas;
    public Animator outlineAnim;
    public DinoType typeOfEggBeingHeld = DinoType.None;
    public float poisonDamagePerSecond = 5f;
    public ParticleSystem poisonEffect;
    public AudioSource eggPickupSound;
    public RandomAudioSamplePlayer damageSounds;
    public RandomAudioSamplePlayer poisonSounds;
    public RandomAudioSamplePlayer footstepsNormalSounds;
    public RandomAudioSamplePlayer footstepsMudSounds;

    // egg prefabs
    public GameObject DimetroEgg;
    public GameObject PachyEgg;
    public GameObject RaptorEgg;
    public GameObject DilopoEgg;

    private LevelManager levelMgr;
    private Healthbar playerHealth;
    private EnergyBar energyBar;
    private bool wasAlive = true;

    private Color defaultStartColor = Color.white;
    private Color damageEndColor = Color.red;
    private float damageAnimationDuration = 0.2f;
    private float targetScale = 1.1f;
    private bool damageAnimationLoop = false;
    private int damageTweenA;
    private int damageTweenB;
    private ScreenDistortionEffect screenDistortion;
    private float timeLeftWithPoison = 0f;

    private Animator anim; // Reference to the player's animator component.

    private Rigidbody2D body;

    private SingleJoystick joystick;

    public bool HasCameraFocus { get; set; }

    internal float CollideRadius
    {
        get; private set;
    }

    private bool returningHome = false;
    public bool isReturningHome
    {
        get { return typeOfEggBeingHeld != DinoType.None || returningHome; }
    }

    public bool isAlive
    {
        get { return playerHealth == null ? true : playerHealth.isAlive(); }
    }

    public float currentHealth
    {
        get { return playerHealth.currentHealth; }
        set { playerHealth.currentHealth = value; }
    }

    public bool isMoving { get; private set; }

    // Use this for initialization
    void Awake()
    {
        var hud = FindObjectOfType<Hud>();
        joystick = hud.joystick;
        energyBar = hud.energyBar;
        playerHealth = hud.healthBar;
        levelMgr = FindObjectOfType<LevelManager>();
        body = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        CollideRadius = GetComponent<Collider2D>().bounds.extents.magnitude;
        screenDistortion = Camera.main.GetComponent<ScreenDistortionEffect>();

        if (screenDistortion == null)
        {
            Debug.LogWarning("Can't find the screen distortion effect on the main camera, make sure you are using the MainCamera prefab in your scene");
        }

        if (playerHealth == null)
        {
            Debug.LogError("Can't find the healthbar, make sure you have the UI prefab in your scene");
        }
    }

    public void ToggleReturningHome()
    {
        returningHome = !returningHome;
    }

    private void UpdateMovement()
    {
        var actualSpeed = 0.0f;
        Vector3 walkDirection = Vector3.zero;

        // Disable input if level not active
        if ((levelMgr == null || levelMgr.levelState == LevelState.Playing) && HasCameraFocus)
        {
            actualSpeed = speedModifier;
            if (slowAreas.Count > 0)
            {
                actualSpeed *= combinedSpeedModifierFromSlowAreas();
            }

            walkDirection = joystick == null ? walkDirection : joystick.GetInputDirection();
            // Fall back to keyboard input
            if (walkDirection.sqrMagnitude < 0.1f)
            {
                walkDirection.x = Input.GetAxis("Horizontal");
                walkDirection.y = Input.GetAxis("Vertical");
            }
        }

        if (psychadelicSeconds > 0.0f)
        {
            walkDirection = -walkDirection;
        }

        if (walkDirection.sqrMagnitude < 0.1f)
        {
            actualSpeed = 0.0f;
            StopWalking();
        }
        else
        {
            walkDirection.Normalize();
            Walk(walkDirection * walkSpeed * actualSpeed);
        }

        if (actualSpeed > 0.0f)
        {
            anim.speed = actualSpeed;
            outlineAnim.speed = actualSpeed;
        }

        float h = body.velocity.x;

        // If the input is moving the player doesn't match direction facing
        if ((Mathf.Abs(h) > 0.05 && (h < 0) == facingRight))
        {
            Flip();
        }

        anim.SetFloat("VerticalVelocity", body.velocity.y);
        outlineAnim.SetFloat("VerticalVelocity", body.velocity.y);
        anim.SetFloat("HorizontalVelocity", System.Math.Abs(h));
        outlineAnim.SetFloat("HorizontalVelocity", System.Math.Abs(h));
    }

    private float combinedSpeedModifierFromSlowAreas()
    {
        float combinedModifier = 1;
        foreach (SlowArea area in slowAreas)
        {
            combinedModifier *= area.speedModifier;
        }
        return combinedModifier;
    }

    public bool hasBackTurnedToCamera()
    {
        return anim.GetCurrentAnimatorStateInfo(0).IsName("PlayerRun_B")
            || anim.GetCurrentAnimatorStateInfo(0).IsName("PlayerIdle_B");
    }

    private void UpdateDeath()
    {
        if (wasAlive && !isAlive)
        {
            Kill();
        }

        anim.SetBool("IsAlive", isAlive);
        outlineAnim.SetBool("IsAlive", isAlive);
        wasAlive = isAlive;
    }

    private float psychadelicSeconds;

    private void HandleInput()
    {
        if (Input.GetKeyUp("p"))
        {
            InflictPoison(3.0f);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        UpdateDeath();

        if (!isAlive)
        {
            return;
        }

        HandleInput();
        UpdatePoison();
        UpdateDamage();
        UpdateMovement();
        UpdatePsychadelic();
    }

    private void UpdatePoison()
    {
        timeLeftWithPoison = timeLeftWithPoison - Time.deltaTime;
        if (timeLeftWithPoison < 0)
        {
            timeLeftWithPoison = 0;
            poisonEffect.Stop();
        }
        else
        {
            var healthReduction = poisonDamagePerSecond * Time.deltaTime;
            playerHealth.takeHealth(healthReduction);
            poisonSounds.Play();
            if (!poisonEffect.isPlaying)
            {
                poisonEffect.Play();
            }
        }
    }

    private void startDamageAnimation(bool continousLoop)
    {
        damageAnimationLoop = continousLoop;

        LeanTween.cancel(damageTweenA);
        damageTweenA = LeanTween.value(gameObject, defaultStartColor, damageEndColor, damageAnimationDuration)
            .setLoopPingPong(continousLoop ? -1 : 2)
            .uniqueId;

        LeanTween.cancel(damageTweenB);
        damageTweenB = LeanTween.value(gameObject, 1, targetScale, damageAnimationDuration)
            .setOnUpdate((float size, object arg2) =>
            {
                float xSize = size;
                if (!facingRight)
                {
                    xSize *= -1;
                }
                transform.localScale = new Vector3(xSize, size, size);
            })
            .setLoopPingPong(continousLoop ? -1 : 2)
            .uniqueId;
    }

    private void UpdateDamage()
    {
        if (damageAreas.Count > 0)
        {
            if (!damageAnimationLoop)
            {
                startDamageAnimation(true);
            }
            var healthReduction = totalDamageAmountFromDamageAreas() * Time.deltaTime;
            playerHealth.takeHealth(healthReduction);
            damageSounds.Play();
        }
        else if (damageAnimationLoop)
        {
            LeanTween.cancel(damageTweenA);
            LeanTween.cancel(damageTweenB);
            float xScale = facingRight ? 1 : -1;
            transform.localScale = new Vector3(xScale, 1, 1);
            gameObject.GetComponent<SpriteRenderer>().color = Color.white;
            damageAnimationLoop = false;
        }
    }

    private float totalDamageAmountFromDamageAreas()
    {
        float total = 0;
        foreach (DamageArea area in damageAreas)
        {
            total += area.damagePerSecond;
        }
        return total;
    }

    public void DealDamage(float amount)
    {
        playerHealth.takeHealth(amount);
        startDamageAnimation(false);
        damageSounds.Play();
    }

    public void Kill()
    {
        playerHealth.currentHealth = 0.0f;
        psychadelicSeconds = 0.0f;
        anim.SetBool("IsAlive", false);
        outlineAnim.SetBool("IsAlive", false);
        StopWalking();

        // Stop dinosaurs from attacking
        foreach (Collider c in GetComponents<Collider>())
        {
            c.enabled = false;
        }
        body.isKinematic = true;

        if (typeOfEggBeingHeld != DinoType.None)
        {
            SpawnEgg(GetComponentInChildren<EggInBagBehaviour>().transform.position);
            typeOfEggBeingHeld = DinoType.None;
        }

        timeLeftWithPoison = 0;
        energyBar.resetStamina();

        Invoke("Respawn", 3.0f);
    }

    private void Respawn()
    {
        var spawnObj = FindObjectOfType<PlayerSpawnPos>();
        var spawnPos = spawnObj == null ? Vector3.zero : spawnObj.transform.position;
        this.transform.position = spawnPos;
        playerHealth.currentHealth = Healthbar.kMaxHealth;
        // TODO reset boost
        wasAlive = true;

        body.isKinematic = false;
        anim.SetBool("IsAlive", true);
        outlineAnim.SetBool("IsAlive", true);
    }

    void Walk(Vector3 v)
    {
        isMoving = true;
        transform.GetComponent<Rigidbody2D>().velocity = v;
    }

    void StopWalking()
    {
        isMoving = false;
        transform.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
    }

    void Flip()
    {
        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (!isAlive)
            return;

        if (other.tag == "Egg" && typeOfEggBeingHeld == DinoType.None)
        {
            Egg otherEgg = other.gameObject.GetComponent<Egg>();
            if (!otherEgg.isHatching())
            {
                typeOfEggBeingHeld = otherEgg.type;
                Destroy(other.gameObject);
                eggPickupSound.Play();
            }
        }
        else if (other.tag == "EggDropoff" && typeOfEggBeingHeld != DinoType.None)
        {
            Egg newEggObject = SpawnEgg(other.transform.position);
            if (newEggObject != null)
            {
                newEggObject.spawnDino();
            }
            typeOfEggBeingHeld = DinoType.None;
        }
        else if (other.tag == "Explosion")
        {
            DealDamage(20);
        }
    }

    Egg SpawnEgg(Vector3 pos)
    {
        GameObject newEggObject = null;
        switch (typeOfEggBeingHeld)
        {
            case DinoType.Dimetro:
                newEggObject = Instantiate(DimetroEgg, pos, Quaternion.identity);
                break;
            case DinoType.Pachy:
                newEggObject = Instantiate(PachyEgg, pos, Quaternion.identity);
                break;
            case DinoType.Raptor:
                newEggObject = Instantiate(RaptorEgg, pos, Quaternion.identity);
                break;
            case DinoType.Dilopo:
                newEggObject = Instantiate(DilopoEgg, pos, Quaternion.identity);
                break;
        }
        return newEggObject.GetComponent<Egg>();
    }

    internal void addPsychadelicSeconds(float amount)
    {
        this.psychadelicSeconds += amount;
    }

    internal void UpdatePsychadelic()
    {
        if (psychadelicSeconds > 0)
        {
            psychadelicSeconds -= Time.deltaTime;
        }
        if (screenDistortion != null)
        {
            screenDistortion.SetDistortionEnabled(psychadelicSeconds > 0.0f);
        }
    }

    public void InflictPoison(float poisonDuration)
    {
        timeLeftWithPoison += poisonDuration;
    }

    public void playFootstepSound()
    {
        if (slowAreas.Count > 0)
        {
            footstepsMudSounds.Play();
        }
        else
        {
            footstepsNormalSounds.Play();
        }
    }
}

