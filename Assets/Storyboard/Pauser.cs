﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class Pauser : MonoBehaviour
{

    PlayableDirector director;

    // Use this for initialization
    void Start()
    {
       
    }



    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {
        director = GameObject.FindObjectOfType<PlayableDirector>();
        director.Pause();
    }

    private void OnDisable()
    {
        
    }


    public void Resume()
    {
        director = GameObject.FindObjectOfType<PlayableDirector>();
        director.Resume();
    }
}
