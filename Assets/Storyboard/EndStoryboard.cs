﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class EndStoryboard : MonoBehaviour
{
    void OnEnable()
    {
        if (!SaveGame.Instance.hasSeenStory)
        {
            SaveGame.Instance.hasSeenStory = true;
            SaveGame.Instance.Save();

            SceneManager.LoadScene(SaveGame.Instance.currentLevel);
        }
        else
        {
            SceneManager.LoadScene("main_menu");
        }
    }
}
