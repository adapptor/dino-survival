﻿using UnityEngine;

public class SpitterPlaceholder : Placeholder
{
    SpitterTrigger spitterTrigger;
    Transform venomTarget;

    void SetTriggerAndTarget()
    {
        if (spitterTrigger == null)
        {
            spitterTrigger = GetComponentInChildren<SpitterTrigger>();
        }

        if (venomTarget == null)
        {
            venomTarget = GetComponentInChildren<HideAtRuntime>().transform;
        }
    }

    protected override void CleanUpPrefabInstance()
    {
        SetTriggerAndTarget();

        spitterTrigger.transform.parent = null;
        venomTarget.parent = null;

        base.CleanUpPrefabInstance();

        spitterTrigger.transform.parent = transform;
        venomTarget.parent = transform;
    }

    protected override void PlacePrefabInstance()
    {
        SetTriggerAndTarget();

        var newInstance = prefabInstance == null;

        base.PlacePrefabInstance();

        if (newInstance)
        {
            var spitter = prefabInstance.GetComponent<Spitter>();
            spitter.SetTrigger(spitterTrigger);
            spitter.SetTarget(venomTarget);
        }
    }
}
