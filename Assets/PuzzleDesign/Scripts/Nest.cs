﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Nest : MonoBehaviour
{
    public enum EggCount
    {
        OneEgg,
        TwoEggs,
        ThreeEggs,
        FourEggs
    }
    
    public Transform[] oneEggSpawnPoint = new Transform[1];
    public Transform[] twoEggsSpawnPoints = new Transform[2];
    public Transform[] threeEggSpawnPoints = new Transform[3];
    public Transform[] fourEggSpawnPoints = new Transform[4];

    public DinoTypedPrefab[] dinoTypedPrefabs;

    EggCount? eggsSpawned = null;
    List<GameObject> spawnedEggs = new List<GameObject>();
    DinoType typeSpawned;

    public void SpawnEggs(DinoType eggType, EggCount eggCount)
    {
        if (eggsSpawned.HasValue && eggsSpawned.Value == eggCount && typeSpawned == eggType)
        {
            return;
        }

        eggsSpawned = eggCount;
        typeSpawned = eggType;

        foreach (var spawnedEgg in spawnedEggs)
        {
            DestroyImmediate(spawnedEgg);
        }

        spawnedEggs.Clear();

        var dinoTypedPrefab = dinoTypedPrefabs.SingleOrDefault(dtp => dtp.type == eggType);

        if (dinoTypedPrefab == null)
        {
            return;
        }

        switch (eggCount)
        {
            case EggCount.TwoEggs:
                SpawnEggs(twoEggsSpawnPoints, dinoTypedPrefab.prefab);
                break;
            case EggCount.ThreeEggs:
                SpawnEggs(threeEggSpawnPoints, dinoTypedPrefab.prefab);
                break;
            case EggCount.FourEggs:
                SpawnEggs(fourEggSpawnPoints, dinoTypedPrefab.prefab);
                break;
            default:
                SpawnEggs(oneEggSpawnPoint, dinoTypedPrefab.prefab);
                break;
        }
    }

    void SpawnEggs(Transform[] spawnPoints, GameObject eggPrefab)
    {
        foreach (var spawnPoint in spawnPoints)
        {
            var randomTilt = Quaternion.Euler(0f, 0f, Random.Range(-10f, 10f));
            var egg = Instantiate(eggPrefab, spawnPoint.position, randomTilt);
            egg.transform.parent = transform;
            spawnedEggs.Add(egg);
        }
    }
}
