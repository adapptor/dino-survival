﻿using UnityEngine;

[ExecuteInEditMode]
public class Puzzle : MonoBehaviour
{
    public Theme theme = Theme.Grass;

    public DinoType dinoType = DinoType.Raptor;

    public bool pausePlaceholdersInEditor = false;

    [System.Serializable]
    public class NestEggCount
    {
        public Placeholder nestPlaceholder;
        public Nest.EggCount eggCount;
    }

    public NestEggCount[] eggsPerNest = new NestEggCount[1];

    private void Awake()
    {
        // Sprite renderer attached to a puzzle is just there to give the prefab an icon in the project view
        // disable it in the editor and player
        var puzzleIcon = GetComponent<SpriteRenderer>();
        if (puzzleIcon != null)
        {
            puzzleIcon.enabled = false;
        }

        PausePlaceholders();
        LayOutPuzzle();
    }

    void Update()
    {
        if (!Application.isPlaying)
        {
            if (transform.GetComponentInAncestors<Puzzle>() != null)
            {
                Debug.LogError("Puzzles cannot be nested in other puzzles", this);
            }

            if (!pausePlaceholdersInEditor)
            {
                LayOutPuzzle();
            }
            else
            {
                PausePlaceholders();
            }
        }
    }

    void LayOutPuzzle()
    {
        ApplyDinoTypeInChildren();
        SpawnChildren();
        ThemeChildren();
        SpawnEggsInNests();
    }

    void SpawnChildren()
    {
        var placeholderChildren = GetComponentsInChildren<PlaceholderCommon>(true);

        for (int i = placeholderChildren.Length - 1; i >= 0; --i)
        {
            placeholderChildren[i].Spawn();
        }
    }

    void PausePlaceholders()
    {
        var placeholderChildren = GetComponentsInChildren<PlaceholderCommon>(true);

        // Iterate backwards to make sure nested placeholders have their leaves paused before their parent
        for (int i = placeholderChildren.Length - 1; i >= 0; --i)
        {
            placeholderChildren[i].PauseInEditor();
        }
    }

    void ThemeChildren()
    {
        var themedChildren = GetComponentsInChildren<Themed>(true);

        foreach (var themedChild in themedChildren)
        {
            if (themedChild.theme != theme)
            {
                themedChild.theme = theme;
                themedChild.UpdateTheme();
            }
        }
    }

    void ApplyDinoTypeInChildren()
    {
        var dinoTypedChildren = GetComponentsInChildren<DinoTypedPlaceholder>(true);

        foreach (var dinoTypedChild in dinoTypedChildren)
        {
            dinoTypedChild.dinoType = dinoType;
            dinoTypedChild.UpdateDinoType();
        }
    }

    void SpawnEggsInNests()
    {
        foreach (var nestEgg in eggsPerNest)
        {
            if (nestEgg.nestPlaceholder == null)
            {
                continue;
            }

            var nest = nestEgg.nestPlaceholder.GetComponentInChildren<Nest>(true);

            if (nest == null)
            {
                continue;
            }

            nest.SpawnEggs(dinoType, nestEgg.eggCount);
        }
    }
}
