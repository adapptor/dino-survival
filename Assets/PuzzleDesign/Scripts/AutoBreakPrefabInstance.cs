﻿using UnityEngine;

[ExecuteInEditMode]
public class AutoBreakPrefabInstance : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying)
        {
            UnityEditor.PrefabUtility.DisconnectPrefabInstance(gameObject);
        }
#endif
    }
}
