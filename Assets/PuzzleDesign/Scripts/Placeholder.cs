﻿using System.Collections.Generic;
using UnityEngine;

public static class TransformExtensions
{
    public static T GetComponentInAncestors<T>(this Transform transform) where T : MonoBehaviour
    {
        var parent = transform.parent;
        for (;;)
        {
            if (parent == null)
            {
                return null;
            }

            var component = parent.GetComponent<T>();
            if (component != null)
            {
                return component;
            }

            parent = parent.parent;
        }
    }
}

[ExecuteInEditMode]
public abstract class PlaceholderCommon : MonoBehaviour
{
    public bool prefabInheritsScale = false;
    public bool prefabInheritsRotation = false;

    protected GameObject prefabInstance = null;

    void Awake()
    {
        // Sprite renderer attached to a placeholder is just there to give the prefab an icon in the project view
        // disable it in the editor and player
        var prefabIcon = GetComponent<SpriteRenderer>();
        if (prefabIcon != null)
        {
            prefabIcon.enabled = false;
        }

        if (transform.GetComponentInAncestors<Puzzle>() == null)
        {
            CleanUpPrefabInstance();
            PlacePrefabInstance();
        }
    }

    void Update()
    {
        if (!Application.isPlaying)
        {
            if (transform.GetComponentInAncestors<Puzzle>() == null)
            {
                PlacePrefabInstance();
            }
        }
    }

    public void Spawn()
    {
        PlacePrefabInstance();
    }

    public void PauseInEditor()
    {
        CleanUpPrefabInstance();
    }

    protected virtual void CleanUpPrefabInstance()
    {
        for (int i = transform.childCount - 1; i >= 0; --i)
        {
            DestroyImmediate(transform.GetChild(i).gameObject);
        }

        prefabInstance = null;
    }

    protected abstract void PlacePrefabInstance();

    protected void PlacePrefabInstance(GameObject prefab)
    {
        if (prefabInstance == null)
        {
            prefabInstance = Instantiate(prefab, transform);
        }

        prefabInstance.transform.localPosition = Vector3.zero;

        if (!prefabInheritsRotation)
        {
            prefabInstance.transform.eulerAngles = Vector3.zero;
        }
        else
        {
            prefabInstance.transform.localEulerAngles = Vector3.zero;
        }

        if (!prefabInheritsScale)
        {
            prefabInstance.transform.parent = null;
            prefabInstance.transform.localScale = Vector3.one;
            prefabInstance.transform.parent = transform;
        }
        else
        {
            prefabInstance.transform.localScale = Vector3.one;
        }
    }
}

[ExecuteInEditMode]
public class Placeholder : PlaceholderCommon
{
    public GameObject prefab;

    protected override void PlacePrefabInstance()
    {
        if (prefab != null)
        {
            PlacePrefabInstance(prefab);
        }
    }
}
