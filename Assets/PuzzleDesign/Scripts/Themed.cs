﻿using UnityEngine;

public enum Theme
{
    Grass,
    Snow
}

[ExecuteInEditMode]
public class Themed : MonoBehaviour
{
    public Theme theme = Theme.Grass;

    [System.Serializable]
    public class ThemedChildObject
    {
        public Theme theme;
        public GameObject childObject;
    }

    public ThemedChildObject[] themedChildObjects;

    public void UpdateTheme()
    {
        if (themedChildObjects != null)
        {
            foreach (var themedChildObject in themedChildObjects)
            {
                if (themedChildObject != null && themedChildObject.childObject != null)
                {
                    themedChildObject.childObject.SetActive(themedChildObject.theme == theme);
                }
            }
        }
    }

    void Update()
    {
        if (!Application.isPlaying)
        {
            if (transform.GetComponentInAncestors<Puzzle>() == null)
            {
                UpdateTheme();
            }
        }
    }
}
