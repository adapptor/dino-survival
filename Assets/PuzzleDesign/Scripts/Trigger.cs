﻿using UnityEngine;

public interface IStoryScriptable
{
    // Pause for story purposes 
    void StoryPause();

    // Resume for story purposes
    void StoryResume();
}

[ExecuteInEditMode]
public class Trigger : MonoBehaviour
{
    public GameObject[] activateObjects = new GameObject[0];
    public GameObject[] deactivateObjects = new GameObject[0];
    public GameObject[] pauseUntilTriggered = new GameObject[0];
    public StoryDialogue dialogue;

    // This object will be passed along to the dialogue for the camera to focus on
    // note that this will override any existing focus object
    public Transform focusObjectForDialogue;

    // This will keep the paused objects paused until
    // something external resumes them
    public bool sustainPausedObjects = false;

    void Awake()
    {
        // Sprite renderer is just there to give the prefab an icon in the project view
        // disable it in the editor and player
        var prefabIcon = GetComponentInChildren<SpriteRenderer>();
        if (prefabIcon != null)
        {
            prefabIcon.enabled = false;
        }

        if (Application.isPlaying)
        {
            var meshRenderer = GetComponent<MeshRenderer>();
            if (meshRenderer != null)
            {
                meshRenderer.enabled = false;
            }
        }
    }

    void Start()
    {
        if (Application.isPlaying)
        {
            foreach (var obj in pauseUntilTriggered)
            {
                var scriptable = obj.GetComponentInChildren<IStoryScriptable>();
                if (scriptable != null)
                {
                    scriptable.StoryPause();
                }
                else
                {
                    Debug.LogWarning("Could not find IStoryScriptable on object", obj);
                }
            }

            foreach (var obj in activateObjects)
            {
                obj.SetActive(false);
            }
        }
    }

    public void ResumePausedObjects()
    {
        foreach (var obj in pauseUntilTriggered)
        {
            var scriptable = obj.GetComponentInChildren<IStoryScriptable>();
            if (scriptable != null)
            {
                scriptable.StoryResume();
            }
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && gameObject.activeInHierarchy)
        {
            foreach (var obj in activateObjects)
            {
                obj.SetActive(true);
            }

            foreach (var obj in deactivateObjects)
            {
                obj.SetActive(false);
            }

            if (dialogue != null)
            {
                if (focusObjectForDialogue != null)
                {
                    dialogue.cameraFocusTarget = focusObjectForDialogue;
                }
                dialogue.PlayDialogue();
            }

            if (!sustainPausedObjects)
            {
                ResumePausedObjects();
            }

            gameObject.SetActive(false);
        }
    }
}
