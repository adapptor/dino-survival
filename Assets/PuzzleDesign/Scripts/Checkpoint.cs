﻿using UnityEngine;

[ExecuteInEditMode]
public class Checkpoint : MonoBehaviour
{
    public Checkpoint next;
    public Checkpoint previous;

    public WaypointPath optionalPathToNext;
    public WaypointPath optionalPathToPrevious;

    [Header("These stay unchanged")]
    public GameObject therePteraPrefab;
    public GameObject backPteraPrefab;

    public Transform thereSpawnPoint;
    public Transform backSpawnPoint;
    public WaypointPath therePath;
    public WaypointPath backPath;

    Flyer thereInstance = null;
    Flyer backInstance = null;

    PlayerController player;
    float screenDiagonalWithBuffer = 0f;
    bool playerInCheckpoint = false;

    void DrawOffsetLine(Vector2 from, Vector2 to, float offset = 0.25f)
    {
        var dir = (to - from).normalized;
        var dir90 = new Vector2(dir.y * -1, dir.x);

        Gizmos.DrawLine(from + dir90 * 0.5f, to + dir90 * 0.5f);
    }

    void OnDrawGizmos()
    {
        if (next != null)
        {
            Gizmos.color = Color.magenta;

            if (optionalPathToNext != null)
            {
                DrawOffsetLine(
                    (Vector2)transform.position,
                    optionalPathToNext.GetWaypointPosition(optionalPathToNext.GetFirstWaypointId()));

                DrawOffsetLine(
                    optionalPathToNext.GetWaypointPosition(optionalPathToNext.GetLastWaypointId()),
                    (Vector2)next.transform.position);
            }
            else
            {
                DrawOffsetLine((Vector2)transform.position, (Vector2)next.transform.position);
            }
        }

        if (previous != null)
        {
            Gizmos.color = Color.cyan;

            if (optionalPathToPrevious != null)
            {
                DrawOffsetLine(
                    (Vector2)transform.position,
                    optionalPathToPrevious.GetWaypointPosition(optionalPathToPrevious.GetLastWaypointId()));

                DrawOffsetLine(
                    optionalPathToPrevious.GetWaypointPosition(optionalPathToPrevious.GetFirstWaypointId()),
                    (Vector2)previous.transform.position);
            }
            else
            {
                DrawOffsetLine((Vector2)transform.position, (Vector2)previous.transform.position);
            }
        }
    }

    void Awake()
    {
        // Sprite renderer is just there to give the prefab an icon in the project view
        // disable it in the editor and player
        var prefabIcon = GetComponentInChildren<SpriteRenderer>();
        if (prefabIcon != null)
        {
            prefabIcon.enabled = false;
        }

        if (Application.isPlaying)
        {
            var meshRenderer = GetComponent<MeshRenderer>();
            if (meshRenderer != null)
            {
                meshRenderer.enabled = false;
            }
        }
    }

    void Start()
    {
        if (Application.isPlaying)
        {
            Vector3 lowerLeft = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
            Vector3 upperRight = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));

            // Distance from the player (centre) to the screen corner plus a bit of buffer
            screenDiagonalWithBuffer = Vector3.Distance(lowerLeft, upperRight) / 2f + 3f;

            player = GameObject.FindWithTag("Player").GetComponent<PlayerController>();

            if (next != null)
            {
                SpawnTherePtera();
            }

            if (previous != null)
            {
                SpawnBackPtera();
            }
        }
    }

    void Update()
    {
        if (Application.isPlaying)
        {
            if (playerInCheckpoint)
            {
                if (player.isReturningHome && previous != null && backInstance != null)
                {
                    backInstance.ShowTheWay(previous, optionalPathToPrevious, true);
                    backInstance = null;
                }

                if (!player.isReturningHome && next != null && thereInstance != null)
                {
                    thereInstance.ShowTheWay(next, optionalPathToNext, false);
                    thereInstance = null;
                }
            }

            if (Vector2.Distance(player.transform.position, transform.position) > screenDiagonalWithBuffer)
            {
                if (next != null && thereInstance == null && Vector2.Distance(player.transform.position, thereSpawnPoint.position) > screenDiagonalWithBuffer)
                {
                    SpawnTherePtera();
                }

                if (previous != null && backInstance == null && Vector2.Distance(player.transform.position, backSpawnPoint.position) > screenDiagonalWithBuffer)
                {
                    SpawnBackPtera();
                }
            }
        }
    }

    void SpawnTherePtera()
    {
        thereInstance = Instantiate(therePteraPrefab, thereSpawnPoint.position, Quaternion.identity).GetComponent<Flyer>();
        thereInstance.CircleCheckpoint(therePath);
    }

    void SpawnBackPtera()
    {
        backInstance = Instantiate(backPteraPrefab, backSpawnPoint.position, Quaternion.identity).GetComponent<Flyer>();
        backInstance.CircleCheckpoint(backPath);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            playerInCheckpoint = true;
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            playerInCheckpoint = false;
        }
    }
}
