﻿using UnityEngine;

[ExecuteInEditMode]
public class SelectRootPlaceholder : MonoBehaviour
{
#if UNITY_EDITOR
    void Update()
    {
        if (!Application.isPlaying)
        {
            if (UnityEditor.Selection.activeTransform == transform)
            {
                var placeholder = transform.GetComponentInAncestors<PlaceholderCommon>();
                var rootPlaceholder = placeholder;

                while (placeholder != null)
                {
                    rootPlaceholder = placeholder;
                    placeholder = placeholder.transform.GetComponentInAncestors<PlaceholderCommon>();
                }

                if (rootPlaceholder != null)
                {
                    UnityEditor.Selection.activeTransform = rootPlaceholder.transform;
                }
            }
        }
    }
#endif
}
