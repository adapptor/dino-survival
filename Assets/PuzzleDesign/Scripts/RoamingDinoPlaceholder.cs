﻿using UnityEngine;

public class RoamingDinoPlaceholder : DinoPlaceholder
{
    [Range(5f, 30f)]
    public float roamRadius = 10f;

    void OnDrawGizmos()
    {
#if UNITY_EDITOR
        if (UnityEditor.Selection.activeTransform == transform)
        {
            Gizmos.color = Color.black;
            Gizmos.DrawWireSphere(transform.position, roamRadius);
        }
#endif
    }

    protected override void PlacePrefabInstance()
    {
        base.PlacePrefabInstance();

        if (Application.isPlaying)
        {
            var dinoStateMachine = prefabInstance.GetComponent<Dino>();

            var roamState = prefabInstance.GetComponent<RoamAbout>();
            roamState.MaximumDistanceFromOrigin = roamRadius;
            roamState.PointOfOrigin = transform.position;
            dinoStateMachine.BaseState = roamState;

            dinoStateMachine.StartStateMachine();
        }
    }
}
