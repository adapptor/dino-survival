﻿using UnityEngine;

public abstract class DinoPlaceholder : DinoTypedPlaceholder
{
    public DefendArea defendArea;

    protected float scaleFactor = 1f;

    protected override void PlacePrefabInstance()
    {
        // Force no scale inheritance for all dinosaurs since their scale is randomised
        prefabInheritsScale = false;

        base.PlacePrefabInstance();

        if (Application.isPlaying)
        {
            if (defendArea != null)
            {
                var dinoStateMachine = prefabInstance.GetComponent<Dino>();

                // Currently each dinosaur only has one attack state
                var attackState = prefabInstance.GetComponent<Attack>();
                attackState.DefendArea = defendArea;
                dinoStateMachine.PreferredState = attackState;
            }
            else
            {
                Debug.LogWarning("Dino placeholder does not have a defend area associated with it so it will not attack", this);
            }
        }
    }
}
