﻿using UnityEngine;

[ExecuteInEditMode]
public class SelectParent : MonoBehaviour
{
#if UNITY_EDITOR
    void Update()
    {
        if (!Application.isPlaying)
        {
            if (UnityEditor.Selection.activeTransform == transform)
            {
                UnityEditor.Selection.activeTransform = transform.parent;
            }
        }
    }
#endif
}
