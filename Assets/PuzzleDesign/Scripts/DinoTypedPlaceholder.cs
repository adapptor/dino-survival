﻿using System.Linq;
using UnityEngine;

[System.Serializable]
public class DinoTypedPrefab
{
    public DinoType type;
    public GameObject prefab;
}

[ExecuteInEditMode]
public class DinoTypedPlaceholder : PlaceholderCommon
{
    public DinoType dinoType = DinoType.Raptor;

    public DinoTypedPrefab[] dinoTypedPrefabs;

    private DinoType spawnedDinoType = DinoType.Raptor;

    public void UpdateDinoType(bool respawnIfNecessary = false)
    {
        if (spawnedDinoType != dinoType)
        {
            CleanUpPrefabInstance();

            if (respawnIfNecessary)
            {
                PlacePrefabInstance();
            }
        }
    }

    protected override void PlacePrefabInstance()
    {
        if (dinoTypedPrefabs != null)
        {
            var dinoTypedPrefab = dinoTypedPrefabs.SingleOrDefault(dtp => dtp.type == dinoType);
            if (dinoTypedPrefab != null && dinoTypedPrefab.prefab != null)
            {
                spawnedDinoType = dinoType;
                PlacePrefabInstance(dinoTypedPrefab.prefab);
            }
        }
    }

    void Update()
    {
        if (!Application.isPlaying)
        {
            if (transform.GetComponentInAncestors<Puzzle>() == null)
            {
                UpdateDinoType(true);
            }
        }
    }
}
