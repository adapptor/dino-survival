﻿using UnityEngine;

public class PathFollowingDinoPlaceholder : DinoPlaceholder
{
    public WaypointPath path;

    protected override void PlacePrefabInstance()
    {
        base.PlacePrefabInstance();

        if (Application.isPlaying)
        {
            if (path != null)
            {
                var dinoStateMachine = prefabInstance.GetComponent<Dino>();

                var followPathState = prefabInstance.GetComponent<FollowPath>();
                followPathState.Path = path;
                dinoStateMachine.BaseState = followPathState;

                dinoStateMachine.StartStateMachine();
            }
            else
            {
                Debug.LogWarning("Path following dino placeholder does not have a path associated with it so it will just stand still", this);
            }
        }
    }
}
