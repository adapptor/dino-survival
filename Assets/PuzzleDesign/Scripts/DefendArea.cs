﻿using UnityEngine;

[ExecuteInEditMode]
public class DefendArea : MonoBehaviour
{
    private Collider2D playerCollider;
    private Collider2D defendAreaCollider;

    void Awake()
    {
        // Sprite renderer is just there to give the prefab an icon in the project view
        // disable it in the editor and player
        var prefabIcon = GetComponentInChildren<SpriteRenderer>();
        if (prefabIcon != null)
        {
            prefabIcon.enabled = false;
        }


        if (Application.isPlaying)
        {
            defendAreaCollider = GetComponent<PolygonCollider2D>();

            var playerController = FindObjectOfType<PlayerController>();
            if (playerController != null)
            {
                playerCollider = playerController.GetComponent<CircleCollider2D>();
            }

            var meshRenderer = GetComponent<MeshRenderer>();
            if (meshRenderer != null)
            {
                meshRenderer.enabled = false;
            }
        }
    }

    public bool PlayerInDefendArea()
    {
        if (playerCollider != null && defendAreaCollider != null)
        {
            return defendAreaCollider.IsTouching(playerCollider);
        }

        return false;
    }

    public bool ColliderInDefendArea(Collider2D coll)
    {
        if (defendAreaCollider != null)
        {
            return defendAreaCollider.IsTouching(coll);
        }

        return false;
    }
}
