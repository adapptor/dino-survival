﻿Shader "Custom/DistortEffect"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Amount("Amount", Float) = 0.05
		_InvertedAmount("InvertedAmount", Float) = 0
		_Speed("Speed", Float) = 2.0
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			sampler2D _MainTex;
			float _Amount;
			float _InvertedAmount;
			float _Speed;

			fixed4 frag(v2f i) : SV_Target
			{
				// Warpy warpy!
				float t = _Time.y * _Speed;
				float2 offsetUV = float2(
					(sin(i.uv.y * i.uv.x * 4.5 + t * 0.3) + cos(i.uv.x * 12.5 + t * 1.3)),
					(cos(i.uv.y * 11.5 + t * 1.2) + sin(i.uv.x * i.uv.y * 5.5 + t * 0.5)));

				// Sample screen texture with offset applied
				fixed4 col = tex2D(_MainTex, i.uv + _Amount * offsetUV);

				// invert the colors and blend with original
				col.rgb = lerp(col.rgb, 1 - col.rgb, _InvertedAmount);
				return col;
			}
			ENDCG
		}
	}
}
