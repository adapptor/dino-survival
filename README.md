# Dino Survival B

Trello board: https://trello.com/b/5LLeKB9I/dino-survival
Unity version required: 2017.1.1f1

## Backstory

You are sent back in time to save dinosaurs from extinction!
You have a time machine which can send dinosaurs back into the future.
But, here's the thing, it doesn't work on big dinosaurs, only small ones.
It also doesn't work on dinosaur eggs because they are too fragile.

Your goal is to collect dinosaur eggs from around the world, hatch them, and send the baby dino's back into the future! But, beware of the big dinos, because they will be protecting those eggs and will be _very_ angry if they see someone trying to steal them! 

Your success depends on how many dinosaurs you save before you get eaten alive by angry dinosaurs :)


## Minimal game plan
- Collect dinosaur eggs from around the world
- Bring them back to a start area with "time machine"
- As they hatch, dinosaurs get sent to the time machine and it contributes to your "score"
- Dinosaurs can attack and kill your character
- When you die, you get a summary of all of the dinosaurs you have saved.

Main gameplay is about avoiding dinosaurs, finding and hatching eggs.

## Wild Dinosaur behaviour
- They "roam" around their nests, if you get too close to their nests and they spot you then they will chase and attack you, until you leave their nest area
- They can be hit by a stick (melee) or rocks (projectile) by the player, which will stop their attack momentarily
- They will continue to attack you until you die, then they will roam again
- Dinosaurs are given a random size and corresponding attack strength
    - bigger dinosaurs hurt you more
    - bigger dinosaurs are slower

## Game world
- Randomly generated game world with "points of interest"
- Points of interest do not include the standard randomly generated object but have custom objects
- Standard world objects:
    - Apple trees, apples can be eaten to regain health
    - Rocks
    - plants
    - palm trees
    - bodies of water (cannot be passed)
    - bodies of lava (cannot be passed)
- World will be surrounded by either water or lava so character cannot escape
- Points of interest
    - Volcano
    - Dinosaur nests
        - Dimetrodon
        - Pachy
        - Pterodactyl
        - Raptor
    - Starting location
        - Walled off area for eggs to hatch and baby dinosaurs to roam
        - "Time Machine" in the middle which teleports baby dinos into the future

## Dinosaur nests and eggs
- Eggs are spawned in the nest (perhaps when the nest isn't visible)
- Eggs can be picked up by the character
- Eggs can be taken back to the start location where they are left to "incubate" (dropped on the floor somewhere)
- Incubated eggs hatch after a while, and the player "scores"
- Hatched egg turns into a small (scaled down) version of a dinosaur which roams within the start location

## Starting location
- Walled off area which cannot be entered by dinosaurs
- If and egg gets dropped in the area, then it will begin to "incubate"
- If the egg hatches and the player is not in the area, the baby dino will roam 
- If the egg hatched and the player is in the area, the baby will follow the player
    - The player can then lead the dino into the time machine and the dino will be sent into the future
    - If the player has baby dinos following them and leaves the area, the dinos will roam again.

## Difficulty and gameplay
- As the game progresses, more dinosaurs appear and they become more aggressive
- It's about saving as many dinosaurs you can by sending them into the future before dying
- You continuously lose health as you play due to hunger, eating replenishes health

## iOS Build

Make sure you have installed the iOS Build Support package. The installer is
available in the `PublicData/Software/Unity` directory on Doozer and Moonshine.

Inside Unity select `File -> Build Settings -> iOS -> Build And Run`. This will
cause Unity to generate and open a new XCode project.

Next you will need to install the signing certificates and provisioning
profiles. We're using Fastlane Match to manage these. In order to pull down the
signing certificates all you need to run is `fastlane match development`. You
can find the passphrase for the certificates repository in LastPass.

Inside XCode you need to select a valid provisioning profile under Signing
(Debug) and then you should be able to run the game on your device. If you see
an error then go to 'Build Settings' and under 'Code Signing Identity -> Debug'
change the selection from 'iOS Distribution' to 'iOS Development'.

If you need to add a new device to the provisioning profile then you first need
to find the device UUID from iTunes and then run this command:

```
fastlane run register_device udid:"<uuid>" name:"<device-name>"
fastlane match development --force
```

## Releasing a new build to Test Flight

In the Unity editor go to 'Edit -> Project Settings -> Player'. Expand the
'Other Settings' section and increase the version and build number. Afterwards,
build the app as per the instructions above.

When you are happy to release, in XCode go to 'Product -> Archive'. After it is
complete select 'Upload to App Store' on the RHS.

## Impassable Terrain

A script called `ImpassableTerrain` fills any attached collider with randomly
spawned prefab objects. In addition, there are two starter prefabs for the two
terrain themes (`ImpassableWoodlandForest` and `ImpassableSnowyForest`) that you
can drop into your scene and start configuring. 

The starter prefabs come pre-configured with the appropriate environment objects
for each theme (e.g. green and orange trees for woodland, blue and purple for
snowy).

You can tell the script to generate during `Start()` by checking the `Generate
At Start` option. If the scene is not procedurally generate then it is better to
pre-generate using the `Regenerate` and `Clear` inspector buttons.

Since the generator creates a lot of objects, you should be careful to make sure
that they are _lightweight_. All of the prefabs in the `Environment` subfolder
have been stripped down to just a sprite and a simple script that sets their
sorting order at start-up.
